<?php

return [
    'message' => 'il presente sito web utilizza cookie tecnici per garantire il corretto funzionamento delle procedure e migliorare l\'esperienza di uso delle applicazioni online. Se vuoi sapeme di pui ',
    'message_link' => 'clicca qui.',
    'message_more' => 'Chiudendo questo banner acconsenti all\'uso dei cookies.',
    'agree' => 'Acconsenti a tutti',
];
