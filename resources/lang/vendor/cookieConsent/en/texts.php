<?php

return [
    'message' => 'This website uses technical cookies to ensure the proper functioning of the procedures and improve the experience of using online applications. If you want to know more ',
    'message_link' => 'click here.',
    'message_more' => 'By closing this banner you consent to the use of cookies.',
    'agree' => 'Allow cookies',
];
