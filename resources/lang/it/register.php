<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'Registrati ora!',
    'first_name' => 'Nome',
    'error_first_name'=>'Si prega di inserire il nome.',
    'last_name' => 'Cognome',
    'error_last_name'=>'Si prega di inserire il cognome.',
    'country' => 'Paese',
    'error_country'=>'Si prega di compilare il paese.',
    'city' => 'Città',
    'error_city'=>'Per favore riempi la città.',
    'date_birth' => 'Data di nascita',
    'error_date_birth'=>'Si prega di inserire la data di nascita.',
    'email'=>'Indrizzo email',
    'error_email'=>'Si prega di compilare l\'email.',
    'password'=>'Crea una Password',
    'error_password'=>'Si prega di inserire la password.',
    'password_confirm'=>'Conferma password',
    'is_agree_terms1'=>'Continuando, accetti i',
    'is_agree_terms2'=>'Termini di utilizzo e',
    'is_agree_terms3'=>' confermi di aver lettob la',
    'is_agree_terms4'=>'Normativa sulla privacy e dell`uso dei cookie',
    'error_agree_terms'=>'Accetta i termini e le condizioni',
    'login'=>'Accedi',
    'register'=>'Registrati',
    'have_account'=>'Hai già un account?',
    'prefer_language'=>'Preferisci lingua',
    'error_prefer_language'=>'Nessun valore valido',
    'wrong_password'=>'La password deve essere lunga almeno 8 caratteri, contenere almeno 1 lettera maiuscola e almeno 1 numero'

];
