<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'Profilo',
    'first_name' => 'Nome',
    'error_first_name'=>'Si prega di inserire il nome.',
    'last_name' => 'Cognome',
    'error_last_name'=>'Si prega di inserire il cognome.',
    'country' => 'Paese',
    'error_country'=>'Si prega di compilare il paese.',
    'city' => 'Città',
    'error_city'=>'Per favore riempi la città.',
    'date_birth' => 'Data di nascita',
    'error_date_birth'=>'Si prega di inserire la data di nascita.',
    'email'=>'Indrizzo email',
    'error_email'=>'Si prega di compilare l\'email.',
    'password'=>'Crea una Password',
    'error_password'=>'Si prega di inserire la password.',
    'password_confirm'=>'Conferma password',
    'is_agree_terms1'=>'Continuando, accetti i',
    'is_agree_terms2'=>'Termini di utilizzo e',
    'is_agree_terms3'=>' confermi di aver lettob la',
    'is_agree_terms4'=>'Normativa sulla privacy e dell`uso dei cookie',
    'error_agree_terms'=>'Accetta i termini e le condizioni',
    'login'=>'Accedi',
    'register'=>'Registrati',
    'have_account'=>'Hai già un account?',
    'prefer_language'=>'Preferisci lingua',
    'error_prefer_language'=>'Nessun valore valido',
    'profile_update'=>'Aggiornare',
    'old_password'=>'Vecchia password',
    'new_password'=>'Nuova password',
    'confirm_new_password'=>'Conferma la nuova password',
    'profile_change'=>'Il profilo è stato aggiornato',
    'book_corner_posts'=>'Pali d\'angolo dei libri',
    'accomodations_posts'=>'Post di alloggi',
    'not_posts'=>'Non post',
    'id'=>'ID',
    'title'=>'Titolo',
    'status'=>'Stato',
    'date'=>'Data',
    'actions'=>'Azioni',
    'enable'=>'Attivo',
    'disable'=>'Non attivo',
    'on_moderate'=>'Su moderato',
    'error_post_not_found'=>'Post di errore di ups non trovato',
    'did_you_want_delete'=>'Volevi eliminare il post?',
    'news_posts'=>'Post di notizie',
    'blogs_posts'=>'Post di blog',
    'seo_profile_title'=>'Profilo',
    'seo_profile_description'=>'Profilo utente',
    'seo_profile_keywords'=>'Utente, profilo, suggerimenti, università',
    'seo_title_edit_books_corner_post'=>'Modifica post d\'angolo dei libri',
    'seo_description_edit_books_corner_post'=>'Profilo utente: modifica post d\'angolo del libro',
    'seo_keywords_edit_books_corner_posts'=>'Utente, profilo, modifica, post, suggerimenti, università',

    'seo_title_edit_accommodation_post'=>'Modifica il post dell\'alloggio',
    'seo_description_edit_accommodation_post'=>'Profilo utente - modifica il post dell\'alloggio',
    'seo_keywords_edit_accommodation_posts'=>'Utente, profilo, alloggio, modifica, post, consigli, università',
    'seo_title_edit_blog_post'=>'Modifica il post del blog',
    'seo_description_edit_blog_post'=>'Profilo utente: modifica il post del blog',
    'seo_keywords_edit_blog_posts'=>'User, Profile , blog, edit, post, tips, university',
    'seo_title_edit_news_post'=>'Modifica post di notizie',
    'seo_description_edit_news_post'=>'Profilo utente: modifica post di notizie',
    'seo_keywords_edit_news_posts'=>'Utente, profilo, notizie, modifica, post, suggerimenti, università',
    'edit_book_corner_post'=>'Modifica post d\'angolo del libro',
    'edit_accommodations_post'=>'Modifica post di alloggio',
    'edit_blog_post'=>'Modifica il post del blog',
    'edit_news_post'=>'Modifica post di notizie',


];
