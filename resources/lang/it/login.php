<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Login Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'return_hello' => 'Bentornato. Accedi!',
    'email' => 'Indirizzo email',
    'password'=>'Password',
    'email_invalid_feedback'=>'Per favore inserisci un\'email valida.',
    'pass_invalid_feedback'=>'Si prega di fornire una password valida.',
    'log_in'=>'Accedi',
    'forgot_password'=>'Password dimenticata ?',
    'not_account'=>'Non hai un account?',
    'registration'=>'Registrati',
    'is_agree_terms1'=>'Continuando, accetti i',
    'is_agree_terms2'=>'Termini di utilizzo e',
    'is_agree_terms3'=>' confermi di aver lettob la',
    'is_agree_terms4'=>'Normativa sulla privacy e dell`uso dei cookie',

];
