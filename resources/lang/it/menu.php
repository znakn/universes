<?php
return [
    'tips'=>'Tips',
    'books_corner'=>'Angolo del libro',
    'logistica'=>'Alloggi',
    'social_blog'=>'Blog',
    'tips_news'=>'Tip news',
    'info'=>'Info',
    'profile'=>'Profilo',
    'go_back'=>'Torna indietro'
];
