<?php
return [
    'placeholder_region'=>'Seleziona la regione',
    'placeholder_province'=>'Seleziona Province',
    'placeholder_city'=>'Seleziona Città',
    'placeholder_type'=>'Seleziona Tipo',
    'placeholder_section'=>'Seleziona Sezione',
    'placeholder_logistica_type'=>'Select Type'
];
