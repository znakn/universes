<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'Registration',
    'first_name' => 'First name',
    'error_first_name'=>'Please fill the first name.',
    'last_name' => 'Last name',
    'error_last_name'=>'Please fill the last name.',
    'country' => 'Country',
    'error_country'=>'Please fill the country.',
    'city' => 'City',
    'error_city'=>'Please fill the city.',
    'date_birth' => 'Date birth',
    'error_date_birth'=>'Please fill the date birth.',
    'email'=>'Email',
    'error_email'=>'Please fill the email',
    'password'=>'Password',
    'error_password'=>'Please fill the password',
    'password_confirm'=>'Confirm password',
    'is_agree_terms1'=>'By continuing, you accept the ',
    'is_agree_terms2'=>'Terms of Use and confirm ',
    'is_agree_terms3'=>'that you have read',
    'is_agree_terms4'=>'the Privacy and Cookie Policy',
    'erro_agree_terms'=>'Please agree with terms and conditions',
    'login'=>'Log in',
    'register'=>'Register',
    'have_account'=>'Are you have account ?',
    'prefer_language'=>'Prefer language',
    'error_prefer_language'=>'No valid value',
    'wrong_password'=>'Password must be at least 8 characters long, have at least 1 capital letter and at least 1 number.'




];
