<?php
return [
    'placeholder_region'=>'Select Region',
    'placeholder_province'=>'Select Province',
    'placeholder_city'=>'Select City',
    'placeholder_type'=>'Select Type',
    'placeholder_section'=>'Select Section',
    'placeholder_logistica_type'=>'Select Type'
];
