<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Login Language Lines
    |--------------------------------------------------------------------------
    |
    |
    */

    'return_hello' => 'Welcome back. Log in!',
    'email' => 'Email address',
    'password'=>'Password',
    'email_invalid_feedback'=>'Please provide a valid email.',
    'pass_invalid_feedback'=>'Please provide a valid password.',
    'log_in'=>'Log in',
    'forgot_password'=>'Forgot password ?',
    'not_account'=>'Do not have an account?',
    'registration'=>'Sign in',
    'is_agree_terms1'=>'By continuing, you accept the ',
    'is_agree_terms2'=>'Terms of Use and confirm ',
    'is_agree_terms3'=>'that you have read',
    'is_agree_terms4'=>'the Privacy and Cookie Policy',

];
