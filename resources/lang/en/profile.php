<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'title' => 'Profile',
    'first_name' => 'First name',
    'error_first_name'=>'Please fill the first name.',
    'last_name' => 'Last name',
    'error_last_name'=>'Please fill the last name.',
    'country' => 'Country',
    'error_country'=>'Please fill the country.',
    'city' => 'City',
    'error_city'=>'Please fill the city.',
    'date_birth' => 'Date birth',
    'error_date_birth'=>'Please fill the date birth.',
    'email'=>'Email',
    'error_email'=>'Please fill the email',
    'password'=>'Password',
    'error_password'=>'Please fill the password',
    'password_confirm'=>'Confirm password',
    'is_agree_terms1'=>'By continuing, you accept the ',
    'is_agree_terms2'=>'Terms of Use and confirm ',
    'is_agree_terms3'=>'that you have read',
    'is_agree_terms4'=>'the Privacy and Cookie Policy',
    'erro_agree_terms'=>'Please agree with terms and conditions',
    'login'=>'Log in',
    'register'=>'Register',
    'have_account'=>'Are you have account ?',
    'prefer_language'=>'Prefer language',
    'error_prefer_language'=>'No valid value',
    'profile_update'=>'Update',
    'old_password'=>'Old password',
    'new_password'=>'New password',
    'confirm_new_password'=>'Confirm new password',
    'profile_change'=>'Profile was updated',
    'book_corner_posts'=>'Books corner posts',
    'accomodations_posts'=>'Accomodations posts',
    'not_posts'=>'Not posts',
    'id'=>'ID',
    'title'=>'Title',
    'status'=>'Status',
    'date'=>'Date',
    'actions'=>'Actions',
    'enable'=>'Active',
    'disable'=>'Not Active',
    'on_moderate'=>'On moderate',
    'error_post_not_found'=>'Ups error post not found',
    'did_you_want_delete'=>'Did you want delete post?',
    'news_posts'=>'News posts',
    'blogs_posts'=>'Blogs posts',
    'seo_profile_title'=>'Profile',
    'seo_profile_description'=>'User profile',
    'seo_profile_keywords'=>'User, Profile , tips, university',
    'seo_title_edit_books_corner_post'=>'Edit books corner post',
    'seo_description_edit_books_corner_post'=>'User profile - edit book corner post',
    'seo_keywords_edit_books_corner_posts'=>'User, Profile , edit, post, tips, university',
    'seo_title_edit_accomodation_post'=>'Edit accomodation post',
    'seo_description_edit_accomodation_post'=>'User profile - edit accomodation post',
    'seo_keywords_edit_accomodation_posts'=>'User, Profile , accomodation, edit, post, tips, university',
    'seo_title_edit_blog_post'=>'Edit blog post',
    'seo_description_edit_blog_post'=>'User profile - edit blog post',
    'seo_keywords_edit_blog_posts'=>'User, Profile , blog, edit, post, tips, university',
    'seo_title_edit_news_post'=>'Edit news post',
    'seo_description_edit_news_post'=>'User profile - edit news post',
    'seo_keywords_edit_news_posts'=>'User, Profile , news, edit, post, tips, university',
    'edit_book_corner_post'=>'Edit book corner post',
    'edit_accommodations_post'=>'Edit accommodation post',
    'edit_blog_post'=>'Edit blog post',
    'edit_news_post'=>'Edit news post',




];
