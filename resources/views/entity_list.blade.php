@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url, 'menu' => $menu])

@section('content')
    <!-- Search form and regions menu -->
    <div class="wrap-bg-beige-2">
        <div class="container">
            <div class="row regions pb-3">
                <!-- Regione links -->
                <div class="region-title">{{__('global.regions')}}</div>
                <div class="w-100 d-block d-sm-none"></div>
                @foreach($regions as $region)
                    <a href="{{LaravelLocalization::localizeUrl('/'.$program->url.'/'.$region['url'].'/list')}}" class="region-link @if((isset($region_url))&&($region['url']==$region_url)) active  @endif">
                        <span>{{$region['name']}}</span>
                        <!-- <img src="../img/reg-1.svg" alt="" class="img-fluid region-img region-img-1"> -->
                        <svg version="1.1" class="img-fluid region-img item-{{$region['svg_order']}}" xmlns="http://www.w3.org/2000/svg" viewBox="{{$region['svg_view_box']}}" style="{{$region['svg_style']}}">
                            <path d="{{$region['svg']}}">
                            </path>
                        </svg>
                    </a>
                @endforeach
            </div>
            <div class="row justify-content-center py-3">
                <div class="col col-lg-8">
                    <!-- Search form -->
                    @include('parts/search_entity_form')
                </div>
            </div>
        </div>
    </div>

    <!-- Education item cards -->
    <div class="wrap-bg-beige-6">
        <div class="container">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 py-5">
                @foreach($entities as $entity)
                @php $rating = $entity->getRating(); @endphp
                <div class="col card-edu-wrap">
                    <div class="card card-edu-block text-in-theme-color card-edu-color-1">
                        <a href="{{LaravelLocalization::localizeUrl('/'.$program->url.'/entity/'.$entity->url)}}">
                        @if($entity->image)
                        <img src="{{'/storage/'.$entity->thumbnail('icon','logo')}}"  alt="Img" class="card-img-top">
                        @else
                        <img src="https://via.placeholder.com/50/" alt="Img" class="card-img-top">
                        @endif
                        <div class="card-body">
                            <a href="{{LaravelLocalization::localizeUrl('/'.$program->url.'/entity/'.$entity->url)}}">
                                <h5 class="card-title">{{$entity->{'name_'.app()->getLocale()} }}</h5>
                            </a>
                            <div class="card-text">
                                <address>
                                    <b>{{$entity->city->{'name_'.app()->getLocale()} }}</b> {{$entity->address}}
                                </address>
                                @if($entity->phone)
                                tel: <a target="_blank" href="tel:{{$entity->phone}}">{{$entity->phone}}</a> <br>
                                @endif
                                @if($entity->email)
                                <a target="_blank" href="mailto:{{$entity->email}}">{{$entity->email}}</a><br>
                                @endif
                                @if($entity->web_address)
                                <a href="@if(mb_strpos($entity->web_address,'http') !== false) {{$entity->web_address}} @else https://{{$entity->web_address}} @endif" target="_blank"><b>{{$entity->web_address}}</b></a>
                                @endif
                            </div>
                            <div class="text-right card-edu-stars mt-2">
                                <span class="star-output" id="starOut-{{$entity->id}}">{{$rating['rating']}}</span>
                                <div class="stars-block d-inline-block">
                                    <input type="range" disabled name="stars" min="0" max="5" step=".1" value="{{$rating['rating']}}" class="stars-input" data-target="starOut-{{$entity->id}}">
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                </div>

                @endforeach

            </div>
            <div class="row pb-4">
                <div class="col" >
                    <nav aria-label="Page navigation">
                        {{$entities->links()}}
                    </nav>
                </div>
            </div>

        </div>
    </div>


    <!-- Text block -->
    {{--<div class="wrap-bg-white-1">--}}
        {{--<div class="container">--}}
            {{--<div class="text-block py-5">--}}
                {{--<h1 class="h2 title">Lorem ipsum dolor sit amet.</h1>--}}
                {{--<div class="text">Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quaerat aliquid similique quos, hic esse repellendus placeat veniam alias, atque, aspernatur omnis, assumenda beatae temporibus aliquam non expedita recusandae iure perspiciatis cumque fugiat eveniet explicabo. Repellendus, quis facere blanditiis?</div>--}}
                {{--<p>Lorem, ipsum, dolor sit amet consectetur adipisicing elit. Dolorum sint at corrupti placeat provident quae?</p>--}}
                {{--Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, dolore impedit rem itaque voluptas odit animi rerum magni.--}}
                {{--<br>--}}
                {{--Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quae hic similique aspernatur perferendis dolor deleniti eveniet cumque est eos porro. Illo, nisi incidunt unde quo quasi cumque! A ullam consequatur, unde quasi, perspiciatis ut ipsam voluptate repellat, animi minima ducimus iste ea ex nesciunt expedita facere? Omnis sapiente error, fugiat commodi nisi hic nemo ipsum cupiditate eaque ad, natus suscipit repellat eos expedita deserunt ullam rem nobis! Dolores, vel nesciunt, sapiente aliquam ipsa nulla! Architecto repellendus, ea, nemo impedit sapiente pariatur magnam asperiores.--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="wrap-bg-beige-3">
        <div class="container">
            <div class="row">
                {{--<div class="col-12 col-sm">--}}
                    {{--<div class="text-block py-5">--}}
                        {{--<h2 class="h2 title">Lorem ipsum dolor sit amet.</h2>--}}
                        {{--<div class="text">Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quaerat aliquid similique quos, hic esse repellendus placeat veniam alias, atque, aspernatur omnis, assumenda beatae temporibus aliquam non expedita recusandae iure perspiciatis cumque fugiat eveniet explicabo. Repellendus, quis facere blanditiis?</div>--}}
                        {{--<p>Lorem, ipsum, dolor sit amet consectetur adipisicing elit. Dolorum sint at corrupti placeat provident quae?</p>--}}
                        {{--Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, dolore impedit rem itaque voluptas odit animi rerum magni.--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-12 col-sm">--}}
                    {{--<div class="text-block py-5">--}}
                        {{--<h2 class="h2 title">Lorem ipsum dolor sit amet.</h2>--}}
                        {{--<div class="text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut doloremque exercitationem itaque nemo at aspernatur harum repellat delectus ex numquam, laborum voluptatibus ab odit maxime, quisquam sit ipsam saepe hic animi laboriosam eligendi ipsa obcaecati. Vitae, impedit. Porro veritatis dolor unde ab molestias esse voluptates ut alias a delectus eius qui laboriosam obcaecati repellat ratione aliquam, nam inventore, architecto natus ea, distinctio. Quas illo, voluptatem vel molestiae ex voluptatibus itaque maxime esse, reiciendis reprehenderit, expedita obcaecati accusantium blanditiis nulla? Possimus dicta, consequatur. Maiores libero numquam repudiandae consectetur, voluptatem nam rerum dicta architecto necessitatibus dignissimos, totam a officiis perferendis iusto. Esse sint architecto rem magni eos mollitia obcaecati provident animi, tempore quasi quibusdam incidunt, rerum labore itaque inventore repellat qui. Quaerat!--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>

@endsection
