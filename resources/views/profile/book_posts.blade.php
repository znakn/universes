@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\BooksCornerPosts;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')
<div class="container">
    @if($profileMessage)
    <div class="row">
        <div class="col-12 my-5" >
            <h3 style="color: red">{{$profileMessage}}</h3>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-4 col-md-4 col-lg-4 col-xl-4 my-5" >
            <h5>{{_('global.menu')}}</h5>
            @include('parts/profile_menu')
        </div>
        <div class="col-8 col-md-8 col-lg-8 col-xl-8 my-5">
            <input type="hidden" id="label_del_question" value="{{__('profile.did_you_want_delete')}}">
            <h5>{{__('profile.book_corner_posts')}}</h5>
            @if($posts->isNotEmpty())
                <table class="table table-bordered">
                   <tr>
                       <th>{{__('profile.id')}}</th>
                       <th>{{__('profile.title')}}</th>
                       <th>{{__('profile.date')}}</th>
                       <th>{{__('profile.status')}}</th>
                       <th>{{__('profile.actions')}}</th>
                   </tr>

                @foreach($posts as $post)
                    <tr>
                        <td>{{$post->id}}</td>
                        <td>{{$post->{'title_'.$locale} }}</td>
                        <td>{{ date('d.m.Y',strtotime($post->created_at)) }}</td>
                        <td>{{$post->getStatusAsText()}}</td>
                        <td>
                            <a href="{{'/profile/post-edit/'.BooksCornerPosts::FORM_TYPE.'/'.$post->id}}"><img src="{{'/img/document-edit.svg'}}" width="20px" ></a>
                            @if($post->status == BooksCornerPosts::ENABLED)
                                <a href="{{LaravelLocalization::localizeUrl('/book-corner-post/'.$post->url)}}" target="_blank" ><img src="{{'/img/preview.svg'}}" width="20px" ></a>
                            @else
                                <a href="{{LaravelLocalization::localizeUrl('/book-corner-post/preview/'.$post->url)}}" target="_blank" ><img src="{{'/img/preview.svg'}}" width="20px" ></a>
                            @endif
                            <a href="#" class="btn-profile-del" data-id="{{$post->id}}" data-type="book_posts"  ><img src="{{'/img/trash.svg'}}" width="20px" ></a>
                        </td>
                    </tr>

                @endforeach

                </table>
                <br/>
                {{$posts->links()}}
            @else
                {{__('profile.not_posts')}}
            @endif


        </div>
    </div>
</div>

@endsection
