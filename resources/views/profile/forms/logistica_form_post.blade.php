@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    $has_province = ($post->province_id) ? 'true':'false';
    $has_city = ($post->city_id) ? 'true':'false';
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url,'post'=>$post ])

@section('content')
    <!-- Recencia library -->
    <div class="wrap-bg-beige-3">
        <div class="container">
            <form action="{{LaravelLocalization::localizeUrl('/profile/profile-update')}}" method="post" >
                <input type="hidden" name="form_type" id="form_type" value="{{$form_type}}">
                <input type="hidden" name="_token" value="{{@csrf_token()}}" />
                <input type="hidden" name="id" id="id" value="{{ $post->id }}">
                <input type="hidden" id="placeholder_logistica_type" value="{{__('search.placeholder_logistica_type')}}" >
                <input type="hidden" id="placeholder_region" value="{{__('search.placeholder_region')}}">
                <input type="hidden" id="placeholder_province" value="{{__('search.placeholder_province')}}">
                <input type="hidden" id="placeholder_city" value="{{__('search.placeholder_city')}}">
                <input type="hidden" id="has_provinces" value="{{$has_province}}" >
                <input type="hidden" id="has_city" value="{{$has_city}}" >

                <div class="row justify-content-center py-4">
                    <div class="col-12 col-md-8">
                        <div class="form-group">
                            <h1>{{$form_title}}</h1>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('logistica_type_id') ? ' has-error' : '' }}">
                            <label>{{__('global.type')}}</label>
                            <select name="logistica_type_id" id="logistica_type_id" class="form-control @if($errors->has('logistica_type_id')) is-invalid  @endif">
                                <option value=""></option>
                                @if($logisticaTypes->isNotEmpty())
                                    @foreach($logisticaTypes as $logisticaType)
                                        @if($type_id == $logisticaType->id)
                                            <option value="{{$logisticaType->id}}" selected="selected">{{$logisticaType->{'name_'.$locale} }}</option>
                                        @else
                                            <option value="{{$logisticaType->id}}">{{$logisticaType->{'name_'.$locale} }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }} ">
                            <label>{{__('global.region')}}</label>
                            <select class="form-control @if($errors->has('region_id')) is-invalid  @endif" name="region_id"  id="logistica_regions" aria-label="Region"   >
                                <option value=""></option>
                                @foreach($regions as $region)
                                    @if(isset($region_id))
                                            <option value="{{$region['id']}}" @if ($region_id == $region['id']) selected="selected" @endif data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                                    @else
                                        <option value="{{$region['id']}}" data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('province_id') ? ' has-error' : '' }} ">
                            <label>{{__('global.province')}}</label>
                            <select class="form-control @if($errors->has('province_id')) is-invalid  @endif" name="province_id"  id="logistica_provinces" aria-label="Province"   >
                                <option value=""></option>
                                @if(isset($provinces))
                                @foreach($provinces as $province)
                                    @if(isset($province_id))
                                        <option value="{{$province['id']}}" @if ($province_id == $province['id']) selected="selected" @endif data-province_id="{{$province['id']}}"  >{{$province['name']}}</option>
                                    @else
                                        <option value="{{$province['id']}}" data-province_id="{{$province['id']}}"  >{{$province['name']}}</option>
                                    @endif
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }} ">
                            <label>{{__('global.cities')}}</label>
                            <select id="logistica_city" aria-label="City" name="city_id" class="form-control @if($errors->has('city_id')) is-invalid  @endif">
                                <option value=""></option>
                                @if(isset($cities))
                                    @foreach($cities as $city)
                                        @if($city_id)
                                            <option value="{{$city['id']}}" data-city_id="{{$city['id']}}" @if($city_id == $city['id']) selected="selected" @endif >{{$city['name']}}</option>
                                        @else
                                            <option value="{{$city['id']}}" data-city_id="{{$city['id']}}"  >{{$city['name']}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                            <label>{{__('global.title')}}</label>
                            <input type="text" aria-label="Title" name="title" value="{{old('title')?old('title'):$post->{'title_'.$locale} }}"  placeholder="{{__('global.title')}}" class="form-control @if($errors->has('title')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                            <label>{{__('global.text')}}</label>
                            <textarea aria-label="text" name="text" id="text"  placeholder="{{__('global.text')}}" class="form-control @if($errors->has('text')) is-invalid  @endif">
                                {{old('text')?old('text'):$post->{'text_'.$locale} }}
                            </textarea>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>{{__('global.contact_email')}}</label>
                            <input type="text" aria-label="Contact Email" name="email" value="{{old('email')?old('email'):$post->email}}"   placeholder="{{__('global.contact_email')}}" class="form-control @if($errors->has('email')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label>{{__('global.contact_phone')}}</label>
                            <input type="text" aria-label="Contact Phone" name="phone" value="{{old('phone')?old('phone'):$post->phone}}"   placeholder="{{__('global.contact_phone')}}" class="form-control @if($errors->has('phone')) is-invalid  @endif">
                        </div>
                        <div class="form-group" >
                            <label>{{__('global.gallery')}}</label>
                            <div class="needsclick dropzone" id="gallery-dropzone">

                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit"  value="{{__('global.save')}}" class="btn btn-main btn-lg btn-block my-1">
                        </div>
                    </div>
                </div>

            </form>
    </div>


    <!-- Advertising -->
    <div class="wrap-bg-beige-3">
        <div class="container">
            <div class="row py-2">
                <div class="col">
                    <div class="advertising my-4"></div>
                </div>
            </div>
        </div>
    </div>

    @endsection

    <script src="{{asset('/vendor/ckeditor/ckeditor.js')}}"></script>
    @section('image-script')

            <script src="{{asset('/js/dropzone/dist/dropzone.js')}}"></script>
            <link rel="stylesheet" href="{{asset('/js/dropzone/dist/dropzone.css')}}">

            <script>

                Dropzone.autoDiscover = false;

                var uploadedDocumentMap = {};
                var dropGallery = new  Dropzone("#gallery-dropzone", {
                    url: '{{ route('form-file-upload') }}',
                    maxFiles: 5, // MB
                    addRemoveLinks: true,
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    success: function (file, response) {
                        $('form').append('<input type="hidden" name="gallereies[]" value="' + response.name + '">');
                        uploadedDocumentMap[file.name] = response.name;
                    },
                    removedfile: function (file) {
                        file.previewElement.remove();
                        var name = '';
                        if (typeof file.file_name !== 'undefined') {
                            name = file.file_name;
                        } else {
                            name = uploadedDocumentMap[file.name];
                        }
                        $('form').find('input[name="gallereies[]"][value="' + name + '"]').remove();
                    },
                    init: function () {
                        this.on("sending", function(file, xhr, formData){
                            formData.append("image_type", "logistica_gallery");
                        });
                                @if(isset($post->images) && $post->images)
                        var files = {!! json_encode($post->images) !!};

                        for (var i in files) {
                            var file = files[i];

                            var mockFile = { name: file.file_name, size: file.file_size, type: 'image/jpeg' };
                            this.emit("addedfile", mockFile);
                            this.emit("thumbnail", mockFile, file.path+file.file_name)
                            {
                                $('[data-dz-thumbnail]').css('height', '120');
                                $('[data-dz-thumbnail]').css('width', '120');
                                $('[data-dz-thumbnail]').css('object-fit', 'cover');
                            };
                            this.emit('complete', mockFile);
                            this.files.push(mockFile);
                            uploadedDocumentMap[file.file_name] = file.file_name;
                            //
                            $('form').append('<input type="hidden" name="gallereies[]" value="' + file.file_name + '">');
                        }
                        @endif
                    }
                });
            </script>




            <script>


            CKEDITOR.replace('text',{
                toolbarGroups : [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    '/',
                    { name: 'styles', groups: [ 'styles' ] },
                 //   { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'others', groups: [ 'others' ] },
                 //   { name: 'about', groups: [ 'about' ] }
                ]
            });


        </script>
    <!--------Crop script ------------------->



@endsection
