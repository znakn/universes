@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    $section_id = old('section_id')?old('section_id') : $post->section_id;
    $type_id = old('type_id')?old('type_id') : $post->type_id;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url])

@section('content')
    <!-- Recencia library -->
    <div class="wrap-bg-beige-3">
        <div class="container">
            <form action="{{LaravelLocalization::localizeUrl('/profile/profile-update')}}" method="post" >
                <input type="hidden" name="form_type" id="form_type" value="{{$form_type}}">
                <input type="hidden" name="id" id="id" value="{{ $post->id }}">
                <input type="hidden" name="_token" value="{{@csrf_token()}}" />
                <div class="row justify-content-center py-4">
                    <div class="col-12 col-md-8">
                        <div class="form-group">
                            <h1>{{$form_title}}</h1>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('section_id') ? ' has-error' : '' }}">
                            <label>{{__('global.section')}}</label>
                            <select name="section_id" class="form-control @if($errors->has('section_id')) is-invalid  @endif ">
                                <option value=""></option>
                                @if($booksCornerSections->isNotEmpty())
                                    @foreach($booksCornerSections as $booksCornerSection)
                                        @if($section_id == $booksCornerSection->id)
                                            <option value="{{$booksCornerSection->id}}" selected="selected" >{{$booksCornerSection->{'title_'.$locale} }}</option>
                                        @else
                                            <option value="{{$booksCornerSection->id}}">{{$booksCornerSection->{'title_'.$locale} }}</option>
                                        @endif

                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group {{ $errors->has('type_id') ? ' has-error' : '' }}">
                            <label>{{__('global.type')}}</label>
                            <select name="type_id" class="form-control @if($errors->has('type_id')) is-invalid  @endif ">
                                <option value=""></option>
                                @if($booksCornerTypes->isNotEmpty())
                                    @foreach($booksCornerTypes as $booksCornerType)
                                        @if($type_id == $booksCornerType->id)
                                            <option value="{{$booksCornerType->id}}" selected="selected" >{{$booksCornerType->{'name_'.$locale} }}</option>
                                        @else
                                            <option value="{{$booksCornerType->id}}">{{$booksCornerType->{'name_'.$locale} }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                            <label>{{__('global.title')}}</label>
                            <input type="text" aria-label="Title" name="title" value="{{old('title')?old('title'):$post->{'title_'.$locale} }}"  placeholder="{{__('global.title')}}" class="form-control @if($errors->has('title')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                            <label>{{__('global.text')}}</label>
                            <textarea aria-label="text" name="text" id="text"  placeholder="{{__('global.text')}}" class="form-control @if($errors->has('text')) is-invalid  @endif">
                                {{old('text')?old('text'):$post->{'text_'.$locale} }}
                            </textarea>
                        </div>
                        <div class="form-group {{ $errors->has('curse') ? ' has-error' : '' }}">
                            <label>{{__('global.curse_matheria')}}</label>
                            <input type="text" aria-label="Curse/Matheria" name="curse" value="{{old('curse')?old('curse'):$post->curse}}"  placeholder="{{__('global.curse_matheria')}}" class="form-control @if($errors->has('curse')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('author') ? ' has-error' : '' }}">
                            <label>{{__('global.author')}}</label>
                            <input type="text" aria-label="Author" name="author" value="{{old('author')?old('author'):$post->author}}"  placeholder="{{__('global.author')}}" class="form-control @if($errors->has('author')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>{{__('global.contact_email')}}</label>
                            <input type="text" aria-label="Contact Email" name="email" value="{{old('email')?old('email'):$post->email}}"   placeholder="{{__('global.contact_email')}}" class="form-control @if($errors->has('email')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label>{{__('global.contact_phone')}}</label>
                            <input type="text" aria-label="Contact Phone" name="phone" value="{{old('phone')?old('phone'):$post->phone}}"   placeholder="{{__('global.contact_phone')}}" class="form-control @if($errors->has('phone')) is-invalid  @endif">
                        </div>
                        <div class="form-group">
                            <label>{{__('global.preview_image')}}</label>
                            <div class="needsclick dropzone" id="image-dropzone">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit"  value="{{__('global.save')}}" class="btn btn-main btn-lg btn-block my-1">
                        </div>
                    </div>
                </div>

            </form>
    </div>


    <!-- Advertising -->
    <div class="wrap-bg-beige-3">
        <div class="container">
            <div class="row py-2">
                <div class="col">
                    <div class="advertising my-4"></div>
                </div>
            </div>
        </div>
    </div>


    @endsection

    <script src="{{asset('/vendor/ckeditor/ckeditor.js')}}"></script>
    @section('image-script')
    <!------- Crop script ------------------->
        <script src="{{asset('/js/dropzone/dist/dropzone.js')}}"></script>
        <link rel="stylesheet" href="{{asset('/js/dropzone/dist/dropzone.css')}}">

        <script>

            Dropzone.autoDiscover = false;
            var dropGallery = new  Dropzone("#image-dropzone", {
                url: '{{ route('form-file-upload') }}',
                maxFiles: 1, // MB
                addRemoveLinks: true,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                success: function (file, response) {
                    $('form').append('<input type="hidden" name="upload_image" value="' + response.name + '">');
                },
                removedfile: function (file) {
                    file.previewElement.remove();
                    var name = '';
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name;
                    }
                    $('form').find('input[name="upload_image"][value="' + name + '"]').remove();
                },
                init:function(){
                    this.on("sending", function(file, xhr, formData){
                        formData.append("image_type", "book_corner_image");
                    });
                            @if(isset($post->image) && $post->image)
                    var file = '{!! $post->image !!}';
                    var file_size = '{!! \App\Helpers\Thumbs::getImageSize(\App\BooksCornerPosts::FORM_TYPE,$post->image) !!}';
                    var path = '{!! \App\BooksCornerPosts::GALLERY_PATH_EXT !!}';
                    var mockFile = { name: file, size: file_size, type: 'image/jpeg' };

                    this.emit("addedfile", mockFile);
                    this.emit("thumbnail", mockFile, path+file)
                    {
                        $('[data-dz-thumbnail]').css('height', '120');
                        $('[data-dz-thumbnail]').css('width', '120');
                        $('[data-dz-thumbnail]').css('object-fit', 'cover');
                    };
                    this.emit('complete', mockFile);
                    this.files.push(mockFile);

                    
                    $('form').append('<input type="hidden" name="upload_image" value="' + file + '">');
                    @endif
                }
            });

            CKEDITOR.replace('text',{
                toolbarGroups : [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    '/',
                    { name: 'styles', groups: [ 'styles' ] },
                 //   { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'others', groups: [ 'others' ] },
                 //   { name: 'about', groups: [ 'about' ] }
                ]
            });



        </script>
    <!--------Crop script ------------------->



@endsection
