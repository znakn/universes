@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\Blog;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')
<div class="container">
    @if($profileMessage)
    <div class="row">
        <div class="col-12 my-5" >
            <h3 style="color: red">{{$profileMessage}}</h3>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-4 col-md-4 col-lg-4 col-xl-4 my-5" >
            <h5>{{_('global.menu')}}</h5>
            @include('parts/profile_menu')
        </div>
        <div class="col-8 col-md-8 col-lg-8 col-xl-8 my-5">
            <h5>{{__('menu.profile')}}</h5>

            <form class="form-horizontal needs-validation" method="POST" action="{{ route('profile_update') }}" novalidate><!-- was-validated -->
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">{{__('profile.first_name')}}</label>
                    <input id="name" type="text" class="form-control @if($errors->has('name')) is-invalid  @endif" name="name" value="{{ old('name')?old('name'):$user->name }}" required placeholder="{{__('profile.first_name')}}">
                    <span class="invalid-feedback">
                    @if ($errors->has('name'))
                        {{ $errors->first('name') }}<br>
                    @else
                        {{__('profile.error_first_name')}}<br>
                    @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="last_name">{{__('profile.last_name')}}</label>
                    <input id="last_name" type="text" class="form-control @if($errors->has('last_name')) is-invalid  @endif" name="last_name" value="{{ old('last_name')?old('last_name'):$user->last_name }}" required placeholder="{{__('profile.last_name')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('last_name'))
                            {{ $errors->first('last_name') }}<br>
                        @else
                            {{__('profile.error_last_name')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                    <label for="country">{{__('profile.country')}}</label>
                    <input id="country" type="text" class="form-control @if($errors->has('country')) is-invalid  @endif" name="country" value="{{ old('country')?old('country'):$user->country }}" required placeholder="{{__('profile.country')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('country'))
                            {{ $errors->first('country') }}<br>
                        @else
                            {{__('profile.error_country')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    <label for="profile_city">{{__('profile.city')}}</label>
                    <input id="profile_city" type="text" class="form-control @if($errors->has('city')) is-invalid  @endif  " name="city" value="{{ old('city')?old('city'):$user->city }}" required placeholder="{{__('profile.city')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('city'))
                            {{ $errors->first('city') }}<br>
                        @else
                            {{__('profile.error_city')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('date_birth') ? ' has-error' : '' }}">
                    <label for="date_birth">{{__('profile.date_birth')}}</label>
                    <input id="date_birth" type="text" class="form-control @if($errors->has('date_birth')) is-invalid  @endif  " name="date_birth" value="{{ old('date_birth')?old('date_birth'):$user->date_birth }}" required placeholder="{{__('profile.date_birth')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('date_birth'))
                            {{ $errors->first('date_birth') }}<br>
                        @else
                            {{__('profile.date_birth')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('prefer_language') ? ' has-error' : '' }}">
                    <label for="prefer_language">{{__('profile.prefer_language')}}</label>
                    <select id="prefer_language" class="form-control @if($errors->has('prefer_language')) is-invalid @endif " name="prefer_language"  placeholder="{{__('profile.prefer_language')}}">
                    <option value="it" @if ((old('prefer_language')||$user->prefer_language)&&((old('prefer_language') == 'it')||($user->prefer_language =='it'))) selected="selected" @endif   >IT</option>
                    <option value="en" @if ((old('prefer_language')||$user->prefer_language)&&((old('prefer_language') == 'en')||($user->prefer_language =='en'))) selected="selected" @endif   >EN</option>
                    </select>
                        <span class="invalid-feedback">
                    @if ($errors->has('prefer_language'))
                            {{ $errors->first('prefer_language') }}<br>
                        @else
                            {{__('profile.error_prefer_language')}}<br>
                        @endif
                    </span>
                </div>


                <div class="form-group password-old {{ $errors->has('old_password') ? ' has-error' : '' }}">
                    <label for="old_password">{{__('profile.old_password')}}</label>
                    <input id="old_password" type="password" class="form-control @if($errors->has('old_password')) is-invalid @endif " name="old_password" placeholder="{{__('profile.old_password')}}">
                    <a href="#" class="password-control"></a>
                    <span class="invalid-feedback">
                        @if ($errors->has('old_password'))
                            {{ $errors->first('old_password') }}<br>
                        @else
                            {{__('profile.old_password')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group password {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">{{__('profile.new_password')}}</label>
                    <input id="password" type="password" class="form-control @if($errors->has('password')) is-invalid @endif " name="password" placeholder="{{__('profile.new_password')}}">
                    <a href="#" class="password-control"></a>
                    <span class="invalid-feedback">
                        @if ($errors->has('password'))
                            {{ $errors->first('password') }}<br>
                        @else
                            {{__('profile.error_password')}}<br>
                        @endif
                    </span>
                </div>


                <div class="form-group password-conf">
                    <label for="password_confirmation">{{__('profile.confirm_new_password')}}</label>
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="{{__('profile.confirm_new_password')}}">
                    <a href="#" class="password-control"></a>
                </div>



                <div class="form-group">
                    <button type="submit" class="btn btn-dark btn-lg btn-block">{{__('profile.profile_update')}}</button>
                </div>


            </form>
        </div>
    </div>
</div>
@endsection
