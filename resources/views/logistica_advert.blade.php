@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\LogisticaPosts;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row pt-3 pb-5">
                <div class="col-12 pb-4">
                    <h1 class="h1">{!! $advert->{'title_'.$locale}  !!}</h1>
                </div>
                <div class="col-12 col-sm-4">
                    <img src="{{'/upload/'.LogisticaPosts::FORM_TYPE.'/main_'.$advert->image}}" alt="Img" class="img-fluid">


                    @if($advert->gallereies)
                    <div class="gall">
                        <div class="row">
                        @foreach($advert->gallereies as $key => $photo)
                            <div class="galleries-image in-row">
                                <img src="{{\App\LogisticaPosts::GALLERY_PATH_EXT.$photo}}"   class="img-fluid" alt="galleries_image_{{$key}}" data-toggle="modal" data-target="#exampleModal" onclick="$('#carouselAccomodation').carousel({{$key}});">
                            </div>
                        @endforeach
                        </div>
                    </div>

                    <div class="modal" id="exampleModal" tabindex="-1">
                        <div class="modal-dialog modal-dialog-centered modal-xl">
                            <div class="modal-content bg-transparent border-0">
                                <div class="modal-body p-0">

                                    <div id="carouselAccomodation" class="carousel slide carousel-in-modal" data-touch="true" data-interval="false">
                                        <div class="carousel-inner">
                                            @foreach($advert->gallereies as $key => $photo)
                                                <div class="carousel-item @if($key == 0) active @endif">
                                                    <!-- Here is some test text {{$key}} -->
                                                    <img src="{{\App\LogisticaPosts::GALLERY_PATH_EXT.$photo}}" class="d-block mx-auto" alt="galleries_image_{{$key}}">
                                                </div>
                                            @endforeach
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselAccomodation" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">{{__('global.previous')}}</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselAccomodation" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">{{__('global.next')}}</span>
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-12 col-sm-8">
                    {!! $advert->{'text_'.$locale} !!}
                    <dl class="row">
                        <dt class="col-4">{{__('global.type')}}</dt>
                        <dd class="col-8">{{$advert->type->{'name_'.$locale} }}</dd>
                        @if(isset($advert->region)&&($advert->region))
                        <dt class="col-4">{{__('global.region')}}</dt>
                        <dd class="col-8">{{$advert->region->{'name_'.$locale} }}</dd>
                        @endif
                        @if(isset($advert->province)&&($advert->province))
                        <dt class="col-4">{{__('global.province')}}</dt>
                        <dd class="col-8">{{$advert->province->{'name_'.$locale} }}</dd>
                        @endif
                        @if(isset($advert->city)&&$advert->city)
                        <dt class="col-4">{{__('global.city')}}</dt>
                        <dd class="col-8">{{$advert->city->{'name_'.$locale} }}</dd>
                        @endif
                        <dt class="col-4">{{__('global.author')}}</dt>
                        <dd class="col-8">{{$advert->author->name }}</dd>
                        <dt class="col-4">{{__('global.contact_email')}}</dt>
                        <dd class="col-8"><a href="mailto:{{$advert->email}}" target="_blank">{{$advert->email}}</a></dd>
                        <dt class="col-4">{{__('global.contact_phone')}}</dt>
                        <dd class="col-8"><a href="tel:{{$advert->phone}}" target="_blank">{{$advert->phone}}</a></dd>
                        <dt class="col-4">{{__('global.date_publication')}}</dt>
                        <dd class="col-8">{{date('Y-m-d',strtotime($advert->created_at))}}</dd>
                    </dl>
                    
                </div>
            </div>
        </div>
    </div>

@endsection
