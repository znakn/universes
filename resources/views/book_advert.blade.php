@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\BooksCornerPosts;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row pt-3 pb-5">
                <div class="col-12 pb-4">
                    <h1 class="h1">{!! $advert->{'title_'.$locale}  !!}</h1>
                </div>
                <div class="col-12 col-sm-4">
                    <img src="{{'/upload/'.BooksCornerPosts::FORM_TYPE.'/main_'.$advert->image}}" alt="Img" class="img-fluid">
                </div>
                <div class="col-12 col-sm-8">
                    {!! $advert->{'text_'.$locale} !!}
                    <dl class="row">
                        <dt class="col-4">{{__('global.type')}}</dt>
                        <dd class="col-8">{{$advert->type->{'name_'.$locale} }}</dd>
                        <dt class="col-4">{{__('global.author')}}</dt>
                        <dd class="col-8">{{$advert->author}}</dd>
                        <dt class="col-4">{{__('global.contact_email')}}</dt>
                        <dd class="col-8"><a href="mailto:{{$advert->email}}" target="_blank">{{$advert->email}}</a></dd>
                        <dt class="col-4">{{__('global.contact_phone')}}</dt>
                        <dd class="col-8"><a href="tel:{{$advert->phone}}" target="_blank">{{$advert->phone}}</a></dd>
                        <dt class="col-4">{{__('global.date_publication')}}</dt>
                        <dd class="col-8">{{date('Y-m-d',strtotime($advert->created_at))}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

@endsection
