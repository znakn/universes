@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4 pb-2">
                    <p class="h1 text-bright">
                    <h1>{!! $page->{'title_'.$locale}  !!}</h1>
                    </p>
                </div>
            </div>

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4 pb-2">
                    <div class="main-image">
                        <img src="{{'/storage/'.$page->image}}" width="390px" height="130px" alt="Img" class="custom_page">
                    </div>
                </div>
            </div>

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4 pb-2">
                    <div class="main-text">
                        {!! $page->{'text_'.$locale} !!}
                    </div>
                    <p>{{__('global.date')}} : {{date('Y-m-d',strtotime($page->created_at))}}</p>
                </div>
            </div>



        </div>


    </div>

@endsection
