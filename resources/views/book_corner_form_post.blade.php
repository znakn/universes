@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url])

@section('content')
    <!-- Recencia library -->
    <div class="wrap-bg-beige-3">
        <div class="container">
            <form action="{{LaravelLocalization::localizeUrl('/'.$program_url.'/post-form/'.$form_type)}}" method="post" >
                <input type="hidden" name="form_type" id="form_type" value="{{$form_type}}">
                <input type="hidden" name="_token" value="{{@csrf_token()}}" />
                <div class="row justify-content-center py-4">
                    <div class="col-12 col-md-8">
                        <div class="form-group">
                            <h1>{{$form_title}}</h1>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group {{ $errors->has('section_id') ? ' has-error' : '' }}">
                            <label>{{__('global.section')}}</label>
                            <select name="section_id" class="form-control @if($errors->has('section_id')) is-invalid  @endif">
                                <option value=""></option>
                                @if($booksCornerSections->isNotEmpty())
                                    @foreach($booksCornerSections as $booksCornerSection)
                                        @if(old('section_id'))
                                            <option value="{{$booksCornerSection->id}}" @if($booksCornerSection->id == old('section_id')) selected="selected" @endif   >{{$booksCornerSection->{'title_'.$locale} }}</option>
                                        @else
                                            <option value="{{$booksCornerSection->id}}">{{$booksCornerSection->{'title_'.$locale} }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>

                        <div class="form-group {{ $errors->has('type_id') ? ' has-error' : '' }}">
                            <label>{{__('global.type')}}</label>
                            <select name="type_id" class="form-control @if($errors->has('type_id')) is-invalid  @endif">
                                <option value=""></option>
                                @if($booksCornerTypes->isNotEmpty())
                                    @foreach($booksCornerTypes as $booksCornerType)
                                        @if(old('type_id'))
                                            <option value="{{$booksCornerType->id}}" @if($booksCornerType->id == old('type_id')) selected="selected" @endif >{{$booksCornerType->{'name_'.$locale} }}</option>
                                        @else
                                            <option value="{{$booksCornerType->id}}">{{$booksCornerType->{'name_'.$locale} }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                            <label>{{__('global.title')}}</label>
                            <input type="text" aria-label="Title" name="title"  value="{{old('title')?old('title'):''}}"    placeholder="{{__('global.title')}}" class="form-control @if($errors->has('title')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                            <label>{{__('global.description')}}</label>
                            <textarea aria-label="text" name="text" id="text"  placeholder="{{__('global.text')}}" class="form-control @if($errors->has('text')) is-invalid  @endif">
                                {{old('text')?old('text'):''}}
                            </textarea>
                        </div>
                        <div class="form-group {{ $errors->has('curse') ? ' has-error' : '' }}">
                            <label>{{__('global.curse_matheria')}}</label>
                            <input type="text" aria-label="Curse/Matheria" value="{{old('curse')?old('curse'):''}}"  name="curse" placeholder="{{__('global.curse_matheria')}}" class="form-control @if($errors->has('curse')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('author') ? ' has-error' : '' }}">
                            <label>{{__('global.author')}}</label>
                            <input type="text" aria-label="Author" name="author" value="{{old('author')?old('author'):''}}" placeholder="{{__('global.author')}}" class="form-control @if($errors->has('author')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>{{__('global.contact_email')}}</label>
                            <input type="text" aria-label="Contact Email" name="email" value="{{old('email')?old('email'):$user_email}}" placeholder="{{__('global.contact_email')}}" class="form-control @if($errors->has('email')) is-invalid  @endif">
                        </div>
                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label>{{__('global.contact_phone')}}</label>
                            <input type="text" aria-label="Contact Phone" name="phone" value="{{old('phone')?old('phone'):''}}" placeholder="{{__('global.contact_phone')}}" class="form-control @if($errors->has('phone')) is-invalid  @endif">
                        </div>
                        <div class="form-group">
                            <label>{{__('global.preview_image')}}</label>
                            <div class="needsclick dropzone" id="image-dropzone">
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit"  value="{{__('global.save')}}" class="btn btn-main btn-lg btn-block my-1">
                        </div>
                    </div>
                </div>

            </form>
    </div>


    <!-- Advertising -->
    <div class="wrap-bg-beige-3">
        <div class="container">
            <div class="row py-2">
                <div class="col">
                    <div class="advertising my-4"></div>
                </div>
            </div>
        </div>
    </div>


    @endsection

    <script src="{{asset('/vendor/ckeditor/ckeditor.js')}}"></script>
    @section('image-script')
    <!------- Crop script ------------------->
        <script src="{{asset('/js/dropzone/dist/dropzone.js')}}"></script>
        <link rel="stylesheet" href="{{asset('/js/dropzone/dist/dropzone.css')}}">
        <script>
            Dropzone.autoDiscover = false;
            var dropGallery = new  Dropzone("#image-dropzone", {
                url: '{{ route('form-file-upload') }}',
                maxFiles: 1, // MB
                addRemoveLinks: true,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                success: function (file, response) {
                    $('form').append('<input type="hidden" name="upload_image" value="' + response.name + '">');
                },
                removedfile: function (file) {
                    file.previewElement.remove();
                    var name = '';
                    if (typeof file.file_name !== 'undefined') {
                        name = file.file_name;
                    }
                    $('form').find('input[name="upload_image"][value="' + name + '"]').remove();
                },
                init:function(){
                    this.on("sending", function(file, xhr, formData){
                        formData.append("image_type", "book_corner_image");
                    });
                }
            });


            CKEDITOR.replace('text',{
                toolbarGroups : [
                    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                    { name: 'forms', groups: [ 'forms' ] },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                    { name: 'links', groups: [ 'links' ] },
                    { name: 'insert', groups: [ 'insert' ] },
                    '/',
                    { name: 'styles', groups: [ 'styles' ] },
                 //   { name: 'colors', groups: [ 'colors' ] },
                    { name: 'tools', groups: [ 'tools' ] },
                    { name: 'others', groups: [ 'others' ] },
                 //   { name: 'about', groups: [ 'about' ] }
                ]
            });




        </script>
    <!--------Crop script ------------------->



@endsection
