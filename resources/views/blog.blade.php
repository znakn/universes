@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\Blog;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4">
                        <h1 class="h1">{!! $blog->{'title_'.$locale}  !!}</h1>
                </div>
                <div class="col-12 col-sm-4">
                    <img src="{{'/upload/'.Blog::FORM_TYPE.'/main_'.$blog->image}}"  alt="Img" class="img-fluid">
                </div>
                <div class="col-12 col-sm-8">
                    {!! $blog->{'text_'.$locale} !!}
                    <dl class="row">
                        <dt class="col-4">{{__('global.author')}}</dt>
                        <dd class="col-8">{{$blog->author->name}}</dd>
                        <dt class="col-4">{{__('global.date_publication')}}</dt>
                        <dd class="col-8">{{date('Y-m-d',strtotime($blog->created_at))}}</dd>
                    </dl>
                </div>
            </div>
        </div>


    </div>

@endsection
