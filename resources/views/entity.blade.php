@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url, 'menu' => $menu])

@section('content')
    <!-- Search form and regions menu -->
    <div class="wrap-bg-beige-2">
        <div class="container">
            <div class="row regions pb-3">
                <!-- Regione links -->
                <div class="region-title">{{__('global.regions')}}</div>
                <div class="w-100 d-block d-sm-none"></div>
                @foreach($regions as $region)
                    <a href="{{LaravelLocalization::localizeUrl('/'.$program->url.'/'.$region['url'].'/list')}}" class="region-link @if($region['url']==$region_url) active  @endif">
                        <span>{{$region['name']}}</span>
                        <!-- <img src="../img/reg-1.svg" alt="" class="img-fluid region-img region-img-1"> -->
                        <svg version="1.1" class="img-fluid region-img item-{{$region['svg_order']}}" xmlns="http://www.w3.org/2000/svg" viewBox="{{$region['svg_view_box']}}" style="{{$region['svg_style']}}">
                            <path d="{{$region['svg']}}">
                            </path>
                        </svg>
                    </a>
                @endforeach
            </div>
            <div class="row justify-content-center py-3">
                <div class="col col-lg-8">
                    <!-- Search form -->
                    @include('parts/search_entity_form')
                </div>
            </div>
        </div>
    </div>

    <!-- Edu information tabs -->
    <div class="wrap-bg-beige-6">
        <!-- Here in comment styles if this block is link -->
        <!-- <div class="container">
            <div class="row pt-5">
                <div class="col-12">
                    <h1 class="h2">Marche</h1>
                    <p class="h3">
                        Ancona/Istituti Tecnico/ <span class="title h2 text-bright">ITS Nautico Aeronautico</span>
                    </p>
                </div>
                <nav class="submenu-block" aria-label="breadcrumb">
                    <ol class="breadcrumb py-0">
                        <li class="breadcrumb-item active"><a aria-current="page" href="#">informazioni</a></li>
                        <li class="breadcrumb-item"><a href="#">materie</a></li>
                        <li class="breadcrumb-item"><a href="#">maturita</a></li>
                        <li class="breadcrumb-item"><a href="#">collegamenti universita</a></li>
                        <li class="breadcrumb-item"><a href="#">formazione lavoro</a></li>
                        <li class="breadcrumb-item"><a href="#">alternanza scuola lavoro</a></li>
                        <li class="breadcrumb-item"><a href="#">tutor supporto</a></li>
                    </ol>
                </nav>
            </div>
            <div class="row py-4">
                <div class="col-12 col-md-4">
                    <img src="https://via.placeholder.com/400/" alt="Img" class="img-fluid">
                </div>
                <div class="col-12 col-md-8">
                    <div class="h3 text-bright text-uppercase">informazioni</div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit numquam delectus exercitationem, qui reiciendis adipisci. Perferendis, qui numquam non eveniet fugit consequuntur nihil odio facilis natus est modi, earum, aspernatur! Lorem ipsum dolor sit amet consectetur adipisicing elit. In nobis nihil placeat omnis nam, laborum vero doloribus, minima dicta, corporis amet incidunt esse aut tempora perferendis inventore? Natus, laudantium, libero.</p>
                    <p>Lorem ipsum, dolor, sit amet consectetur adipisicing elit. Quod sed dolores voluptatem, necessitatibus voluptatibus perferendis modi eos non aut, saepe! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Blanditiis tenetur ipsa quibusdam voluptatum quos harum provident deserunt quidem voluptate amet velit unde ea eveniet libero beatae natus, quaerat sint necessitatibus! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nisi in maxime numquam aliquid atque veniam quod quidem aut officiis laborum quasi quas ab dolorum accusamus, quo iusto deleniti tempora natus.</p>
                    <div class="address-text text-in-theme-color">
                        <address>
                            Lungomare Vanvitelli, Luigi, 76 60121 Ancona AN
                        </address>
                        <a href="tel:+24071992345" target="_blank">071 992345</a> - <a href="tel:+530713355693" target="_blank">071 3355693</a> - <a href="#" target="_blank"><b>www.istitutovolterraelia.it</b></a>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- Tabs -->
        <div class="container">
            <div class="row py-4">
                <div class="col-12">
                    <h1 class="h2">{{ $entity->{'name_'.app()->getLocale()}  }}</h1>
                    <!-- <p class="h3">
                        Ancona/Istituti Tecnico/ <span class="title h2 text-bright">ITS Nautico Aeronautico</span>
                    </p> -->
                </div>
                <div class="col-12 submenu-block">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="main-tab" data-toggle="pill" href="#main" role="tab" aria-controls="main" aria-selected="true">{{__('global.information')}}</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="education-program-tab" data-toggle="pill" href="#education-program" role="tab" aria-controls="education-program" aria-selected="false">{{__('global.edu_program')}}</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="disciplines-tab" data-toggle="pill" href="#disciplines" role="tab" aria-controls="disciplines" aria-selected="false">{{__('global.disciplines')}}</a>
                        </li>
                        @if($galleries->isNotEmpty())
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="galleries-tab" data-toggle="pill" href="#galleries" role="tab" aria-controls="galleries" aria-selected="false">{{__('global.galleries')}}</a>
                        </li>
                        @endif
                        @if($reviews->isNotEmpty())
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="reviews-tab" data-toggle="pill" href="#reviews" role="tab" aria-controls="reviews" aria-selected="false">{{__('global.reviews')}}</a>
                        </li>
                        @endif
                    </ul>
                    <div class="tab-content pt-3" id="pills-tabContent">

                        <!--  This is information tab     -->
                        <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                            <div class="row">
                                <div class="col-12 col-md-4 pb-2">
                                    @if($entity->image)
                                        <img src="{{'/storage/'.$entity->thumbnail('main','logo')}}"  alt="Img" class="card-img-top">
                                    @else
                                        <img src="https://via.placeholder.com/400/" alt="Img" class="img-fluid">
                                    @endif


                                </div>
                                <div class="col-12 col-md-8">
                                    <div class="h3 text-bright text-uppercase">{{__('global.information')}}</div>
                                    <!-- <h1>{{ $entity->{'name_'.app()->getLocale()}  }}</h1> -->
                                    <p>{!!  $entity->{'description_'.app()->getLocale()}   !!}</p>
                                    <div class="address-text text-in-theme-color">
                                        <address>
                                            <b>{{$entity->city->{'name_'.app()->getLocale()} }}</b> {{$entity->address}}
                                        </address>
                                        <a href="tel:{{$entity->phone}}" target="_blank">{{$entity->phone}}</a> -  <a href="@if(mb_strpos($entity->web_address,'http') !== false) {{$entity->web_address}} @else https://{{$entity->web_address}} @endif" target="_blank"><b>{{$entity->web_address}}</b></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="education-program" role="tabpanel" aria-labelledby="education-program-tab" >
                            <p class="h4 text-bright text-uppercase">{{__('global.edu_program')}}</p>
                            <div class="subjects-block text-uppercase">
                                <h3>{{$entity->{'program_title_'.$locale} }}</h3>
                                <div class="progarm-text">
                                    {!! $entity->{'program_text_'.$locale} !!}
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="disciplines" role="tabpanel" aria-labelledby="disciplines-tab">
                            <p class="h4 text-bright text-uppercase">{{__('global.disciplines')}}</p>
                            <div class="subjects-block text-uppercase">
                                @if($disciplines->isNotEmpty())
                                    @foreach($disciplines as $discipline)
                                        <p><a href="{{LaravelLocalization::localizeUrl('/discipline/'.$discipline->url)}}">{{$discipline->{'name_'.$locale} }}</a></p>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                        @if($galleries->isNotEmpty())
                        <div class="tab-pane fade" id="galleries" role="tabpanel" aria-labelledby="galleries-tab">
                            <p class="h4 text-bright text-uppercase">{{__('global.galleries')}}</p>
                            <div class="galleries">
                                <div class="row">
                                @foreach($galleries as $key => $image)
                                    <div class="galleries-image">
                                        <img src="{{'/storage/'.$image->image}}" class="img-fluid" alt="galleries_image_{{$image->id}}" data-toggle="modal" data-target="#exampleModal" onclick="$('#carouselEntity').carousel({{$key}});">
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            <div class="modal" id="exampleModal" tabindex="-1">
                              <div class="modal-dialog modal-dialog-centered modal-xl">
                                <div class="modal-content">
                                  <div class="modal-body p-0">

                                    <div id="carouselEntity" class="carousel slide carousel-in-modal" data-ride="carousel" data-touch="true" data-interval="false">
                                        <div class="carousel-inner">
                                            @foreach($galleries as $key => $image)
                                            <div class="carousel-item @if($key == 0) active @endif">
                                                <img src="{{'/storage/'.$image->image}}" class="d-block w-100" alt="galleries_image_{{$image->id}}">
                                            </div>
                                            @endforeach
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselEntity" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">{{__('global.previous')}}</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselEntity" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">{{__('global.next')}}</span>
                                        </a>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        @endif
                        @if($reviews->isNotEmpty())
                        <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                            <p class="h4 text-bright text-uppercase">{{__('global.reviews')}}</p>
                            <div class="reviews">

                                <div class="row py-4">
                                    <div class="col">
                                        @if($reviews->isNotEmpty())
                                            @foreach($reviews as $review)
                                                <div class="lib-block media mb-5 row">
                                                    <!-- <a href="{{LaravelLocalization::localizeUrl('/review/'.$program_url.'/'.$review->url)}}"> -->
                                                    <div class="col-3">
                                                        <img src="{{'/storage/'.$review->image}}" alt="Img" class="mr-3 img-fluid">
                                                    </div>
                                                    <div class="media-body col-9">
                                                        <h5 class="h5 mt-0">{{$review->{'title_'.$locale} }}</h5>
                                                        {!!  mb_substr($review->{'text_'.$locale },0,300).' ...'.__('global.read_all') !!}
                                                        <p>
                                                            <a href="{{LaravelLocalization::localizeUrl('/review/'.$program_url.'/'.$review->url)}}" class="font-weight-bold">{{__('global.read_all')}}</a>
                                                        </p>
                                                    </div>
                                                    <!-- </a> -->
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="lib-block media mb-5">
                                                <h3>{{__('global.no_publication')}}</h3>
                                            </div>
                                        @endif

                                    </div>
                                </div>

                                <div class="row pb-4">
                                    <div class="col" >
                                        <nav aria-label="Page navigation">
                                            {{$reviews->links()}}
                                        </nav>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Review block -->
    <div class="wrap-bg-white-1">
        <div class="container">
            @if(Auth::check()&&(!$existRatingByUser))
            <!-- Votes or grades -->
            <form method="post" action="{{LaravelLocalization::localizeUrl('/rating')}}">
                <input type="hidden" name="_token" value="{{@csrf_token()}}" />
                <input type="hidden" name="entity_id" value="{{$entity->id}}" />
                <div class="row py-4 votes-block">
                    <div class="col-12">
                        <p class="h2 text-bright">{{__('global.frequent')}}</p>
                    </div>

                    <div class="col-12 vote-title mb-2">
                        {{$rating}} ({{$ratingCount}} {{__('global.reviews')}})
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="form-group row vote-form">
                            <label for="control1" class="col-4  col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.structure')}}</label>
                            <div class="col-6 pt-2">
                                <input type="range" value="0" min="0" max="5" step=".1" name="structura" id="control1" class="form-control-range vote-range" data-target="voteValue1">
                            </div>
                            <span id="voteValue1" class="vote-range-value col-2" for="structura">0.0</span>
                        </div>
                        <div class="form-group row vote-form">
                            <label for="control2" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.courses')}}</label>
                            <div class="col-6 pt-2">
                                <input type="range" value="0" min="0" max="5" step=".1" name="course" id="control2" class="form-control-range vote-range" data-target="voteValue2">
                            </div>
                            <span id="voteValue2" class="vote-range-value col-2" for="course">0.0</span>
                        </div>
                        <div class="form-group row vote-form">
                            <label for="control3" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.teachers')}}</label>
                            <div class="col-6 pt-2">
                                <input type="range" value="0" min="0" max="5" step=".1" name="teachers" id="control3" class="form-control-range vote-range" data-target="voteValue3">
                            </div>
                            <span id="voteValue3" class="vote-range-value col-2" for="teachers">0.0</span>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="form-group row vote-form">
                            <label for="control4" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.social_activities')}}</label>
                            <div class="col-6 pt-2">
                                <input type="range" value="0" min="0" max="5" step=".1" name="social_activities" id="control4" class="form-control-range vote-range" data-target="voteValue4">
                            </div>
                            <span id="voteValue4" class="vote-range-value col-2" for="social_activities">0.0</span>
                        </div>
                        <div class="form-group row vote-form">
                            <label for="control5" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.city')}}</label>
                            <div class="col-6 pt-2">
                                <input type="range" value="0" min="0" max="5" step=".1" name="accommodation" id="control5" class="form-control-range vote-range" data-target="voteValue5">
                            </div>
                            <span id="voteValue5" class="vote-range-value col-2" for="accommodation">0.0</span>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 vote-btn">
                        <input type="submit" class="btn btn-main btn-lg btn-block" value="{{__('global.leave_your_tips')}}">
                    </div>
                </div>
            </form>
            @else
                    <form>
                        <div class="row py-4 votes-block">
                            <div class="col-12">
                                <p class="h2 text-bright">{{__('global.frequent')}}</p>
                            </div>

                            <div class="col-12 vote-title mb-2">
                                {{$rating}} ({{$ratingCount}} {{__('recensioni')}})
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4">
                                <div class="form-group row vote-form">
                                    <label for="control1" class="col-4  col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.structure')}}</label>
                                    <div class="col-6 pt-2">
                                        <input type="range" value="{{$ratingArray['structura']}}" disabled  min="0" max="5" step=".1" name="structura" id="control1" class="form-control-range vote-range" data-target="voteValue1">
                                    </div>
                                    <span id="voteValue1" class="vote-range-value col-2" for="structura">{{$ratingArray['structura']}}</span>
                                </div>
                                <div class="form-group row vote-form">
                                    <label for="control2" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.courses')}}</label>
                                    <div class="col-6 pt-2">
                                        <input type="range" value="{{$ratingArray['courses']}}" disabled min="0" max="5" step=".1" name="course" id="control2" class="form-control-range vote-range" data-target="voteValue2">
                                    </div>
                                    <span id="voteValue2" class="vote-range-value col-2" for="course">{{$ratingArray['courses']}}</span>
                                </div>
                                <div class="form-group row vote-form">
                                    <label for="control3" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.teachers')}}</label>
                                    <div class="col-6 pt-2">
                                        <input type="range" value="{{$ratingArray['teachers']}}" disabled min="0" max="5" step=".1" name="teachers" id="control3" class="form-control-range vote-range" data-target="voteValue3">
                                    </div>
                                    <span id="voteValue3" class="vote-range-value col-2" for="teachers">{{$ratingArray['teachers']}}</span>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4">
                                <div class="form-group row vote-form">
                                    <label for="control4" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.social_activities')}}</label>
                                    <div class="col-6 pt-2">
                                        <input type="range" value="{{$ratingArray['social_activities']}}" disabled min="0" max="5" step=".1" name="social_activities" id="control4" class="form-control-range vote-range" data-target="voteValue4">
                                    </div>
                                    <span id="voteValue4" class="vote-range-value col-2" for="social_activities">{{$ratingArray['social_activities']}}</span>
                                </div>
                                <div class="form-group row vote-form">
                                    <label for="control5" class="col-4 col-form-label pt-0 pr-0" data-toggle="tooltip" data-placement="right" title="Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eveniet dolores fugiat accusantium?">{{__('global.city')}}</label>
                                    <div class="col-6 pt-2">
                                        <input type="range" value="{{$ratingArray['accommodation']}}" disabled min="0" max="5" step=".1" name="accommodation" id="control5" class="form-control-range vote-range" data-target="voteValue5">
                                    </div>
                                    <span id="voteValue5" class="vote-range-value col-2" for="accommodation">{{$ratingArray['accommodation']}}</span>
                                </div>

                            </div>
                            @if(!Auth::check())
                            <div class="col-12 col-lg-4 vote-btn">
                                <a href="{{LaravelLocalization::localizeUrl('/register')}}" class="btn btn-main btn-lg btn-block" >{{__('global.leave_your_tips')}}</a>
                            </div>
                            @endif
                        </div>
                    </form>

            @endif


        </div>
    </div>



@endsection
