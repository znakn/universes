@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\Blog;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row py-2">
                <div class="col-12">
                    <p class="h1 text-bright">
                    <h1>{!! $discipline->{'name_'.$locale}  !!}</h1>
                    </p>
                </div>
            </div>

            <div class="row py-2">
                <div class="col-12">
                    <div class="gallery-block">
                        @if(is_array($discipline->image))
                            <div id="disciplineCarousel" class="carousel slide mb-4" data-ride="carousel" data-touch="true" data-interval="false">
                                <div class="carousel-inner">
                                    @foreach($discipline->image as $key => $image)
                                    <div class="carousel-item @if($key == 0) active @endif">
                                        <img src="{{'/storage/'.$image}}" class="d-block w-100" alt="galleries_image_{{$key}}">
                                    </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#disciplineCarousel" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">{{__('global.previous')}}</span>
                                </a>
                                <a class="carousel-control-next" href="#disciplineCarousel" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">{{__('global.next')}}</span>
                                </a>
                            </div>

                            <!--img src="" width="30%" height="30%" alt="Img" class="main_image_blog"-->
                            <div class="carousel-indicators-block">
                            @foreach($discipline->image as $key => $image)
                                <div class="galleries-image in-carousel">
                                    <img src="{{'/storage/'.$image}}" class="img-fluid" alt="galleries_image_{{$key}}" onclick="$('#disciplineCarousel').carousel({{$key}});">
                                </div>
                            @endforeach
                            </div>
                        @else
                            <img src="{{'/storage/'.$discipline->image}}" class="galleries-image" alt="galleries_image_{{$discipline->id}}">
                        @endif
                    </div>
                </div>
            </div>

            <div class="row py-2">
                <div class="col-12">
                    <div class="main-text">
                        {!! $discipline->{'description_'.$locale} !!}
                    </div>
                    <p>{{__('global.terms')}} : @foreach($discipline->terms as $term)  {{$term->{'name_'.$locale} }} @endforeach </p>
                </div>
            </div>

        </div>
    </div>

@endsection
