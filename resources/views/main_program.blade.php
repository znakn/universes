@php
    use App\Programs;
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
@endphp
@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_url'=>$program_url,'menu' => $menu])

@section('content')
    <div class="wrap-search">
        <div class="container">
            <div class="row pt-3">
                <div class="col col-sm-8 offset-sm-4 col-md-7 offset-md-5 col-lg-4 offset-lg-7">
                    <div class="wrap-map">
                        <!-- To make some js events see ./javascript/custom.js . There is an event for .map-item. Use data-info to add some information to each item. -->
                        <svg xmlns="http://www.w3.org/2000/svg" width="156.966mm" height="185.185mm" viewBox="0 0 445 525" class="map-italy">
                            @foreach($regions as $region)
                                <path class="map-item item-{{$region['id']}}" data-info="{{$region['name']}}" data-url="{{LaravelLocalization::localizeUrl('/'.$program->url.'/'.$region['url'].'/list') }}"
                                      d="{{$region['svg']}}"/>
                            @endforeach
                        </svg>
                    </div>
                </div>
                <div class="search-form">
                </div>
            </div>
            <div class="row justify-content-center mt-n2 pb-3">
                <div class="col-12 col-lg-8">
                @include('parts/search_entity_form')
                </div>
            </div>
        </div>
    </div>

    <!-- Tips -->
    <div class="wrap-bg-beige-1">
        <div class="container pt-5 pb-5">
            <div class="row">
                <div class="col text-center pb-3">
                    <p class="h1">{{$title}}</p>
                    <p class="h2">{!! $description !!}</p>
                </div>
            </div>
            <div class="row">
                <div class="w-100 d-none d-lg-block"></div>
                <div class="col-12 col-md-6 col-lg mb-5">
                    <div class="tips-block tips-color-1">
                        @if($image)
                        <img src="{{'/storage/'.$image}}" width="1000px" height="400px" alt="Img" class="img-fluid mt3 mx-auto d-block">
                        @else
                        <img src="https://picsum.photos/1000/400" alt="Img" class="img-fluid mt3 mx-auto d-block">
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Text block -->
    <div class="wrap-bg-beige-2">
        <div class="container">
            <div class="text-block py-5">
                <h1 class="h2 title">{{$title}}</h1>
                <div class="text">{!! $text !!}</div>
        </div>
    </div>

    <!-- Advertising -->
    <div class="wrap-bg-beige-1">
        <div class="container">
            <div class="row py-2">
                <div class="col">
                    <div class="advertising my-4"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
