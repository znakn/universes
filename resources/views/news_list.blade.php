@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use App\AdminNews;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

<div class="wrap-img text-center">
    <img src="{{asset('/img/tips_news_img_center.webp')}}" alt="Img" class="img-fluid">
</div>

<div class="row wrap-bg-beige-5">
    <div class="col text-center order-10">
        @if(Auth::check())
            <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/post-form/news')}}" class="btn btn-main btn-lg btn-block my-1 font-weight-bold">{{__('Add')}}</a>
        @endif
    </div>

</div>

<div class="wrap-bg-beige-5">
    <div class="container">
        <div class="row pt-3 pb-5">
            <div class="col-12 pt-4 pb-2">
                <p class="h1 text-bright">{{__('global.news_and_trend')}}</p>
            </div>
            @if(($news->isNotEmpty()))
                @foreach($news as $new)

                    <div class="col-6 col-md-3">
                        <div class="card news-tend-block position-relative">
                            <!-- <a href="{{LaravelLocalization::localizeUrl('/news/'.$new->url)}}"> -->
                            <div class="news-img">
                                <img src="{{'/upload/news/thumb_'.$new->image}}" alt="Img" class="card-img-top">
                            </div>
                            <div class="card-body px-0">
                                <a href="{{LaravelLocalization::localizeUrl('/news/'.$new->url)}}" class="h3 card-title">{{$new->{'title_'.$locale } }}</a>
                                <p class="card-text">{!!  mb_substr($new->{'text_'.$locale },0,300).' ...' !!}</p>
                                <p class="">
                                    <b>{{$new->author->name }}</b><br>
                                    {{date('Y-m-d',strtotime($new->created_at))}}
                                </p>
                                <!--a href="#" class="fb-like-btn">{{$new->voite}} mi piace</a-->
                            </div>
                            <!-- </a> -->
                        </div>
                    </div>


                @endforeach
            @else
                <div class="col-12 pt-4 pb-2">
                    <p class="h3 text-bright">{{__('Not news')}}</p>
                </div>
            @endif


        </div>
        <div class="row pb-4">
            <div class="col" >
                <nav aria-label="Page navigation">
                    {{$news->links()}}
                </nav>
            </div>
        </div>




    </div>
</div>

@if(isset($newsBlock)&&($newsBlock->status == \App\Block::ENABLED))
<div class="wrap-bg-bright">
    <div class="container py-4 text-white">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <p class="h2">{{$newsBlock->{'title_'.$locale} }}</p>
                {!! $newsBlock->{'text_'.$locale} !!}
            </div>
        </div>
    </div>
</div>
@endif
<div class="wrap-bg-beige-1">
    <div class="container pt-4">
        <p class="h2 text-bright pb-3">{{__('global.tech_and_social')}}</p>
        <div class="row">
            @if($adminNews->isNotEmpty())
                @foreach($adminNews as $anews)
                    <div class="col-12 col-sm-6">
                        <div class="card news-block">
                            <div class="news-img">
                                @if($anews->image)
                                <img src="{{'/storage/'.$anews->image}}" alt="Img" class="card-img-top">
                                @endif
                                <div class="news-who">
                                    <b>{{$anews->author->name}}</b><br>
                                    {{date('Y-m-d')}}
                                </div>
                            </div>
                            <div class="card-body px-0">
                                <h5 class="h4 card-title">{{$anews->{'title_'.$locale} }}</h5>
                                <p class="card-text">{!!  mb_substr($anews->{'text_'.$locale },0,300).' ...' !!}</p>
                                <a href="{{LaravelLocalization::localizeUrl('/portal-news/'.$anews->url)}}" class="font-weight-bold">leggi tutto</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif


        </div>

        <div class="row pb-4">
            <div class="col" >
                <nav aria-label="Page navigation">
                    {{$adminNews->links()}}
                </nav>
            </div>
        </div>



    </div>
</div>

<div class="wrap-bg-beige-2">
    <div class="container pt-4">
        <p class="h2 text-bright pb-3">{{__('global.videos')}}</p>
        @if($videoNews->isNotEmpty())
            @foreach($videoNews as $vNews)
                <div class="row pb-3">
                    <div class="col-12 col-sm-6">
                        <iframe class="video-frame" src="https://www.youtube.com/embed/{{AdminNews::getYoutubeIdFromUrl($vNews->video)}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="card video-text-block">
                            <div class="card-body p-0">
                                <h5 class="h4 card-title">{{$vNews->{'title_'.$locale} }}</h5>
                                <p class="card-text">{!!  mb_substr($vNews->{'text_'.$locale },0,300).' ...' !!}</p>
                                <a href="{{LaravelLocalization::localizeUrl('/portal-news/'.$vNews->url)}}" class="font-weight-bold d-block mt-3">leggi tutto</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-12 pt-4 pb-2">
                <p class="h3 text-bright">{{__('Not videos')}}</p>
            </div>
        @endif

        <div class="row pb-4">
            <div class="col" >
                <nav aria-label="Page navigation">
                    {{$videoNews->links()}}
                </nav>
            </div>
        </div>

    </div>
</div>

@endsection

