<form method="get" action="/search/form" id="from_search" class="form-filter form-search-block filter-form-long" >
    <div class="input-group">
        {{ csrf_field() }}
        <input type="hidden" id="locale" name="locale" value="{{app()->getLocale()}}">
        <input type="hidden" id="has_province" value="{{isset($provinces) ? 'true':'false'}}" >
        <input type="hidden" id="has_city" value="{{isset($cities) ? 'true':'false'}}" >
        <input type="hidden" id="placeholder_region" value="{{__('search.placeholder_region')}}">
        <input type="hidden" id="placeholder_province" value="{{__('search.placeholder_province')}}">
        <input type="hidden" id="placeholder_city" value="{{__('search.placeholder_city')}}">
        <input type="hidden" name="program" value="{{$program->url}}">
        <select class="form-control" name="region"  id="regions" aria-label="Region"   >
            <option value=""></option>
            @foreach($regions as $region)
                @if(isset($region_url))
                    @if ($region_url == $region['url'])
                    <option value="{{$region['url']}}" selected="selected" data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                    @else
                    <option value="{{$region['url']}}" data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                    @endif
                @else
                    <option value="{{$region['url']}}" data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                @endif

            @endforeach
        </select>

        <select id="province" aria-label="Province" name="province" class="form-control">
            <option value=""></option>
            @if(isset($provinces))
                @foreach($provinces as $province)
                    @if(isset($province_url)&&($province_url == $province['url']))
                    <option value="{{$province['url']}}" data-province_id="{{$province['id']}}" selected="selected"  >{{$province['name']}}</option>
                    @else
                    <option value="{{$province['url']}}" data-province_id="{{$province['id']}}"  >{{$province['name']}}</option>
                    @endif
                @endforeach
            @endif
        </select>

        <select id="city" aria-label="City" name="city" class="form-control">
            <option value=""></option>
            @if(isset($cities))
                @foreach($cities as $city)
                    @if ((isset($city_url))&&($city_url == $city['url']))
                    <option value="{{$city['url']}}" data-city_id="{{$city['id']}}" selected="selected" >{{$city['name']}}</option>
                    @else
                        <option value="{{$city['url']}}" data-city_id="{{$city['id']}}"  >{{$city['name']}}</option>
                    @endif
                @endforeach
            @endif
        </select>
        <input type="text" aria-label="University" name="search_string" placeholder="@if($program->url == \App\Programs::MASTER_UNIVERSITY)   {{__('global.university')}} @else {{__('global.school')}} @endif" class="form-control filter-form">

        <input type="hidden" id="region_id" name="region_id" >
        <input type="hidden" id="province_id" name="province_id" >
        <input type="hidden" id="city_id" name="city_id" >
        <div class="input-group-append">
            <button class="btn btn-primary btn-search" type="button" id="button-addon1">
                <span class="search-icon"></span>
            </button>
        </div>
    </div>
</form>
