<div class="list-group" id="list-tab" role="tablist">
    <a class="list-group-item list-group-item-action @if($active == 'profile') active @endif"  href="/profile"  >{{__('menu.profile')}}

    </a>
    <a class="list-group-item list-group-item-action @if($active == 'book_posts') active @endif"  href="/profile/book_posts" >{{__('menu.books_corner')}}
        <span class="badge bg-primary rounded-pill">{{$count_books_posts}}</span>
    </a>
    <a class="list-group-item list-group-item-action @if($active == 'accommodation_posts') active  @endif"  href="/profile/accommodation_posts" >{{__('menu.logistica')}}
        <span class="badge bg-primary rounded-pill">{{$count_accommodations_posts}}</span>
    </a>
    <a class="list-group-item list-group-item-action @if($active == 'blogs') active  @endif"  href="/profile/blogs" >{{__('menu.social_blog')}}
        <span class="badge bg-primary rounded-pill">{{$count_blog_posts}}</span>
    </a>
    <a class="list-group-item list-group-item-action @if($active == 'news') active  @endif"  href="/profile/news" >{{__('menu.tips_news')}}
        <span class="badge bg-primary rounded-pill">{{$count_news_posts}}</span>
    </a>
</div>
