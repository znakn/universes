<form method="get" action="/search/logistica/form" id="from_search"  class="form-filter form-search-block filter-form-long-5" >
    <div class="input-group">
        {{ csrf_field() }}
        <input type="hidden" id="locale" value="{{app()->getLocale()}}">
        <input type="hidden" id="has_provinces" value="{{isset($provinces) ? 'true':'false'}}" >
        <input type="hidden" id="has_city" value="{{isset($cities) ? 'true':'false'}}" >
        <input type="hidden" id="placeholder_logistica_type" value="{{__('search.placeholder_logistica_type')}}">
        <input type="hidden" id="placeholder_region" value="{{__('search.placeholder_region')}}">
        <input type="hidden" id="placeholder_province" value="{{__('search.placeholder_province')}}">
        <input type="hidden" id="placeholder_city" value="{{__('search.placeholder_city')}}">
        <input type="hidden" name="program" value="{{$program->url}}">

        <select class="form-control" name="logistica_type_id"  id="logistica_type_id" aria-label="{{__('Type')}}"   >
            <option value=""></option>
            @if($logisticaTypes->isNotEmpty())
                @foreach($logisticaTypes as $logisticaType)
                    @if(isset($logisticaTypeId)&&($logisticaTypeId == $logisticaType->id ))
                        <option value="{{$logisticaType->id}}" selected="selected" >{{$logisticaType->{'name_'.$locale} }}</option>
                    @else
                        <option value="{{$logisticaType->id}}">{{$logisticaType->{'name_'.$locale} }}</option>
                    @endif
                @endforeach
            @endif
        </select>

        <select class="form-control" name="region_id"  id="logistica_regions" aria-label="Region"   >
            <option value=""></option>
            @foreach($regions as $region)
                @if(isset($region_url))
                    @if ($region_url == $region['url'])
                    <option value="{{$region['id']}}" selected="selected" data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                    @else
                    <option value="{{$region['id']}}" data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                    @endif
                @else
                    <option value="{{$region['id']}}" data-region_id="{{$region['id']}}"  >{{$region['name']}}</option>
                @endif

            @endforeach
        </select>

        <select id="logistica_provinces" aria-label="Province" name="province_id" class="form-control">
            <option value=""></option>
            @if(isset($provinces))
                @foreach($provinces as $province)
                    @if(isset($province_url)&&($province_url == $province['url']))
                        <option value="{{$province['id']}}" data-province_id="{{$province['id']}}" selected="selected"  >{{$province['name']}}</option>
                    @else
                        <option value="{{$province['id']}}" data-province_id="{{$province['id']}}"  >{{$province['name']}}</option>
                    @endif
                @endforeach
            @endif
        </select>


        <select id="logistica_city" aria-label="City" name="city_id" class="form-control">
            <option value=""></option>
            @if(isset($cities))
                @foreach($cities as $city)
                    @if(isset($city_url)&&($city_url == $city['url']))
                        <option value="{{$city['id']}}" data-city_id="{{$city['id']}}" selected="selected"  >{{$city['name']}}</option>
                    @else
                        <option value="{{$city['id']}}" data-city_id="{{$city['id']}}"  >{{$city['name']}}</option>
                    @endif

                @endforeach
            @endif
        </select>
        <input type="text" aria-label="Search" name="title" placeholder="{{__('global.title')}}" class="form-control">

        <div class="input-group-append">
            <button class="btn btn-primary btn-search" type="button" id="button-addon1">
                <span class="search-icon"></span>
            </button>
        </div>
    </div>
</form>
