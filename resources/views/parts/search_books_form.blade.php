<form method="get" action="/search/books/form" id="from_search_books" class="form-filter form-search-block filter-form-long" >
    <div class="input-group">
        {{ csrf_field() }}
        <input type="hidden" id="locale" value="{{app()->getLocale()}}">
        <input type="hidden" id="placeholder_type" value="{{__('search.placeholder_type')}}">
        <input type="hidden" id="placeholder_section" value="{{__('search.placeholder_section')}}">
        <input type="hidden" name="program" value="{{$program->url}}">

        <select class="form-control" name="section"  id="corner_books_section_id" aria-label="{{__('global.section')}}"   >
            <option value=""></option>
            @if($sections->isNotEmpty())
                @foreach($sections as $sec)
                    @if($section&&($section == $sec->url))
                        <option value="{{$sec->url}}" selected="selected" >{{$sec->{'title_'.$locale} }}</option>
                    @else
                        <option value="{{$sec->url}}">{{$sec->{'title_'.$locale} }}</option>
                    @endif
                @endforeach
            @endif
        </select>

        <select class="form-control" name="corner_books_type_id"  id="corner_books_type_id" aria-label="{{__('global.type')}}"   >
            <option value=""></option>
            @if($booksCornerTypes->isNotEmpty())
                @foreach($booksCornerTypes as $booksCornerType)
                    @if($type_id&&($type_id == $booksCornerType->id))
                        <option value="{{$booksCornerType->id}}" selected="selected"  >{{$booksCornerType->{'name_'.$locale} }}</option>
                    @else
                        <option value="{{$booksCornerType->id}}">{{$booksCornerType->{'name_'.$locale} }}</option>
                    @endif


                @endforeach
            @endif
        </select>

        <input type="text" aria-label="Corso/Materia" name="curse"  placeholder="{{__('global.curse_matheria')}}" class="form-control">
        <input type="text" aria-label="Autore/Titolo" name="author_title"  placeholder="{{__('global.author')}}" class="form-control">

        <div class="input-group-append">
            <button class="btn btn-primary btn-search" type="button" id="button-books-search">
                <span class="search-icon"></span>
            </button>
        </div>
    </div>
</form>
