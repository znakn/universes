@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\BooksCornerPosts;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url, 'menu' => $menu])

@section('content')
    <!-- Recencia library -->
    <div class="wrap-bg-beige-3">
        <div class="container">

                <div class="row justify-content-center py-4">
                    <div class="col-12 col-md-8">
                        @include('parts/search_books_form')
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right order-12">
                        @if(Auth::check())
                        <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/post-form/'.$form_type)}}" class="btn btn-main btn-lg p-3 font-weight-bold">{{'Add'}}</a>
                        @endif
                    </div>
                    <div class="col order-1">
                        <h1 class="h1 mb-0">{{__('menu.books_corner')}}</h1>
                    </div>
                </div>
            <div class="row py-4">
                <div class="col">
                    @if($publications->isNotEmpty())
                        @foreach($publications as $publication)
                            <div class="lib-block media mb-5">
                                <img src="{{'/upload/'.BooksCornerPosts::FORM_TYPE.'/thumb_'.$publication->image}}" alt="Img" class="mr-3">
                                <div class="media-body">
                                    <h5 class="h5 mt-0">{{$publication->{'title_'.$locale} }}</h5>
                                    <p>{!! $publication->{'text_'.$locale} !!}</p>
                                    <a href="{{LaravelLocalization::localizeUrl('/book-corner-post/'.$publication->url)}}" class="font-weight-bold">{{__('global.read_all')}}</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="lib-block media mb-5">
                            <h3>{{__('global.no_publication')}}</h3>
                        </div>
                    @endif

                </div>
            </div>
            <div class="row pb-4">
                <div class="col" >
                    <nav aria-label="Page navigation">
                        {{$publications->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <!-- Text block -->
    @if(isset($block)&&($block->status == \App\Block::ENABLED))
    <div class="wrap-bg-beige-2">
        <div class="container">
            <div class="text-block py-5">
                <h1 class="h2 title">{{$block->{'title_'.$locale} }}</h1>
                <div class="text">
                    {!! $block->{'text_'.$locale} !!}
                </div>
        </div>
    </div>
    @endif
    <!-- Advertising -->
    <div class="wrap-bg-beige-3">
        <div class="container">
            <div class="row py-2">
                <div class="col">
                    <div class="advertising my-4"></div>
                </div>
            </div>
        </div>
    </div>

@endsection
