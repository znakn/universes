@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\AdminNews;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4 pb-2">
                    <p class="h1 text-bright">
                    <h1>{!! $news->{'title_'.$locale}  !!}</h1>
                    </p>
                </div>
            </div>

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4 pb-2">
                    <div class="main-image">

                        @if($news->image)
                        <img src="{{'/storage/'.$news->thumbnail('main','image')}}"  alt="Img" class="main_image_news">
                        @else
                            @if($news->video)
                                <iframe class="video-frame" src="https://www.youtube.com/embed/{{AdminNews::getYoutubeIdFromUrl($news->video)}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4 pb-2">
                    <div class="main-text">
                        {!! $news->{'text_'.$locale} !!}
                    </div>
                    <p>{{__('global.author')}} : {{$news->author->name}} , {{date('Y-m-d',strtotime($news->created_at))}}</p>
                </div>
            </div>



        </div>


    </div>

@endsection
