@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\Blog;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url,'menu' => $menu])

@section('content')
    <div class="wrap-bg-beige-3">
        <div class="wrap-map" id="map">
            @if($contacts->isNotEmpty())
            <iframe
                    width="100%"
                    height="100%"
                    frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAyumsD85VZG5aEmDb3bgs1xTN9sW440fc
			    &q={{$contacts[0]->address}}" allowfullscreen>
            </iframe>
            @endif
        </div>
        <div class="container pt-5">
            <div class="row">
                <div class="col-12">
                    <h1 class="h1">{{__('global.registred_office')}}</h1>
                    <h2 class="h2 text-uppercase">yourtips</h2>
                </div>
                @if($contacts->isNotEmpty())
                    @foreach($contacts as $contact)
                        <div class="col-12 col-md-4 pb-4">
                            <address>{{$contact->address}}</address>
                            @if($contact->phone1 or $contact->phone2)
                                tel: @if($contact->phone1) <a href="tel:{{$contact->phone1}}"
                                                              target="_blank">{{$contact->phone1}}</a> @endif @if($contact->phone1&&$contact->phone2)
                                    - @endif @if($contact->phone2) <a href="tel:{{$contact->phone2}}"
                                                                      target="_blank">{{$contact->phone2}}</a> @endif
                                <br>
                            @endif
                            @if($contact->email1)
                                <a href="mailto:{{$contact->email1}}"
                                   target="_blank">{{$contact->email1}}</a> @endif @if($contact->email1&&$contact->email2)
                                - @endif @if($contact->email2) <a href="mailto:{{$contact->email2}}"
                                                                  target="_blank">{{$contact->email2}}</a> @endif

                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
    @if(Auth::check())

        <div class="wrap-bg-bright">
            <div class="container pt-5 pb-2 mb-4">
                <div class="row">
                    <div class="col pt-5">
                        <h1 class="h1 text-lead-info">{{__('global.leave_your_tips')}}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-8">
                        <form method="post" action="{{LaravelLocalization::localizeUrl('/contact-with-us')}}" >
                            <input type="hidden" name="_token" value="{{@csrf_token()}}" />
                            <div class="form-group form-comment">
                                <label for="comment"></label>
                                <textarea class="form-control" id="comment" name="message" placeholder="{{__('global.some_text')}}"></textarea>
                            </div>
                            <div class="text-right">
                                <input type="submit" class="btn btn-main no-angle btn-lg text-uppercase" value="{{__('global.submit')}}" >
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    @endif
            <div class="container">
                @if(isset($block)&&($block->status == \App\Block::ENABLED))
                <div class="text-block py-5">
                    <h1 class="h2 title">{{$block->{'title_'.$locale} }}</h1>
                    <div class="text">
                        {!! $block->{'text_'.$locale} !!}
                    </div>

                </div>
                @endif
            </div>
        </div>
@endsection
