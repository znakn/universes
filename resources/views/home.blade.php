@php use App\Programs; @endphp
@extends('layouts.home')

@section('content')
    <div class="container-fluid wrap-links">
        <div class="languages at-home-page">
            <a href="/locale/it" @if(app()->getLocale() == 'it')  class="active" @endif >it</a> -
            <a href="/locale/en" @if(app()->getLocale() == 'en')  class="active" @endif >en</a>
        </div>
        <a href="/" class="logo-link">
            <img src="{{asset('/img/logo.svg')}}" alt="Logo">
        </a>
        <div class="row">
            @foreach($titles as $title)
                    <a href="{{$title['url']}}" class="block-link {{$title['class']}}">
                        <span class="name">{!! $title['title'] !!}</span>
                    </a>

            @endforeach
            <!-- schools -->
        </div>
    </div>
    <!-- big red button -->
    @include('cookieConsent::index')
@endsection
