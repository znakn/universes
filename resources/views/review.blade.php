@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row pt-3 pb-5">
                <div class="col-12 pt-4 pb-2">
                    <h1 class="h1">{!! $review->{'title_'.$locale}  !!}</h1>
                </div>
                <div class="col-12 col-sm-4">
                    <img src="{{'/storage/'.$review->image}}" alt="Img" class="img-fluid">
                </div>
                <div class="col-12 col-sm-8">
                    {!! $review->{'text_'.$locale} !!}
                    <p>{{date('Y-m-d',strtotime($review->created_at))}}</p>
                </div>
            </div>
        </div>
    </div>

@endsection
