@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url, 'menu' => $menu])

@section('content')
    <div class="wrap-bg-beige-3">
        <div class="container">
            <div class="row justify-content-center py-4">
                </br>
                </br>
                </br>
                </br>
                </br>
                    <h1>{{$page_title}}</h1>
                </br>
                </br>
                </br>
                </br>
                </br>
            </div>
            <div class="row" >
                @if(isset($return_url)&&(isset($return_text)))
                    <b><a href="{{$return_url}}">{{$return_text}}</a></b>
                @endif
            </div>
            <div class="row">
                <div class="advertising my-4">

                </div>
            </div>
        </div>
    </div>
@endsection
