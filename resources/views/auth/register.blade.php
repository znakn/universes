@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\Blog;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-md-8 col-lg-6 col-xl-4 my-5">
            <h5>{{__('register.title')}}</h5>
            <form class="form-horizontal needs-validation" method="POST" action="{{ route('register') }}" novalidate><!-- was-validated -->
                {{ csrf_field() }}


                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">{{__('register.first_name')}}</label>
                    <input id="name" type="text" class="form-control @if($errors->has('name')) is-invalid  @endif" name="name" value="{{ old('name') }}" required placeholder="{{__('register.first_name')}}">
                    <span class="invalid-feedback">
                    @if ($errors->has('name'))
                        {{ $errors->first('name') }}<br>
                    @else
                        {{__('register.error_first_name')}}<br>
                    @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="last_name">{{__('register.last_name')}}</label>
                    <input id="last_name" type="text" class="form-control @if($errors->has('last_name')) is-invalid  @endif" name="last_name" value="{{ old('last_name') }}" required placeholder="{{__('register.last_name')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('last_name'))
                            {{ $errors->first('last_name') }}<br>
                        @else
                            {{__('register.error_last_name')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                    <label for="country">{{__('register.country')}}</label>
                    <input id="country" type="text" class="form-control @if($errors->has('country')) is-invalid  @endif" name="country" value="{{ old('country') }}" required placeholder="{{__('register.country')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('country'))
                            {{ $errors->first('country') }}<br>
                        @else
                            {{__('register.error_country')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    <label for="register_city">{{__('register.city')}}</label>
                    <input id="register_city" type="text" class="form-control @if($errors->has('city')) is-invalid  @endif  " name="city" value="{{ old('city') }}" required placeholder="{{__('register.city')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('city'))
                            {{ $errors->first('city') }}<br>
                        @else
                            {{__('register.error_city')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('date_birth') ? ' has-error' : '' }}">
                    <label for="date_birth">{{__('register.date_birth')}}</label>
                    <input id="date_birth" type="text" class="form-control @if($errors->has('date_birth')) is-invalid  @endif  " name="date_birth" value="{{ old('date_birth') }}" required placeholder="{{__('register.date_birth')}}">
                    <span class="invalid-feedback">
                        @if ($errors->has('date_birth'))
                            {{ $errors->first('date_birth') }}<br>
                        @else
                            {{__('register.date_birth')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">{{__('register.email')}}</label>
                    <input id="email" type="email" class="form-control @if($errors->has('email')) is-invalid @endif " name="email" value="{{ old('email') }}" required placeholder="{{__('register.email')}}">
                    <span class="invalid-feedback">
                    @if ($errors->has('email'))
                          {{ $errors->first('email') }}<br>
                        @else
                          {{__('register.error_email')}}<br>
                    @endif
                    </span>
                </div>

                <div class="form-group{{ $errors->has('prefer_language') ? ' has-error' : '' }}">
                    <label for="prefer_language">{{__('register.prefer_language')}}</label>
                    <select id="prefer_language" class="form-control @if($errors->has('prefer_language')) is-invalid @endif " name="prefer_language"  placeholder="{{__('register.prefer_language')}}">
                    <option value="it" @if (old('prefer_language')&&(old('prefer_language') == 'it')  ) selected="selected" @endif   >IT</option>
                    <option value="en" @if (old('prefer_language')&&(old('prefer_language') == 'en')  ) selected="selected" @endif   >EN</option>
                    </select>
                        <span class="invalid-feedback">
                    @if ($errors->has('prefer_language'))
                            {{ $errors->first('prefer_language') }}<br>
                        @else
                            {{__('register.error_prefer_language')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group password {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">{{__('register.password')}}</label>
                    <input id="password" type="password" class="form-control @if($errors->has('password')) is-invalid @endif " name="password" required placeholder="Password">
                    <a href="#" class="password-control"></a>
                    <span class="invalid-feedback">
                        @if ($errors->has('password'))
                            {{ $errors->first('password') }}<br>
                        @else
                            {{__('register.password')}}<br>
                        @endif
                    </span>
                </div>


                <div class="form-group password-conf">
                    <label for="password-confirm">{{__('register.password_confirm')}}</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="{{__('register.password_confirm')}}">
                    <a href="#" class="password-control"></a>
                </div>

                <div class="form-group {{ $errors->has('is_agree_terms') ? ' has-error' : '' }} form-check">
                    <input type="checkbox" class="form-check-input" id="confirmCheckbox" required name="is_agree_terms" >
                    <label class="form-check-label" for="confirmCheckbox">
                        {{__('register.is_agree_terms1')}}<a href="{{LaravelLocalization::localizeUrl('/terms_condition')}}" target="_blank" ><strong>{{__('register.is_agree_terms2')}}</strong></a>{{__('register.is_agree_terms3')}} <a href="{{LaravelLocalization::localizeUrl('/coocies_police')}}" target="_blank" ><strong>{{__('register.is_agree_terms4')}}</strong></a>.
                    </label>
                    <span class="invalid-feedback">
                        @if ($errors->has('is_agree_terms'))
                            <strong>{{ $errors->first('is_agree_terms') }}</strong>
                        @else
                            {{__('register.error_agree_terms')}}<br>
                        @endif
                    </span>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-dark btn-lg btn-block">{{__('register.register')}}</button>
                </div>

                <p class="text-center">
                    {{__('register.have_account')}}
                    <a href="#" data-toggle="modal" data-target="#signinModal">
                        <b>{{__('register.login')}}</b>
                    </a>
                </p>
                <!-- <p class="text-center">
                    Vuoi accedere con Facebook o Google?
                    <a href="#">
                        <b>Indietro</b>
                    </a>
                </p> -->

                <!--div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="subscriptionCheckbox">
                    <label class="form-check-label" for="subscriptionCheckbox">
                        Si, desidero ricevere informazioni su università e scuole, suggerimenti e tips. Posso annulare questa richiesta in qualsiasi momento.
                    </label>
                </div-->
            </form>
        </div>
    </div>
</div>
@endsection
