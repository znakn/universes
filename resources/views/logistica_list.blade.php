@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\LogisticaPosts;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url,'menu' => $menu])

@section('content')
    <!-- Apartments -->
    <div class="wrap-bg-beige-3 pb-2">
        <div class="container">
                <div class="row justify-content-center py-4">
                    <div class="col-12 col-md-8">
                        @include('parts/search_logistica_form')
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right order-12">
                        @if(Auth::check())
                        <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/post-form/'.$form_type)}}" class="btn btn-main btn-lg p-3 font-weight-bold">{{__('Add')}}</a>
                        @endif
                    </div>
                    <div class="col order-1">
                        <h1 class="h1 mb-0">{{__('accommodation.title')}}</h1>

                    </div>
                </div>
            <div class="row py-4">
                <div class="col">
                    @if($apartmentsReview->isNotEmpty())
                        @foreach($apartmentsReview as $review)
                            <div class="lib-block media mb-5">
                                <div class="lib-who">
                                    <img src="{{'/upload/'.LogisticaPosts::FORM_TYPE.'/thumb_'.$review->image}}" alt="Img" class="img-fluid"><br>
                                    {{$review->author->name}}<br>
                                    {{date('Y-m-d',strtotime($review->created_at))}}
                                </div>
                                <div class="media-body lib-body">
                                    <h5 class="h5 mt-0">{{$review->{'title_'.$locale} }}</h5>
                                    <p>
                                        {!! $review->{'title_'.$locale} !!}
                                    </p>
                                    <a href="{{LaravelLocalization::localizeUrl('/logistica-post/'.$review->url)}}" class="font-weight-bold">{{__('Leggi tutto')}}</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="lib-block media mb-5">
                            <h3>{{__('global.no_apartament')}}</h3>
                        </div>
                    @endif



                </div>

            </div>
            <div class="row pb-4">
                <div class="col" >
                    <nav aria-label="Page navigation">
                        {{$apartmentsReview->links()}}
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection
