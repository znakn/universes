@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>$seo, 'program_ur'=>$program_url,'menu' => $menu])

@section('content')

    <div class="wrap-bg-beige-5">
        <div class="container">

            <div class="row justify-content-center pt-3 pb-5">
                <div class="col-12 pb-4">
                    <h1 class="h1">{!! $news->{'title_'.$locale}  !!}</h1>
                </div>
                <div class="col-8 pb-5">
                    @if($news->image)
                    <img src="{{'/upload/news/main_'.$news->image}}" alt="Img" class="img-fluid">
                    @endif
                </div>
                <div class="col-12">
                    {!! $news->{'text_'.$locale} !!}
                    <p>
                        <b>{{__('global.author')}}</b>:
                        {{$news->author->name}} , {{date('Y-m-d',strtotime($news->created_at))}}
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection
