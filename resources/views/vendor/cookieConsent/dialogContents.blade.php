<div class="js-cookie-consent cookie-consent cookie-dialog">

    <div class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
        <a href="{{'/'.app()->getLocale().'/coocies_police'}}" target="_blank" >{!! trans('cookieConsent::texts.message_link') !!}</a>
        {!! trans('cookieConsent::texts.message_more') !!}
    </div>

    <div class="text-center mt-3">
	    <button class="js-cookie-consent-agree cookie-consent__agree btn btn-primary btn-r0">
	        {{ trans('cookieConsent::texts.agree') }}
	    </button>
    </div>

</div>
