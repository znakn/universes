@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
@endphp

@extends('layouts.main',['color'=>$color,'homeUrl'=>$homeUrl,'seo'=>[], 'program_ur'=>$program_url ,'menu' => $menu])

@section('content')
    <!-- Images -->
    <div class="wrap-bg-beige-3 wrap-social-images text-center">
        <!-- <img src="../img/socialblog.webp" alt="Img" class="img-fluid"> -->
        <!-- <img src="../img/" alt=""> -->
        @if(Auth::check())
        <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/post-form/blog')}}" class="btn btn-primary btn-block btn-big">{{__('blog.add_content')}}</a>
        @else
        <div class="btn btn-primary btn-block btn-big">{{__('blog.login')}}</div>
        @endif
    </div>

    <!-- Comments -->
    <div class="wrap-bg-beige-3 pb-2">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="display-4 my-4 text-bright">{{__('blog.our_voice')}}</h1>
                </div>
            </div>
            <div class="row py-4">
                <div class="col">
                    @if($blogs->isNotEmpty())
                        @foreach($blogs as $blog)
                            <div class="lib-block media mb-5">
                                <div class="lib-who">
                                    <img src="{{'/upload/blog/thumb_'.$blog->image}}" alt="Img" class="img-fluid"><br>
                                    {{$blog->author->name}}<br>
                                    {{date('Y-m-d',strtotime($blog->created_at))}}
                                </div>
                                <div class="media-body lib-body">
                                    <h5 class="h5 mt-0">{{$blog->{'title_'.$locale} }}</h5>
                                    <p>
                                        {!! $blog->{'text_'.$locale} !!}
                                    </p>
                                    <a href="{{LaravelLocalization::localizeUrl('/blog/'.$blog->url)}}" class="font-weight-bold">{{__('global.read_all')}}</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="lib-block media mb-5">
                            <h3>{{__('global.no_blog_post')}}</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Text block -->
    @if(isset($blogBlock)&&($blogBlock->status == \App\Block::ENABLED))
    <div class="wrap-bg-beige-2">
        <div class="container">
            <div class="text-block py-5">
                <h2 class="h2 title">{{$blogBlock->{'title_'.$locale} }}</h2>
                <div class="text">
                    {!! $blogBlock->{'text_'.$locale} !!}
                </div>
        </div>
    </div>
    @endif

    <!-- Advertising -->
    <div class="wrap-bg-beige-1">
        <div class="container">
            <div class="row py-2">
                <div class="col">
                    <div class="advertising my-4"></div>
                </div>
            </div>
        </div>
    </div>




@endsection
