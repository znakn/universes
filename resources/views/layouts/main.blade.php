@php
    use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
    use Illuminate\Support\Facades\Auth;
    use App\BooksCornerPosts;
    use App\LogisticaPosts;
    use App\Blog;
    use App\BookCornerSections;
    use App\Programs;


    $bookCornerSections = BookCornerSections::getListByProgram($program_url);
    $locale = app()->getLocale();


@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>@if(isset($seo['title'])) {{'YourTips '.$seo['title']}} @else {{'YourTips'}} @endif </title>
    <meta name="description" value="@if(isset($seo['description'])) {{$seo['description']}} @else '' @endif" >
    <meta name="keywords" value="@if(isset($seo['keywords'])) {{$seo['keywords']}} @else '' @endif" >
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('/css/additional.css')}}">
    <link rel="stylesheet" href="{{asset('/css/select2/select2.css')}}"  />
    <!-- <link rel="stylesheet" href="{{asset('/css/select2/select2-bootstrap4.css')}}"> -->
    <!--link rel="stylesheet" href="{{asset('/js/datepicker/bootstrap-datepicker.standalone.css')}}"  /-->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />
    <link rel="shortcut icon" href="{{'/img/logo.ico'}}" type="image/x-icon">
    <link href="/icons/css/all.css" rel="stylesheet"> <!--load all styles -->
</head>
<body>
<!-- Use .green or .blue color to change color map of page -->
<body class="{{$color}}">
<div class="header">
    <div class="container pb-3">
        <div class="row">
            <div class="col pt-2">
                <div class="d-flex flex-row float-right">
                    <div class="px-2 languages">
                        <a href="/locale/it" @if(app()->getLocale() == 'it')  class="active" @endif >it</a> -
                        <a href="/locale/en" @if(app()->getLocale() == 'en')  class="active" @endif >en</a>
                    </div>
                    <div class="socials">
                        <a href="#" class="facebook"></a>
                        <a href="#" class="pinterest"></a>
                        <a href="#" class="instagram"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="navbar col col-lg-3 my-lg-n1">
                <div class="col col-sm-5 col-md-4 col-lg-12 px-0 my-n3-lg">
                    <a class="navbar-brand navbar-logo-link py-0 my-n1" href="{{$homeUrl}}">
                        @if((app()->getLocale() == 'it')&&($program_url == \App\Programs::MASTER_UNIVERSITY))
                                <img src="{{asset('/img/Universita_it.png')}}" alt="Logo" alt="" loading="lazy" class="logo size-big">
                        @endif
                        @if((app()->getLocale() == 'it')&&($program_url == \App\Programs::SECONDARY_SCHOOL))
                                <img src="{{asset('/img/scuola_it.png')}}" alt="Logo" alt="" loading="lazy" class="logo size-big">
                        @endif
                        @if((app()->getLocale() == 'en')&&($program_url == \App\Programs::MASTER_UNIVERSITY))
                                <img src="{{asset('/img/University_en.png')}}" alt="Logo" alt="" loading="lazy" class="logo size-big">
                        @endif
                            @if((app()->getLocale() == 'en')&&($program_url == \App\Programs::SECONDARY_SCHOOL))
                                <img src="{{asset('/img/school_en.png')}}" alt="Logo" alt="" loading="lazy" class="logo size-big">
                        @endif
                    </a>
                </div>
                <div class="col px-0 btn-menu d-lg-none text-right">
                    <button class="btn navbar-toggler" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
            <div class="col-12 col-lg-9 collapse d-lg-block" id="collapseExample">
                <div class="row mt-4">
                    <div class="col-12 col-lg-11">
                        <nav class="nav nav-fill menu">
                            <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/list')}}" class="nav-link @if($menu == 'tips') active @endif">{{__('menu.tips')}}</a>
                            <div class="btn-group nav-item nav-item-submenu">
                                <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/list/books-corner')}}" class="nav-link @if($menu == BooksCornerPosts::FORM_TYPE) active @endif nav-link-submenu">
                                    {{__('menu.books_corner')}}
                                </a>

                                @if ($bookCornerSections->isNotEmpty())
                                <div class="dropdown-menu main-menu-submenu">
                                    @foreach($bookCornerSections as $section)
                                        <a class="dropdown-item" href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/list/books-corner/'.$section->url)}}">{{$section->{'title_'.$locale} }}</a>
                                    @endforeach
                                    <!--a class="dropdown-item" href="#">Some link</a>
                                    <a class="dropdown-item" href="#">Else link</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Separated link</a-->
                                </div>
                                @endif
                            </div>
                            <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/list/logistica')}}" class="nav-link @if($menu == LogisticaPosts::FORM_TYPE) active @endif ">{{__('menu.logistica')}}</a>
                            <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/list/blog')}}" class="nav-link @if($menu == Blog::FORM_TYPE) active @endif">{{__('menu.social_blog')}}</a>
                            <a href="{{LaravelLocalization::localizeUrl('/'.$program_url.'/list/news')}}" class="nav-link @if($menu == 'news') active @endif">{{__('menu.tips_news')}}</a>
                            <a href="{{LaravelLocalization::localizeUrl('/info')}}" class="nav-link  @if($menu == 'info') active @endif" tabindex="-1" aria-disabled="true">{{__('menu.info')}}</a>
                            
                            <!-- @if(Auth::check())
                                <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            @endif -->

                        </nav>
                    </div>

                    <div class="col-12 col-lg-1 signin-wrap mt-n1">
                        @if(!Auth::check())
                            <a href="#" class="signin pt-lg-2" data-toggle="modal" data-target="#signinModal">
                                <img src="{{asset('/img/man.svg')}}" alt="Sign in" width="21" height="29">
                                <!-- <span class="text">Sign in</span> -->
                            </a>
                        @else
                            <a href="#" class="signin pt-lg-2">
                                <img src="{{asset('/img/man.svg')}}" alt="Sign in" width="21" height="29">
                                <!-- <span class="text">Sign in</span> -->
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg-right menu-signin">
                                <a class="dropdown-item" href="{{ LaravelLocalization::localizeUrl('/profile') }}">{{__('menu.profile')}}</a>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="signinModal" tabindex="-1" aria-labelledby="signinModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- https://getbootstrap.com/docs/4.5/components/forms/#validation -->
            <form method="post" action="/login" class="needs-validation" novalidate><!-- was-validated -->
                <input type="hidden" name="_token" value="{{@csrf_token()}}" />
                <input type="hidden" name="program" value="{{$program_url}}" />
                <div class="modal-header">
                    <div class="row">
                        <div class="col-12">
                            <img src="{{asset('/img/logo.svg')}}" alt="YourTips" class="img-fluid">
                        </div>
                        <div class="col-12">
                            <h5 class="modal-title" id="signinModalLabel">{{__('login.return_hello')}}</h5>
                        </div>
                    </div>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputLogin">{{__('login.email')}}</label>
                        <!-- Use is-invalid class for server-side validation. Read more https://getbootstrap.com/docs/4.5/components/forms/#server-side -->
                        <input type="email"  name="email" class="form-control" id="inputLogin" required>
                        <div class="invalid-feedback">
                            {{__('login.email_invalid_feedback')}}
                        </div>
                    </div>
                    <div class="form-group password">
                        <label for="inputPassword" >{{__('login.password')}}</label>
                        <!-- Use is-invalid class for server-side validation. Read more https://getbootstrap.com/docs/4.5/components/forms/#server-side -->
                        <input type="password" name="password"  class="form-control" id="inputPassword" required>
                        <a href="#" class="password-control"></a>


                        <div class="invalid-feedback">
                            {{__('login.pass_invalid_feedback')}}
                        </div>
                    </div>
                    <button type="submit" class="btn btn-dark btn-lg btn-block">{{__('login.log_in')}}</button>
                </div>
                <div class="modal-footer">
                    <div>
                        <p class="text-center">
                            <a href="{{'/password/reset'}}"><strong>{{__('login.forgot_password')}}</strong></a>
                        </p>
                        <p class="text-center">
                            {{__('login.not_account')}} <a href="{{LaravelLocalization::localizeUrl('/register')}}" class="text-bright"><strong>{{__('login.registration')}}</strong></a>
                        </p>
                        <!-- <p class="text-center">
                            Vuoi accedere con Facebook o Google? <a href="#"><strong>Indietro</strong></a>
                        </p> -->
                        <p class="text-center">{{__('login.is_agree_terms1')}} <a href="{{LaravelLocalization::localizeUrl('/terms_condition')}}" target="_blank" ><strong>{{__('login.is_agree_terms2')}}</strong></a> {{__('login.is_agree_terms3')}} <a href="{{LaravelLocalization::localizeUrl('/coocies_police')}}" target="_blank" ><strong>{{__('login.is_agree_terms4')}}</strong></a>.</p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@yield('content')


<!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-6 col-sm-4 col-md-2">
                <a href="#">
                    <img src="{{'/img/logo.svg'}}" alt="Logo">
                </a>
            </div>
            <!--div class="col-12 col-sm-8 col-md-10 info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa odio quam magnam qui vel blanditiis dignissimos similique? Aperiam sit numquam libero praesentium sunt quas tempora doloribus voluptates expedita.
            </div-->
        </div>
    </div>
</div>

<button
        type="button"
        class="btn btn-danger btn-floating btn-lg"
        id="btn-back-to-top"
>
    <i class="fas fa-arrow-up"></i>
</button>

<style>
    .password {
        position: relative;
    }
    .password-conf {
        position: relative;
    }
    .password-old {
        position: relative;
    }
    .password-control {
        position: absolute;
        top: 44px;
        right: 15px;
        display: inline-block;
        width: 20px;
        height: 20px;
        background: url(/img/view.svg) 0 0 no-repeat;
    }
    .password-control.view {
        background: url(/img/no-view.svg) 0 0 no-repeat;
    }

    #btn-back-to-top {
        position: fixed;
        bottom: 20px;
        right: 20px;
        display: none;
    }

</style>

<script>
    let mybutton = document.getElementById("btn-back-to-top");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
        scrollFunction();
    };

    function scrollFunction() {
        if (
            document.body.scrollTop > 20 ||
            document.documentElement.scrollTop > 20
        ) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
    }
    // When the user clicks on the button, scroll to the top of the document
    mybutton.addEventListener("click", backToTop);

    function backToTop() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }


</script>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('/js/jquery-3.5.1.js')}}" ></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<!-- https://mikeking.io/succinct/ -->
<script type="text/javascript" src="{{asset('/js/succinct-master/jQuery.succinct.min.js')}}"></script>
<!-- https://rangeslider.js.org/ -->
<script type="text/javascript" src="{{asset('/js/rangeslider.js-2.3.0/rangeslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/select2/select2.full.js')}}"></script>
<!--script type="text/javascript" src="{{asset('/js/datepicker/js/bootstrap-datepicker.js')}}"></script-->
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

<script type="text/javascript" src="{{asset('/js/custom.js')}}" defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js" integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>

@yield('image-script')

</body>
</html>
