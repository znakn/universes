<?php

use App\Programs;
use Illuminate\Database\Seeder;

class ProgramsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programs = [Programs::SECONDARY_SCHOOL=>[
            'url'=>Programs::SECONDARY_SCHOOL,
            'title_it'=>'Scuola media',
            'title_en'=>'Secondary school',
            'description_it'=>'Descrizione della scuola secondaria',
            'description_en'=>'Secondary school description',
            'text_it'=>'Testo descrittivo della scuola secondaria',
            'text_en'=>'Secondary school description text',
            'status'=>Programs::ENABLED,
        ],Programs::MASTER_UNIVERSITY=>[
            'url'=>Programs::MASTER_UNIVERSITY,
            'title_it'=>'Master e Università',
            'title_en'=>'Master & University',
            'description_it'=>'Descrizione Master e Università',
            'description_en'=>'Master & University description',
            'text_it'=>'Master e testo universitario',
            'text_en'=>'Master & University text',
            'status'=>Programs::ENABLED,
        ]];



        DB::statement("SET foreign_key_checks=0");
        // CLEAR PROGRAMS TABLE
        Programs::query()->delete();

        foreach ($programs as $program)
        {
            $p = new Programs();
            $p->url = $program['url'];
            $p->title_it = $program['title_it'];
            $p->title_en = $program['title_en'];
            $p->description_it = $program['description_it'];
            $p->description_en = $program['description_en'];
            $p->text_it = $program['text_it'];
            $p->text_en = $program['text_en'];
            $p->status = $program['status'];
            $p->save();
        }

        DB::statement("SET foreign_key_checks=1");

    }
}
