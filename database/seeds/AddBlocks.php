<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Block;

class AddBlocks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blocks = ['book_corner'=>[
            'title_it'=>'Book Corner',
            'title_en'=>'Book Corner',
            'text_it'=>'This is book corner page',
            'text_en'=>'This is book corner page',
            'status'=>Block::ENABLED,
        ],'blog_page'=>[
            'title_it'=>'Blog page',
            'title_en'=>'Blog page',
            'text_it'=>'This blog corner page',
            'text_en'=>'This blog corner page',
            'status'=>Block::ENABLED,
        ],'news_page'=>[
            'title_it'=>'News page',
            'title_en'=>'News page',
            'text_it'=>'This is news page',
            'text_en'=>'This is news page',
            'status'=>Block::ENABLED,
        ],'info_page'=>[
            'title_it'=>'Info page',
            'title_en'=>'Info page',
            'text_it'=>'This is info page',
            'text_en'=>'This is info page',
            'status'=>Block::ENABLED,
        ]
            ];



        DB::statement("SET foreign_key_checks=0");
        // CLEAR PROGRAMS TABLE
        Block::query()->delete();

        foreach ($blocks as $page => $blog)
        {
            $bl = new Block();
            $bl->page = $page;
            $bl->title_it = $blog['title_it'];
            $bl->title_en = $blog['title_en'];
            $bl->text_it = $blog['text_it'];
            $bl->text_en = $blog['text_en'];
            $bl->status = $blog['status'];
            $bl->save();
        }

        DB::statement("SET foreign_key_checks=1");

    }

}
