<?php

use Illuminate\Database\Seeder;

class AddUnknownLocation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $region = \App\Regions::where('id','>','0')->first();
        $province = \App\Province::where(['region_id'=>$region->id])->first();
        $unknownCity = new \App\Cities();
        $unknownCity->country_id = 1;
        $unknownCity->region_id = $region->id;
        $unknownCity->province_id = $province->id;
        $unknownCity->name_it = 'Unknown';
        $unknownCity->name_en = 'Unknown';
        $unknownCity->url = \App\Helpers\Slug::url('Unknown');
        $unknownCity->save();
    }
}
