<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LocationSeeder::class);
        $this->call(ProgramsSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(AddUnknownLocation::class);
    }
}
