<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEducationEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_entities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->nullable();
            $table->string('logo')->nullable();
            $table->string('web_address')->nullable();
            $table->string('facebook_address')->nullable();
            $table->string('twitter_address')->nullable();
            $table->string('instagram_address')->nullable();
            $table->string('linkedin_address')->nullable();
            $table->integer('program_id');
            $table->integer('institution_id');
            $table->integer('country_id');
            $table->integer('region_id');
            $table->integer('city_id');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('name_it')->nullable();
            $table->string('name_en')->nullable();
            $table->text('description_it')->nullable();
            $table->text('description_en')->nullable();
            $table->text('text_it')->nullable();
            $table->text('text_en')->nullable();
            $table->string('program_title_en')->nullable();
            $table->string('program_title_it')->nullable();
            $table->text('program_text_it')->nullable();
            $table->text('program_text_en')->nullable();
            $table->string('image')->nullable();
            // SEO
            $table->string('seo_title_it')->nullable();
            $table->string('seo_title_en')->nullable();
            $table->text('seo_decription_it')->nullable();
            $table->text('seo_description_en')->nullable();
            $table->text('seo_keywords_it')->nullable();
            $table->text('seo_keywords_en')->nullable();

            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('education_entities');
    }
}
