<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePublicationTable extends Migration
{

    private $tableName = 'books_corner_posts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->integer('type_id')->unsigned();
            $table->string('curse',1024)->nullable();
            $table->string('author',1024)->nullable();
            $table->foreign('type_id')->references('id')->on('book_corner_types')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropForeign(['type_id']);
            $table->dropColumn('type_id');
            $table->dropColumn('curse');
            $table->dropColumn('author');

        });
    }
}
