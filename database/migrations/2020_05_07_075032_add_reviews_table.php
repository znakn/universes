<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_id')->unsigned();
            $table->foreign('entity_id')->references('id')->on('education_entities');
            $table->string('image')->nullable();
            $table->string('url',1024);
            $table->string('title_it')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_it')->nullable();
            $table->text('text_en')->nullable();
            // SEO
            $table->string('seo_title_it')->nullable();
            $table->string('seo_title_en')->nullable();
            $table->text('seo_decription_it')->nullable();
            $table->text('seo_description_en')->nullable();
            $table->text('seo_keywords_it')->nullable();
            $table->text('seo_keywords_en')->nullable();

            $table->integer('status')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
