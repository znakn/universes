<?php

use App\Programs;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeEntityRelationToProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET foreign_key_checks=0");
        if (!Schema::hasColumn('education_entities', 'program'))
        {
            Schema::table('education_entities', function (Blueprint $table) {
            //
            $table->string('program','255')->default(Programs::MASTER_UNIVERSITY);
            });
        }
        Schema::dropIfExists('entities_programs');
        DB::statement("SET foreign_key_checks=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET foreign_key_checks=0");
        Schema::table('education_entities', function (Blueprint $table) {
            //
            $table->dropColumn('program');
        });

        Schema::create('entities_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
            $table->foreign('program_id')->references('id')->on('programs');
            $table->integer('entity_id')->unsigned();
            $table->foreign('entity_id')->references('id')->on('education_entities');
        });

        DB::statement("SET foreign_key_checks=1");
    }
}
