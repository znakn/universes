<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSvgFieldToRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->addColumn('text','svg_vertical')->nullable();
            $table->addColumn('text','svg_horizontal')->nullable();
            $table->addColumn('text','svg_view_box')->nullable();
            $table->addColumn('text','svg_style')->nullable();
            $table->addColumn('integer','svg_order')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->dropColumn('svg_vertical');
            $table->dropColumn('svg_horizontal');
            $table->dropColumn('svg_view_box');
            $table->dropColumn('svg_style');
            $table->dropColumn('svg_order');
        });
    }
}
