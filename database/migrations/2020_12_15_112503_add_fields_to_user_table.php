<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserTable extends Migration
{
    private $tableName = 'users';


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->string('last_name',1024)->nullable();
            $table->string('country',1024)->nullable();
            $table->string('city',1024)->nullable();
            $table->string('date_birth',1024)->nullable();
            $table->integer('is_agree_terms')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->dropColumn('country');
            $table->dropColumn('city');
            $table->dropColumn('date_birth');
            $table->dropColumn('is_agree_terms');

        });
    }
}
