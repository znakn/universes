<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminNewsTable extends Migration
{
    private $table = 'admin_news';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('url',1024);
            $table->string('video',1024)->nullable();
            $table->string('image')->nullable();
            $table->string('program',255);
            $table->string('title_it')->nullable();
            $table->string('title_en')->nullable();
            $table->integer('user_id');
            $table->text('text_it')->nullable();
            $table->text('text_en')->nullable();
            // SEO
            $table->string('seo_title_it')->nullable();
            $table->string('seo_title_en')->nullable();
            $table->text('seo_description_it')->nullable();
            $table->text('seo_description_en')->nullable();
            $table->text('seo_keywords_it')->nullable();
            $table->text('seo_keywords_en')->nullable();
            $table->integer('vote_count')->nullable();
            $table->integer('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
