<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->change();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
        });

        Schema::table('cities', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->change();
            $table->integer('region_id')->unsigned()->change();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('restrict');
        });

        Schema::table('education_entities', function (Blueprint $table) {
            $table->integer('country_id')->unsigned()->change();
            $table->integer('region_id')->unsigned()->change();
            $table->integer('city_id')->unsigned()->change();
            $table->integer('institution_id')->unsigned()->change();


            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('restrict');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('restrict');
            $table->foreign('institution_id')->references('id')->on('institutions')->onDelete('restrict');
        });

        Schema::table('disciplines', function (Blueprint $table) {
            $table->integer('entity_id')->unsigned()->change();
            $table->foreign('entity_id')->references('id')->on('education_entities')->onDelete('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
        });

        Schema::table('cities', function (Blueprint $table) {
            $table->dropForeign(['country_id','region_id']);
        });

        Schema::table('education_entities', function (Blueprint $table) {
            $table->dropForeign(['country_id','region_id','city_id','institution_id']);
        });

        Schema::table('disciplines', function (Blueprint $table) {
            $table->dropForeign(['entity_id']);
        });

    }
}
