<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLogisticaTable extends Migration
{
    private $tableName = 'logistica_posts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->addColumn('integer','type_id')->unsigned();
            $table->addColumn('integer','region_id')->unsigned();
            $table->addColumn('integer','city_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('book_corner_types')->onDelete('restrict');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('restrict');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropForeign(['type_id','region_id','city_id']);
            $table->dropColumn('type_id');
            $table->dropColumn('region_id');
            $table->dropColumn('city_id');

        });
    }
}
