$(document).ready(function() {


    $('.password-old').on('click', '.password-control', function(){
        if ($('#old_password')!=undefined) {
            if ($('#old_password').attr('type') == 'password'){
                $(this).addClass('view');
                $('#old_password').attr('type', 'text');
            } else {
                $(this).removeClass('view');
                $('#old_password').attr('type', 'password');
            }
        }
        return false;
    });

    $('.password-conf').on('click', '.password-control', function(){
        if ($('#password-confirm')!=undefined) {
            if ($('#password-confirm').attr('type') == 'password'){
                $(this).addClass('view');
                $('#password-confirm').attr('type', 'text');
            } else {
                $(this).removeClass('view');
                $('#password-confirm').attr('type', 'password');
            }
        }

        if ($('#password_confirm')!=undefined) {
            if ($('#password_confirmation').attr('type') == 'password'){
                $(this).addClass('view');
                $('#password_confirmation').attr('type', 'text');
            } else {
                $(this).removeClass('view');
                $('#password_confirmation').attr('type', 'password');
            }
        }
        return false;
    });

    $('.password').on('click', '.password-control', function(){
        if ($('#inputPassword')!=undefined) {
            if ($('#inputPassword').attr('type') == 'password'){
                $(this).addClass('view');
                $('#inputPassword').attr('type', 'text');
            } else {
                $(this).removeClass('view');
                $('#inputPassword').attr('type', 'password');
            }
        }
        if ($('#password')!=undefined) {
            if ($('#password').attr('type') == 'password'){
                $(this).addClass('view');
                $('#password').attr('type', 'text');
            } else {
                $(this).removeClass('view');
                $('#password').attr('type', 'password');
            }
        }




        return false;
    });

	$("<div class='push'></div>").insertBefore(".footer");
	$(".push").css('height', $('.footer').outerHeight());

	/* Event click for map items */
	$(".map-item").on("click", function(event) {
		console.log($(this).data("info"));
        window.location.href = $(this).data("url");
	});

	$(".map-item").mouseover(function () {
        var title = $(this).data("info");
        console.log(title);
        $(this).tooltip({
            'title':title
        });
    });




	/* Convert address tags to google map links - Copyright Michael Jasper 2011 */
	$("address").each(function () {
		var link = "<a href='http://maps.google.com/maps?q=" + encodeURIComponent($(this).text()) + "' target='_blank'>" + $(this).html() + "</a>";
		$(this).html(link);
	});
	$('input[type="range"]').rangeslider('update', true);

	$(".vote-range").on("input", function() {
		document.getElementById($(this).data("target")).innerHTML = this.value;
	});

	// $('.stars-input').on("input", function() {
	// 	document.getElementById($(this).data("target")).innerHTML = this.value;
	// });

	$('[data-toggle="tooltip"]').tooltip();

	$('#regions').select2({
		placeholder: $('#placeholder_region').val(),
		theme:'bootstrap4-filter',
        allowClear: true
	});

    $('#province').select2({
        placeholder: $('#placeholder_province').val(),
        theme:'bootstrap4-filter',
        disabled: ($('#has_province').val() == 'true') ? false: true,
        allowClear: true
    });


	$('#city').select2({
        placeholder: $('#placeholder_city').val(),
        theme:'bootstrap4-filter',
		disabled: ($('#has_city').val() == 'true') ? false: true,
        allowClear: true
    });

    $('#regions').change(function () {
        var region = $('#regions option:selected').val();
        var locale = $('#locale').val();
        $.post('/search/provinces',{'region':region},function (r) {
            if (r.res = 'ok') {
                $('#province').html('');
                var provinces = r.provinces;
                var emptyOption = new Option('','',false,false);

                $('#province').append(emptyOption).trigger('change');
                $('#province').attr('data-placeholder',$('#placeholder_province').val()).trigger('change');
                $(provinces).each(function (key,province) {
                    var newOption = new Option(province['name'],province['url'],false,false);
                    newOption.dataset.province_id = province['id'];
                    $('#province').append(newOption);

                });
                $('#province').prop('disabled', false).trigger('change');
            }
        });
    });



	$('#province').change(function () {
		var province = $('#province option:selected').val();
		if (province) {
            var locale = $('#locale').val();
            $.post('/search/cities',{'province':province},function (r) {
                if (r.res = 'ok') {
                    $('#city').html('');
                    var cities = r.cities;
                    var emptyOption = new Option('','',false,false);

                    $('#city').append(emptyOption).trigger('change');
                    $('#city').attr('data-placeholder',$('#placeholder_city').val()).trigger('change');
                    $(cities).each(function (key,city) {
                        var newOption = new Option(city['name'],city['url'],false,false);
                        newOption.dataset.city_id = city['id'];
                        $('#city').append(newOption);

                    });
                    $('#city').prop('disabled', false).trigger('change');
                }
            });
        }

    });

    $('#logistica_regions').select2({
        placeholder: $('#placeholder_region').val(),
        theme:'bootstrap4-filter',
        allowClear: true
    });

    $('#logistica_provinces').select2({
        placeholder: $('#placeholder_province').val(),
        theme:'bootstrap4-filter',
        disabled: ($('#has_provinces').val() == 'true') ? false: true,
        allowClear: true
    });

    $('#logistica_city').select2({
        placeholder: $('#placeholder_city').val(),
        theme:'bootstrap4-filter',
        disabled:  ($('#has_city').val() == 'true') ? false: true,
        allowClear: true
    });

    $('#logistica_regions').change(function () {
        var region_id = $('#logistica_regions option:selected').val();
        var locale = $('#locale').val();
        $.post('/search/provinces-by-id',{'region_id':region_id},function (r) {
            if (r.res = 'ok') {
                $('#logistica_provinces').html('');
                var provinces = r.provinces;
                var emptyOption = new Option('','',false,false);

                $('#logistica_provinces').append(emptyOption).trigger('change');
                $('#logistica_provinces').attr('data-placeholder',$('#placeholder_province').val()).trigger('change');
                $(provinces).each(function (key,province) {
                    var newOption = new Option(province['name'],province['id'],false,false);
                    newOption.dataset.province_id = province['id'];
                    $('#logistica_provinces').append(newOption);

                });
                $('#logistica_provinces').prop('disabled', false).trigger('change');
            }
        });
    });

    $('#logistica_provinces').change(function () {
        var province_id = $('#logistica_provinces option:selected').data('province_id');
        var locale = $('#locale').val();
        if (province_id) {
            $.post('/search/cities-by-id',{'province_id':province_id},function (r) {
                if (r.res = 'ok') {
                    $('#logistica_city').html('');
                    var cities = r.cities;
                    var emptyOption = new Option('','',false,false);

                    $('#logistica_city').append(emptyOption).trigger('change');
                    $('#logistica_city').attr('data-placeholder',$('#placeholder_city').val()).trigger('change');
                    $(cities).each(function (key,city) {
                        var newOption = new Option(city['name'],city['id'],false,false);
                        newOption.dataset.city_id = city['id'];
                        $('#logistica_city').append(newOption);

                    });
                    $('#logistica_city').prop('disabled', false).trigger('change');
                }
            });
        }

    });

	$('#button-addon1').click(function () {
		var region_id = $('#regions option:selected').data('region_id');
		var city_id = $('#city option:selected').data('city_id');
		$('#region_id').val(region_id);
        $('#city_id').val(city_id);
		$('#from_search').submit();
    });

	$('#button-books-search').click(function () {
        $('#from_search_books').submit();
    });

    $('#corner_books_type_id').select2({
        placeholder: $('#placeholder_type').val(),
        theme:'bootstrap4-filter',
        allowClear: true
    });

    $('#logistica_type_id').select2({
        placeholder: $('#placeholder_logistica_type').val(),
        theme:'bootstrap4-filter',
        allowClear: true
    });

    $('#date_birth').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd-mm-yyyy'
    });

    $('#corner_books_section_id').select2({
        placeholder: $('#placeholder_section').val(),
        theme:'bootstrap4-filter',
        allowClear: true
    });

    $('.btn-profile-del').click(function () {
        var id = $(this).data('id');
        var type = $(this).data('type');
        if (window.confirm($('#label_del_question').val()))
        {
            $.post('/profile/post-delete/'+type+'/'+id,'',function (res) {
                var r = JSON.parse(res);
                if (r.status == 'ok') {
                    console.log(r.message)
                    document.location.reload();
                } else {
                    alert(r.message);
                }

            })
        }


    });


    // $('#signinModal').modal('show');
/*    $('.dropdown-hoverable').hover(function(){
        $(this).children('[data-toggle="dropdown"]').click();
    }, function(){
        $(this).children('[data-toggle="dropdown"]').click();
    });*/

});
/* Truncate text */
$(function() {
	$(".truncate").succinct({
		size: 200
	});
});

$('input[type="range"]').rangeslider({polyfill: false});
$( window ).resize(function() {
	$(".push").css('height', $('.footer').outerHeight());
});

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
