<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    protected $table = 'cities';

    public function country()
    {
        return $this->hasOne('App\Countries','id','country_id');
    }

    public function region()
    {
        return $this->hasOne('App\Regions','id','region_id');
    }

    public function province()
    {
        return $this->hasOne('App\Province','id','province_id');
    }


}
