<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Province extends Model
{

    private $locale = 'it';

    //
    protected $table = "provinces";


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->locale = Session::exists('locale') ? Session::get('locale'):'it';
    }


    public function country()
    {
        return $this->hasOne('App\Countries', 'id', 'country_id');
    }

    public function region()
    {
        return $this->hasOne('App\Regions', 'id', 'region_id');
    }

    public function cities()
    {
        return $this->hasMany(Cities::class, 'province_id', 'id');
    }

    public function getCitiesListArray()
    {
        $citiesArray = [];

        foreach ($this->cities as $city) {
            $citiesArray[] = ['id' => $city->id, 'url' => $city->url, 'name' => $city->{'name_' . $this->locale}];
        }
        return $citiesArray;
    }
}
