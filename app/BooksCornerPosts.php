<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Thumbs;
use App\Helpers\Slug;


class BooksCornerPosts extends Model
{
    protected $table = 'books_corner_posts';

    const ON_MODERATE = 2;
    const ENABLED = 1;
    const DISABLED = 0;

    const FORM_TYPE = 'books_corner';
    const GALLERY_PATH_EXT = '/upload/books_corner/';

    public static function boot()
    {
        parent::boot();
        self::saved(function ($model) {
            if ($model->image) {
                Thumbs::makeResizePhoto(self::FORM_TYPE,$model->image);
            }
        }); 
    }


    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function type()
    {
        return $this->hasOne('App\BookCornerTypes','id','type_id');
    }

    public function section()
    {
        return $this->hasOne('App\BookCornerSections','id','section_id');
    }

    public function getStatusAsText()
    {
        $textStatus = '';
        switch ($this->status)
        {
            case self::ENABLED:
                $textStatus = __('profile.enable');
                break;
            case self::DISABLED:
                $textStatus = __('profile.disable');
                break;
            case self::ON_MODERATE:
                $textStatus = __('profile.on_moderate');
                break;
        }

        return $textStatus;
    }


    public function saveNewBooksCornerPost($data)
    {
        $this->status = self::ON_MODERATE;
        $this->type_id = $data['type_id'];
        $this->section_id = $data['section_id'];
        $this->title_en = $data['title'];
        $this->title_it = $data['title'];
        $this->text_en = $data['text'];
        $this->text_it = $data['text'];
        if (isset($data['upload_image'])) {
            $this->image = isset($data['upload_image'])?$data['upload_image']:'';
            Thumbs::makeResizePhoto(self::FORM_TYPE,$data['upload_image']);
        }

        $this->user_id = $data['user_id'];
        $this->program = $data['program'];
        $this->author = $data['author'];
        $this->curse = $data['curse'];
        $this->url = Slug::url($data['title']);
        $this->seo_title_en = $data['title'];
        $this->seo_title_it = $data['title'];
        $this->seo_description_en = mb_substr($data['text'],0,250);
        $this->seo_description_it = mb_substr($data['text'],0,250);
        $this->seo_keywords_en = mb_substr($data['title'],0,100);
        $this->seo_keywords_it = mb_substr($data['title'],0,100);
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->save();
    }

    public function getList($params)
    {
        $query = self::newQuery();

        $query->select('books_corner_posts.*');
        if (isset($params['status'])) {
            $query = $query->where('books_corner_posts.status', '=', $params['status']);
        }

        if (isset($params['type_id'])) {
            $query = $query->where('books_corner_posts.type_id', '=', $params['type_id']);
        }

        if (isset($params['section_id'])) {
            $query = $query->where('books_corner_posts.section_id', '=', $params['section_id']);
        }

        if (isset($params['section'])) {
            $query->join('book_corner_sections','books_corner_posts.section_id','=','book_corner_sections.id');
            $query = $query->where('book_corner_sections.url', '=', $params['section']);
        }


        if (isset($params['program'])) {
            $query = $query->where('books_corner_posts.program', '=', $params['program']);
        }

        if (isset($params['title'])) {
            $query = $query->where('books_corner_posts.author', 'like', '%'.$params['title'].'%')->
                orWhere('books_corner_posts.title_en','like','%'.$params['title'].'%')->
                orWhere('books_corner_posts.title_it','like','%'.$params['title'].'%')->
                orWhere('books_corner_posts.text_it','like','%'.$params['title'].'%')->
                orWhere('books_corner_posts.text_it','like','%'.$params['title'].'%');
        }

        if (isset($params['curse'])) {
            $query = $query->where('books_corner_posts.curse', 'like', '%'.$params['curse'].'%');
        }

        if ($params['order']) {
            foreach ($params['order'] as $field=>$type) {
                $query->orderBy($field,$type);
            }
        }


        $limit = isset($params['limit']) ? $params['limit'] : 0;
        $offset = isset($params['offset']) ? $params['offset'] : 0;
        if ($limit) {
            $query = $query->limit($limit);
        }
        if ($offset) {
            $query = $query->offset($offset);
        }

        return $query->paginate(3);
    }



}
