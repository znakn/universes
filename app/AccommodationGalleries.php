<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccommodationGalleries extends Model
{
    protected $table = 'accommodation_gallereies';
    //
    protected $fillable = ['entity_id','image','created_at','updated_at'];

    public function entity()
    {
        return $this->hasOne('App\LogisticaPosts','id','entity_id');
    }

    public function deleteByEntityId($entityId)
    {
        $photos = self::where(['entity_id'=>$entityId])->get();
        foreach ($photos as $photo) {
            $photo->delete();
        }
    }

}
