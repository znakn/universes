<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisciplinesTerms extends Model
{
    protected $table = 'disciplines_terms';
}
