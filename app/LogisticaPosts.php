<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Thumbs;
use App\Helpers\Slug;

class LogisticaPosts extends Model
{
    protected $table = 'logistica_posts';

    private $gallereies = [];
    private $images = [];

    protected $appends = [
        'gallereies','images'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['gallereies'];


    const ON_MODERATE = 2;
    const ENABLED = 1;
    const DISABLED = 0;

    const FORM_TYPE = 'logistica_posts';
    const GALLERY_PATH_EXT = '/upload/logistica_posts/';


    public static function boot()
    {
        parent::boot();

        self::saving(function($model){
            if (isset($model->gallereies[0])) {
                $model->image = $model->gallereies[0];
            }
        });

        self::saved(function ($model) {
            // remove old gallereis
            $accGallery = new AccommodationGalleries();
            $accGallery->deleteByEntityId($model->id);
            $images = $model->gallereies;

            if (is_array($images)) {
                $i = 0 ;
                foreach ($images as $image) {
                    $img = AccommodationGalleries::where('image', $image)->where('entity_id', $model->id)->first();
                    if (!$img) {
                        $imgn = new AccommodationGalleries();
                        $imgn->entity_id = $model->id;
                        $imgn->image = $image;
                        $imgn->save();
                        unset($imgn);
                    }
                    if ($i == 0) {
                        // update main image
                        Thumbs::makeResizePhoto(self::FORM_TYPE,$image);
                    }
                $i++;
                }
            }
        });


        self::deleting(function($model){
            // Delete all related entities with this entity
            // Delete related images
            $entityId = $model->id;

            $images = AccommodationGalleries::where(['entity_id'=>$entityId])->get();

            foreach ($images as $image) {
                $image->delete();
            }
        });
    }


    public function getGallereiesAttribute()
    {
        $this->gallereies = [];
        $images = AccommodationGalleries::where('entity_id', $this->id)->get();
        foreach ($images as $image) {
            $this->gallereies[] = $image->image;
        }
        return $this->gallereies;
    }

    public function getImagesAttribute()
    {
        $this->images = [];
        $path = public_path(self::GALLERY_PATH_EXT);
        $images = AccommodationGalleries::where('entity_id', $this->id)->get();
        //ppr($images);
        foreach ($images as $image) {
            $fileSize = file_exists($path.'/'.$image->image) ? Thumbs::getImageSize(self::FORM_TYPE,$image->image) : 0;
            $this->images[] = ['path'=>self::GALLERY_PATH_EXT,'file_name'=>$image->image,'file_size'=>$fileSize];
        }
        return $this->images;
    }



    public function author()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function type()
    {
        return $this->hasOne('App\LogisticaTypes','id','type_id');
    }

    public function region()
    {
        return $this->hasOne('App\Regions', 'id', 'region_id');
    }

    public function city()
    {
        return $this->hasOne('App\Cities', 'id', 'city_id');
    }

    public function province()
    {
        return $this->hasOne('App\Province','id','province_id');
    }

    public function getStatusAsText()
    {
        $textStatus = '';
        switch ($this->status)
        {
            case self::ENABLED:
                $textStatus = __('profile.enable');
                break;
            case self::DISABLED:
                $textStatus = __('profile.disable');
                break;
            case self::ON_MODERATE:
                $textStatus = __('profile.on_moderate');
                break;
        }

        return $textStatus;
    }


    public function saveNewPost($data)
    {
        $this->status = self::ON_MODERATE;
        $this->title_it = $data['title'];
        $this->title_en = $data['title'];
        $this->type_id = $data['logistica_type_id'];
        $this->region_id = $data['region_id'];
        $this->province_id = $data['province_id'];
        $this->city_id = $data['city_id'];
        $this->text_en = $data['text'];
        $this->text_it = $data['text'];
        $this->image = isset($data['upload_image'])?$data['upload_image']:'';
        $this->user_id = $data['user_id'];
        $this->program = $data['program'];
        $this->url = Slug::url($data['title']);
        $this->seo_title_en = $data['title'];
        $this->seo_title_it = $data['title'];
        $this->seo_description_en = mb_substr($data['text'],0,250);
        $this->seo_description_it = mb_substr($data['text'],0,250);
        $this->seo_keywords_en = mb_substr($data['title'],0,100);
        $this->seo_keywords_it = mb_substr($data['title'],0,100);
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->save();

        if (isset($data['gallereies'])&&is_array($data['gallereies'])) {

            $aG = new AccommodationGalleries();
            $aG->deleteByEntityId($this->id);
            $i = 0 ;
            foreach ($data['gallereies'] as $fileName) {
                if ($i == 0) {
                    $this->image = $fileName;
                    Thumbs::makeResizePhoto(self::FORM_TYPE,$fileName);
                    $this->save();
                }
                $accomodationGalleryImage = new AccommodationGalleries();
                $accomodationGalleryImage->entity_id = $this->id;
                $accomodationGalleryImage->image = $fileName;
                $accomodationGalleryImage->created_at = date('Y-m-d H:i:s');
                $accomodationGalleryImage->updated_at = date('Y-m-d H:i:s');
                $accomodationGalleryImage->save();
                $i++;
            }
        }


    }


    public function getList($params)
    {
        $query = self::newQuery();
        if (isset($params['status'])) {
            $query = $query->where('logistica_posts.status', '=', $params['status']);
        }

        if (isset($params['type_id'])) {
            $query = $query->where('logistica_posts.type_id', '=', $params['type_id']);
        }

        if (isset($params['region_id'])) {
            $query = $query->where('logistica_posts.region_id', '=', $params['region_id']);
        }

        if (isset($params['province_id'])) {
            $query = $query->where('logistica_posts.province_id', '=', $params['province_id']);
        }

        if (isset($params['city_id'])) {
            $query = $query->where('logistica_posts.city_id', '=', $params['city_id']);
        }

        if (isset($params['program'])) {
            $query = $query->where('logistica_posts.program', '=', $params['program']);
        }

        if (isset($params['title'])) {
            $query = $query->where('logistica_posts.title_en','like','%'.$params['title'].'%')->
            orWhere('logistica_posts.title_it','like','%'.$params['title'].'%')->
            orWhere('logistica_posts.text_it','like','%'.$params['title'].'%')->
            orWhere('logistica_posts.text_it','like','%'.$params['title'].'%');
        }

        if ($params['order']) {
            foreach ($params['order'] as $field=>$type) {
                $query->orderBy($field,$type);
            }
        }

        $limit = isset($params['limit']) ? $params['limit'] : 0;
        $offset = isset($params['offset']) ? $params['offset'] : 0;
        if ($limit) {
            $query = $query->limit($limit);
        }
        if ($offset) {
            $query = $query->offset($offset);
        }

        return $query->paginate(3);

    }

    public function setGallereiesAttribute($images)
    {
        $this->gallereies = $images;
    }
}
