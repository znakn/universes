<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookCornerTypes extends Model
{
    protected $table = 'book_corner_types';

    const ENABLED = 1;
    const DISABLED = 0;

}
