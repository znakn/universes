<?php

namespace App\Admin\Forms;


use Encore\Admin\Widgets\StepForm;
use Illuminate\Http\Request;

class Import extends StepForm
{
    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'Import Data';

    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request)
    {
        //dump($request->all());

        if ($file = $request->file('importFile')) {
            $destinationPath = public_path('import'); // upload path
            $importFileName = 'import.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $importFileName);
            $this->clear();

            return $this->next(['fileName' => $importFileName]);
        } else {
            admin_error('File not exist');
            return back();
        }


    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->file('importFile','Select import file');
    }

    /**
     * The data of the form.
     *
     * @return array $data
     */
    public function data()
    {

    }
}
