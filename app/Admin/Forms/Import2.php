<?php

namespace App\Admin\Forms;

use App\Cities;
use App\Config;
use App\EducationEntities;
use App\Helpers\Slug;
use App\Institutions;
use Encore\Admin\Widgets\StepForm;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Encore\Admin\Widgets\Table;

class Import2 extends StepForm
{

    private $reader;

    private $sheet;

    private $dataArray;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->reader = new Xlsx();
    }

    /**
     * The form title.
     *
     * @var string
     */
    public $title = 'Import step2';

    /**
     * Handle the form request.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request)
    {

        $data = $this->all();
        $fields = isset($data['import2'])?$data['import2']:'';
        if ($fields&&is_array($fields)) {
            Config::setValue('import',json_encode($fields));
        }

        $this->importData($fields);


        admin_success('Processed successfully.');

        $this->clear();

        return redirect('/admin/import?step=import');
    }

    private function importData($map)
    {
        $column = $this->getClearColumnNumber($map);
        $unknownCity = Cities::where(['name_en'=>'Unknown'])->first();

        for ($i = 1;$i<count($this->dataArray);$i++)
        {
            $row = $this->dataArray[$i];
            $program = '';
            $institution = '';
            $entityName = '';
            $entityDescription = '';
            $address = '';
            $webAddress = '';
            $country = '';
            $region = '';
            $province = '';
            $city = '';
            $email = '';
            $phone = '';
            foreach ($column as $index =>$type) {
                switch ($type) {
                    case 'program':
                        $program = $this->getProgram(isset($row[$index])?$row[$index] : '');
                        break;
                    case 'institution':
                        $institution = $this->getInstitution(isset($row[$index])?$row[$index] : '');
                        break;
                    case 'name':
                         $entityName = isset($row[$index])?$row[$index] : '';
                        break;
                    case 'description':
                         $entityDescription = isset($row[$index])?$row[$index] : '';
                        break;
                    case 'address':
                         $address = isset($row[$index])?$row[$index] : '';
                        break;
                    case 'www':
                         $webAddress =  isset($row[$index])?$row[$index] : '';
                        break;
                    case 'email':
                        $email =  isset($row[$index])?$row[$index] : '';
                        break;
                    case 'phone':
                        $phone =  isset($row[$index])?$row[$index] : '';
                        break;
                    case 'city':
                        $location = $this->getLocationObj(isset($row[$index])?$row[$index] : '');

                        if (!empty($location)) {
                            $country = isset($location['country_id'])?$location['country_id']:'';
                            $region = isset($location['region_id'])?$location['region_id']:'';
                            $province = isset($location['province_id'])?$location['province_id']:'';
                            $city = isset($location['city_id'])?$location['city_id']:'';
                        }
                        break;
                }
            }
            $entity = EducationEntities::where('name_it','like',$entityName)->orWhere('name_en','like',$entityName)->where('program','=',$program)->first();
            if (!$entity) {
                $entity = EducationEntities::where('name_it','like',$entityName.'%')->orWhere('name_en','like',$entityName.'%')->where('program','=',$program)->first();
            }

            if (!$entity) {
                $entity = new EducationEntities();
                $entity->url = Slug::url($entityName);
                $entity->name_it = $entityName;
                $entity->name_en = $entityName;
                $entity->text_it = $entityDescription;
                $entity->text_en = $entityDescription;
                $entity->status = EducationEntities::ENABLED;
            }
            $entity->email = $email;
            $entity->phone = $phone;
            $entity->description_it = $entityDescription;
            $entity->description_en = $entityDescription;
            $entity->program = $program;
            $entity->country_id = $country ? $country:1;
            $entity->region_id = $region ? $region : $unknownCity->region_id;
            $entity->province_id = $province ? $province : $unknownCity->province_id;
            $entity->city_id = $city ? $city : $unknownCity->id;
            $entity->web_address = $webAddress;
            $entity->address = $address;
            $entity->institution_id = $institution;
            $entity->status = EducationEntities::ENABLED;
            $entity->save();


        }
    }

    private function getLocationObj($city)
    {
        if (!$city) return [];
         $locationObj = [];
         $city = Cities::where('name_it','like',$city)->orWhere('name_en','like',$city)->first();
         if (!$city) {
             $city = Cities::where('name_it','like',$city.'%')->orWhere('name_en','like',$city.'%')->first();
         }
         if (!$city) {
             $city = Cities::where('name_it','like','%'.$city.'%')->orWhere('name_en','like','%'.$city.'%')->first();
         }
         if ($city) {
             $locationObj['city_id'] = $city->id;
             $locationObj['province_id'] = $city->province_id;
             $locationObj['region_id'] = $city->region_id;
             $locationObj['country_id'] = $city->country_id;
         }
         return $locationObj;
    }

    private function getProgram($value)
    {
        if (!$value) return '';
        return (mb_strpos($value,'niver')) ? 'master_university':'secondary_school' ;
    }

    private function getInstitution($value)
    {
        if (!$value) return '';
        $institutionId = '';
        // check exist institution
        $institution = Institutions::where('name_it','like','%'.$value.'%')->orWhere('name_en','like','%'.$value.'%')->first();
        if ($institution) {
            $institutionId = $institution->id;
        } else {
            $institution = new Institutions();
            $institution->name_it = $value;
            $institution->name_en = $value;
            $institution->status = Institutions::ENABLED;
            $institution->url = Slug::url($value);
            $institution->save();
            $institutionId = $institution->id;
        }
        return $institutionId;
    }

    private function getClearColumnNumber($map)
    {
        $column = [];
        foreach ($map as $key => $value)
        {
            $index = str_replace('field_','',$key);
            $column[(int)$index] = $value;
        }
        return $column;
    }

    /**
     * Build a form here.
     */
    public function form()
    {

        //ppr($settings);
        $this->prepareData();
        if ($this->dataArray) {
            $rowTitle = $this->dataArray[0];
            $i = 0;
            foreach ($rowTitle as $field) {
                //ppr(isset($settings['field_'.$i]) ? $settings['field_'.$i] : '');
                $this->select('field_'.$i,$field)->options(['program'=>'PROGRAM','institution'=>'INSTITUTION','country'=>'COUNTRY','name'=>'NAME IT/EN',
                    'description'=>'DESCRIPTION IT/EN','address'=>'ADDRESS','email'=>'EMAIL','city'=>'CITY','province'=>'PROVINCE','region'=>'REGION','phone'=>'PHONE','www'=>'WWW ADDRESS'
                    ])->value(isset($settings['field_'.$i]) ? $settings['field_'.$i] : '');
                $i++;
            }
        }
        $this->method('POST');
    }

    /**
     * The data of the form.
     *
     * @return array $data
     */
    public function data()
    {
        return (array)json_decode(Config::getValue('import'));
    }

    private function prepareData()
    {
        $params = $this->all();
        if (isset($params['import']['fileName'])) {
            $fileName = $params['import']['fileName'];

            $this->sheet = $this->reader->load(public_path('import').'/'.$fileName);
            $dataArray = $this->sheet->getActiveSheet()->toArray();
            $this->dataArray = $dataArray;
        }
    }
}
