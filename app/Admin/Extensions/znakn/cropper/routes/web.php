<?php

use ZnakN\Cropper\Http\Controllers\CropperController;

Route::post('cropper-file-upload', CropperController::class.'@file_upload');
