<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">
    <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>
    <div class="{{$viewClass['field']}}">

        @include('admin::form.error')
        @include('admin::form.help-block')
        <input class="cropper-file"  id="file_image_{{$id}}"  type="file" accept="image/*" {!! $attributes !!}/>
        <input type="hidden" name="{{$name}}" id="upload_img_{{$id}}" value="{{ old($column, $value) }}" >
    </div>
    <div class="{{$viewClass['field']}}" id="uploadImg_{{$id}}"  @if($value) style="margin-top: 10px" @else style="margin-top: 10px;display: none"  @endif  >
        <img id="upload_img_preview_{{$id}}" src="{{$preview}}" alt="uploadImage">
        <input type="button" id="button_clear_img_{{$id}}"  style="width:{{$img_config['thumb']['width']}}px;margin-top: 10px"  class="form-control btn btn-danger" value="{{__('Clear')}}" >
    </div>
</div>


<!------ Modal Crop -------------------------->
<div class="modal fade" id="modal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Laravel Crop Image Before Upload using Cropper JS - NiceSnippets.com</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="img-container">
                    <div class="row">
                        <div class="col-md-8">
                            <img id="image_{{$id}}" src="https://avatars0.githubusercontent.com/u/3456749">
                        </div>
                        <div class="col-md-4">
                            <div class="preview"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>
<!------ Modal Crop  ------------------------->
{!! $css !!}

