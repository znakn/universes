<?php

namespace ZnakN\Cropper;


use Encore\Admin\Form\Field;
use Encore\Admin\Admin;

class Crop extends Field
{
    protected $view = 'cropper::index';

    private $imageType = '';
    private $imageConfig = [];

    public function __construct(string $column = '', array $arguments = [])
    {
        parent::__construct($column, $arguments);
        $this->imageType = isset($arguments[1]['image_type'])?$arguments[1]['image_type']:'';
        $imgesConfig = config('app.img');
        $this->imageConfig = isset($imgesConfig[$this->imageType])?$imgesConfig[$this->imageType]:'';
    }

    public function preview()
    {
        if(!is_null($this->value())) {
            return '/upload/'.$this->imageType.'/thumb_'.$this->value();
        }
    }


    public function render()
    {
        $variables = $this->variables();
        $id = $variables['id'];
        $aspectRatio = $this->imageConfig['thumb']['width']/$this->imageConfig['thumb']['height'];

        $thumbWidth = $this->imageConfig['thumb']['width'];
        $thumbHeight = $this->imageConfig['thumb']['height'];

        $mainWidth = $this->imageConfig['main']['width'];
        $mainHeight = $this->imageConfig['main']['height'];

        $token = csrf_token();

        $css = "
        <style type=\"text/css\">
            img {
                display: block;
                max-width: 100%;
            }
            .preview {
                overflow: hidden;
                width: ".$thumbWidth."px;
                height: ".$thumbHeight."px;
                margin: 10px;
                border: 1px solid red;
            }
            .modal-lg{
                max-width: 1000px !important;
            }
        </style>";


        $script = <<<EOT
            
            var modal_window = $('#modal_$id');
            var image = document.getElementById('image_$id');
            var cropper;

            $("body").on("change",".cropper-file",function(e){
                var files = e.target.files;
                var done = function (url) {
                    image.src = url;
                    modal_window.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (url) {
                        done(url.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            modal_window.on('shown.bs.modal', function () {
            
                cropper = new Cropper(image, {
                    aspectRatio: '$aspectRatio',
                    viewMode: 3,
                    preview: '.preview'
                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
                $('.modal-backdrop').remove()
                $(document.body).removeClass("modal-open");
            });

            $("#crop").click(function(){
                canvas = cropper.getCroppedCanvas({
                    width: '$mainWidth',
                    height: '$mainHeight',
                });

                canvas.toBlob(function(blob) {
                    url = URL.createObjectURL(blob);
                    var reader = new FileReader();
                    reader.readAsDataURL(blob);
                    reader.onloadend = function() {
                        var base64data = reader.result;

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: "/admin/cropper-file-upload",
                            data: {'image_type':'$this->imageType','_token': '$token', 'image': base64data},
                            success: function(data){
                                $('#modal_$id').modal('hide');
                                $('#upload_img_preview_$id').attr('src',data.file);
                                $('#upload_img_$id').val(data.file_name);
                                $('#uploadImg_$id').show();
                            }
                        });
                    }
                });
            })

            $('#button_clear_img_$id').click(function () {
                $('#upload_img_$id').val('');
                $('#file_image_$id').val('');
                $('#uploadImg_$id').hide();

            });



EOT;



        if (!$this->display) {
            return '';
        }

        Admin::script($script);



        return view($this->getView(),$this->variables(),['preview'=>$this->preview(),'css'=>$css,'img_config'=>$this->imageConfig]);
    }
}
