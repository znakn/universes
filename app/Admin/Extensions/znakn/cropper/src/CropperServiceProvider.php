<?php

namespace ZnakN\Cropper;

use Illuminate\Support\ServiceProvider;
use Encore\Admin\Admin;
use Encore\Admin\Form;
use ZnakN\Cropper\Crop;

class CropperServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(Cropper $extension)
    {
        if (! Cropper::boot()) {
            return ;
        }

        if ($views = $extension->views()) {
            $this->loadViewsFrom($views, 'cropper');
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/znakn/cropper')],
                'cropper'
            );
        }

        $this->app->booted(function () {
            Cropper::routes(__DIR__.'/../routes/web.php');
        });

        Admin::booting(function () {
            Form::extend('cropper', Crop::class);
            Admin::js('vendor/znakn/cropper/cropper.js');
            Admin::css('vendor/znakn/cropper/cropper.css');
        });
    }
}
