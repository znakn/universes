<?php

namespace ZnakN\Cropper;

use Encore\Admin\Extension;

class Cropper extends Extension
{
    public $name = 'cropper';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';

    public $menu = [
        'title' => 'Cropper',
        'path'  => 'cropper',
        'icon'  => 'fa-gears',
    ];
}