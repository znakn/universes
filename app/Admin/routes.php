<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');

    $router->get('countries/all','CountriesController@all')->name('countries.all');
    $router->get('regions/all','RegionsController@all')->name('regions.all');
    $router->get('provinces/all','ProvinceController@all')->name('provinces.all');
    $router->get('cities/all','CitiesController@all')->name('cities.all');
    $router->get('sections/all','BookCornerSectionsController@all')->name('sections.all');

    $router->get('education-entities/entities','EducationEntitiesController@entities')->name('entities.entities');
    $router->resource('countries', CountriesController::class);
    $router->resource('cities', CitiesController::class);
    $router->resource('regions', RegionsController::class);
    $router->resource('programs', ProgramsController::class);
    $router->resource('institutions', InstitutionsController::class);
    $router->resource('education-entities', EducationEntitiesController::class);
    $router->resource('disciplines', DisciplinesController::class);
    $router->resource('terms', TermsController::class);
    $router->resource('gallereies', GallereisController::class);
    $router->resource('reviews', ReviewsController::class);
    $router->resource('static-pages', StaticPagesController::class);
    $router->resource('news', NewsController::class);
    $router->resource('blogs', BlogController::class);
    $router->resource('books-corner-posts', BooksCornerPostsController::class);
    $router->resource('logistica-posts', LogisticaPostsController::class);
    $router->resource('libraries-reviews', LibrariesReviewController::class);
    $router->resource('book-corner-types', BookCorneTypesController::class);
    $router->resource('logistica-types', LogisticaTypesController::class);
    $router->resource('contacts', ContactController::class);
    $router->resource('admin-news', AdminNewsController::class);
    $router->resource('book-corner-sections', BookCornerSectionsController::class);
    $router->resource('contact-with-uses', ContactWithUsController::class);
    $router->resource('users', UsersController::class);
    $router->resource('ratings', RatingsController::class);
    $router->resource('provinces', ProvinceController::class);
    $router->resource('import',ImportController::class);
    $router->resource('blocks', BlockController::class);














});
