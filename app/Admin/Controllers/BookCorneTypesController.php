<?php

namespace App\Admin\Controllers;

use App\BookCornerTypes;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BookCorneTypesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Book Corner Types';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BookCornerTypes());

        $grid->column('id', __('Id'));
        $grid->column('name_it', __('Name it'));
        $grid->column('name_en', __('Name en'));
        $grid->column('status', __('Status'))->using([BookCornerTypes::DISABLED => __('Disabled'),BookCornerTypes::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BookCornerTypes::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name_it', __('Name it'));
        $show->field('name_en', __('Name en'));
        $show->field('status', __('Status'))->using([BookCornerTypes::DISABLED => __('Disabled'),BookCornerTypes::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BookCornerTypes());

        $form->text('name_it', __('Name it'));
        $form->text('name_en', __('Name en'));
        $form->select('status', __('Status'))->options([BookCornerTypes::DISABLED => __('Disabled'),BookCornerTypes::ENABLED => __('Enabled')]);

        return $form;
    }
}
