<?php

namespace App\Admin\Controllers;

use App\Contact;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ContactController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Contact';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Contact());

        $grid->column('id', __('Id'));
        $grid->column('address', __('Address'));
        $grid->column('phone1', __('Phone1'));
        $grid->column('phone2', __('Phone2'));
        $grid->column('email1', __('Email1'));
        $grid->column('email2', __('Email2'));
        $grid->column('status', __('Status'))->using([Contact::DISABLED => __('Disabled'),Contact::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Contact::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('address', __('Address'));
        $show->field('phone1', __('Phone1'));
        $show->field('phone2', __('Phone2'));
        $show->field('email1', __('Email1'));
        $show->field('email2', __('Email2'));
        $show->field('status', __('Status'))->using([Contact::DISABLED => __('Disabled'),Contact::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Contact());

        $form->text('address', __('Address'));
        $form->text('phone1', __('Phone1'));
        $form->text('phone2', __('Phone2'));
        $form->text('email1', __('Email1'));
        $form->text('email2', __('Email2'));
        $form->select('status', __('Status'))->options([Contact::DISABLED => __('Disabled'),Contact::ENABLED => __('Enabled')]);

        return $form;
    }
}
