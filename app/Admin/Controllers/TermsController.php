<?php

namespace App\Admin\Controllers;

use App\Terms;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TermsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Terms';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Terms());

        $grid->column('id', __('Id'));
        $grid->column('name_it', __('Name it'));
        $grid->column('name_en', __('Name en'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Terms::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name_it', __('Name it'));
        $show->field('name_en', __('Name en'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Terms());

        $form->text('name_it', __('Name it'));
        $form->text('name_en', __('Name en'));

        return $form;
    }
}
