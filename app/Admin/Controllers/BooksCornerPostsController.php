<?php

namespace App\Admin\Controllers;

use App\BookCornerTypes;
use App\BooksCornerPosts;
use function count;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Programs;
use App\BookCornerSections;
use Illuminate\Support\Facades\Request;

class BooksCornerPostsController extends CustomAdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Books Corner Posts';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BooksCornerPosts());

        $grid->column('id', __('Id'));
        $grid->column('url');
        $grid->column('program',__('Program'));
        $grid->column('section.title_en',__('Section'));
        $grid->column('image', __('Image'))->display(function ($image) {

        return '/upload/'.BooksCornerPosts::FORM_TYPE.'/thumb_'.$image;

    })->image(env('APP_URL'));



        $grid->column('title_it', __('Title it'));
        $grid->column('title_en', __('Title en'));
        $grid->column('user.name', __('User'));
        $grid->column('author', __('Author'));
        $grid->column('vote_count', __('Vote count'));
        $grid->column('status', __('Status'))->using([BooksCornerPosts::DISABLED => __('Disabled'),BooksCornerPosts::ON_MODERATE=>__('Moderated'),BooksCornerPosts::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->equal('status',__('Status'))->select([BooksCornerPosts::DISABLED => __('Disabled'),BooksCornerPosts::ON_MODERATE=>__('Moderated'),BooksCornerPosts::ENABLED => __('Enabled')]);
        });
        $grid->model()->orderBy('status','DESC');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BooksCornerPosts::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('user_id', __('User'))->as(function ($user_id){
            return User::where(['id'=>$user_id])->first()->name;
        });
        $show->field('program', __('Program'))->as(function ($program){
            return Programs::where(['url'=>$program])->first()->title_en;
        });

        $show->field('section_id', __('Section'))->as(function ($section_id){
            return BookCornerSections::where(['id'=>$section_id])->first()->title_en;
        });

        $show->field('author',__('Author'));
        $show->field('curse',__('Curse'));
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('image', __('Image'))->as(function ($image) {

            return '/upload/'.BooksCornerPosts::FORM_TYPE.'/thumb_'.$image;

        })->image(env('APP_URL'));
        $show->field('text_it', __('Text it'));
        $show->field('text_en', __('Text en'));
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_description_it', __('Seo description it'));
        $show->field('seo_description_en', __('Seo description en'));
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('email', __('Contact Email'));
        $show->field('phone', __('Contact Phone'));
        $show->field('vote_count', __('Vote count'));
        $show->field('status', __('Status'))->using([BooksCornerPosts::DISABLED => __('Disabled'),BooksCornerPosts::ON_MODERATE=>__('Moderated'),BooksCornerPosts::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('program', __('Program'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $form = new Form(new BooksCornerPosts());

        $post = $id ? BooksCornerPosts::find($id): null;

        $form->text('url', __('Url'));

        $form->select('user_id', __('User'))->options(User::all()->pluck('name','id'))->default(Auth::user()->id);

        $form->select('type_id', __('Type'))->options(BookCornerTypes::all()->pluck('name_en','id'));


        $form->select('program',__('Program'))->options(Programs::all()->pluck('title_en','url'))
            ->load('section_id','/admin/sections/all');

        if ($form->isEditing()&&$post) {
            $form->select('section_id',__('Section'))
                ->options(BookCornerSections::where('program',$post->program)->pluck('title_en','id'));
        } else {
            $form->select('section_id',__('Section'));
        }

        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));
        $form->text('author', __('Author'));
        $form->text('curse', __('Curse/Matherie'));
        $form->image('image', __('Image'))->disk(BooksCornerPosts::FORM_TYPE)->removable()->uniqueName();
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->text('email', __('Contact email'));
        $form->text('phone', __('Contact phone'));
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->ckeditor('seo_description_it', __('Seo description it'));
        $form->ckeditor('seo_description_en', __('Seo description en'));
        $form->ckeditor('seo_keywords_it', __('Seo keywords it'));
        $form->ckeditor('seo_keywords_en', __('Seo keywords en'));
        $form->number('vote_count', __('Vote count'));
        $form->select('status', __('Status'))->options([BooksCornerPosts::DISABLED => __('Disabled'),BooksCornerPosts::ON_MODERATE=>__('Moderated') ,BooksCornerPosts::ENABLED => __('Enabled')]);


        return $form;
    }
}
