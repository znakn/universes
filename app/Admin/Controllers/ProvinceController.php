<?php

namespace App\Admin\Controllers;

use App\Province;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Countries;
use App\Regions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProvinceController extends CustomAdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Province';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Province());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('country.name_en', __('Country'));
        $grid->column('region.name_en', __('Region'));
        $grid->column('name_it', __('Name it'));
        $grid->column('name_en', __('Name en'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Province::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('country_id', __('Country'))->as(function ($country_id){
            $country = Countries::find($country_id);
            if ($country) {
                return $country->name_en;
            } else {
                return 'Not select';
            }

        });
        $show->field('region_id', __('Region'))->as(function ($region_id){
            $region = Regions::find($region_id);
            if ($region) {
                return $region->name_en;
            } else {
                return 'Not select';
            }

        });
        $show->field('name_it', __('Name it'));
        $show->field('name_en', __('Name en'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id)
    {
        $form = new Form(new Province());

        $province = $id ? Province::find($id): null;

        $form->url('url', __('Url'));
        $form->select('country_id', __('Country'))->options(function($id){
            $country = Countries::find($id);
            if ($country) {
                return [$country->id => $country->name_en];
            }

        })->ajax('/admin/countries/all')->load('region_id','/admin/regions/all');
        if ($form->isEditing()) {
            if ($province) {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',$province->country_id)->pluck('name_en','id'));
            } else {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',$form->country_id)->pluck('name_en','id'));
            }

        } else {
            $form->select('region_id',__('Region'));
        }
        $form->text('name_it', __('Name it'));
        $form->text('name_en', __('Name en'));

        return $form;
    }

    public function all(Request $request)
    {
        $regionId = $request->get('q');
        return Province::where('region_id', $regionId)->get(['id', DB::raw('name_en as text')]);
    }
}
