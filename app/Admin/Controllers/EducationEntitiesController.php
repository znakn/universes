<?php

namespace App\Admin\Controllers;

use App\Cities;
use App\Countries;
use App\EducationEntities;
use App\Institutions;
use App\Programs;
use App\Regions;
use Encore\Admin\Controllers\AdminController;
use App\Admin\Controllers\CustomAdminController;
use Encore\Admin\Form;
use App\Admin\Components\CustomGrid as Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use App\Province;


class EducationEntitiesController extends CustomAdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Education Entities';

    public function index(Content $content)
    {
        $params = [];

        $institutionId = \request()->get('institution_id');

        if ($institutionId) {
            $params['institutionId'] = $institutionId;
            $institution = Institutions::find($institutionId);
            if ($institution) {
                $this->title = $institution->name_en;
            }
        }

        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list'))
            ->body($this->grid($params));
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($params = [])
    {
        $grid = new Grid(new EducationEntities());
        if (isset($params['institutionId'])) {
            $grid->setAddUrl('institution_id='.$params['institutionId']);
        }

        $grid->column('id', __('Id'));
        $grid->column('logo', __('Logo'))->image();
        $grid->column('name_en', __('Name en'));
        //$grid->column('web_address', __('Web address'));
        //$grid->column('program.title_en', __('Program'));
        $grid->column('institution.name_en', __('Institution'));
        $grid->column('country.name_en', __('Country'));
        $grid->column('region.name_en', __('Region'));
        $grid->column('province.name_en', __('Province'));
        $grid->column('city.name_en', __('City'));
        //$grid->column('email', __('Email'));
        //$grid->column('phone', __('Phone'));
        $grid->column('status', __('Status'))->using([EducationEntities::DISABLED => __('Disabled'),EducationEntities::ENABLED => __('Enabled')]);
        //$grid->column('created_at', __('Created at'));
        //$grid->column('updated_at', __('Updated at'));


        $grid->filter(function($filter){


            $unknownCity = Cities::where(['name_en'=>'Unknown'])->first();
            // Add a column filter
            $filter->like('name_en',__('Name'));
            $filter->equal('institution_id',__('Institution'))->select(Institutions::all()->pluck('name_en','id'));
            $filter->equal('city_id',__('Show Unknown location'))->select([$unknownCity->id => $unknownCity->name_en]);

        });


        $grid->perPages([10, 20, 30, 40, 50, 100, 500, 1000]);




        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(EducationEntities::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('logo', __('Logo'))->image();
        $show->field('name_it', __('Name it'));
        $show->field('name_en', __('Name en'));
        $show->field('decription_it', __('Decription it'))->unescape();
        $show->field('description_en', __('Description en'))->unescape();
        $show->field('program_title_it', __('Education program title it'));
        $show->field('program_title_en', __('Education program title en'));
        $show->field('program_text_it', __('Education program text it'))->unescape();
        $show->field('program_text_en', __('Education program text en'))->unescape();
        $show->field('web_address', __('Web address'))->link();
        $show->field('facebook_address', __('Facebook address'))->link();
        $show->field('twitter_address', __('Twitter address'))->link();
        $show->field('instagram_address', __('Instagram address'))->link();
        $show->field('linkedin_address', __('Linkedin address'))->link();
        $show->field('program', __('Program'))->as(function ($program){
            $programObj = Programs::where(['url'=>$program])->first();
            if ($programObj) {
                return $programObj->title_en;
            } else {
                return __('Not select');
            }
        });
        $show->field('institution_id', __('Institution'))->as(function ($institutionId){
            $institution = Institutions::find($institutionId);
            if ($institution) {
                return $institution->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('country_id', __('Country'))->as(function ($country_id){
            $country = Countries::find($country_id);
            if ($country) {
                return $country->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('region_id', __('Region'))->as(function ($regionId){
            $region = Regions::find($regionId);
            if ($region) {
                return $region->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('province_id', __('Province'))->as(function ($provinceId){
            $province = Province::find($provinceId);
            if ($province) {
                return $province->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('city_id', __('City'))->as(function ($cityId){
            $city = Cities::find($cityId);
            if ($city) {
                return $city->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('email', __('Email'));
        $show->field('phone', __('Phone'));
        $show->field('address', __('Address'));

        $show->field('text_it', __('Text it'))->unescape();
        $show->field('text_en', __('Text en'))->unescape();
        $show->field('image', __('Image'))->image();
        $show->field('gallereies',__('Galleries'))->image();
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_description_it', __('Seo decription it'))->unescape();
        $show->field('seo_description_en', __('Seo description en'))->unescape();
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('status', __('Status'))->using([EducationEntities::DISABLED => __('Disabled'),EducationEntities::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $entity = $id ? EducationEntities::find($id) : null;

        $form = new Form(new EducationEntities());
        $form->display('id',__('ID'));
        $form->text('url', __('Url'));
        $form->image('logo', __('Logo'))->removable();
        $form->url('web_address', __('Web address'));
        $form->url('facebook_address', __('Facebook address'));
        $form->url('twitter_address', __('Twitter address'));
        $form->url('instagram_address', __('Instagram address'));
        $form->url('linkedin_address', __('Linkedin address'));
        $form->select('program',__('Program'))->options(Programs::all()->pluck('title_en','url'));

        if ($form->isCreating()) {
            $institutionId = \request()->get('institution_id');
            if ($institutionId) {
                $form->select('institution_id', __('Institution'))->options(Institutions::all()->pluck('name_en','id'))->default($institutionId);
            } else {
                $form->select('institution_id', __('Institution'))->options(Institutions::all()->pluck('name_en','id'));
            }
        } else {
            $form->select('institution_id', __('Institution'))->options(Institutions::all()->pluck('name_en','id'));
        }

        $form->select('country_id', __('Country'))->options(Countries::all()->pluck('name_en','id'))
            ->load('region_id','/admin/regions/all');


        if ($form->isEditing()) {
            if ($entity) {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',$entity->country_id)->pluck('name_en','id'))
                    ->load('province_id','/admin/provinces/all');
            } else {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',$form->country_id)->pluck('name_en','id'))
                    ->load('province_id','/admin/provinces/all');
            }

        } else {
            $form->select('region_id', __('Region'))->loads(['province_id'],['/admin/provinces/all']);
        }

        if ($form->isEditing()) {
            if ($entity) {
                $form->select('province_id',__('Province'))
                    ->options(Province::where('country_id',$entity->country_id)->where('region_id',$entity->region_id)
                        ->pluck('name_en','id'))->load('city_id','/admin/cities/all');
            } else {
                $form->select('province_id',__('Province'))
                    ->options(Province::where('country_id',$form->country_id)->where('region_id',$form->region_id)
                        ->pluck('name_en','id'))->load('city_id','/admin/cities/all');
            }

        } else {
            $form->select('province_id',__('Province'))->load('city_id','/admin/cities/all');
        }

        if ($form->isEditing()) {
            if ($entity) {
                $form->select('city_id',__('City'))
                    ->options(Cities::where('country_id',$entity->country_id)->where('region_id',$entity->region_id)->where('province_id',$entity->province_id)->pluck('name_en','id'));
            } else {
                $form->select('city_id',__('City'))
                    ->options(Cities::where('country_id',$form->country_id)->where('region_id',$form->region_id)->where('province_id',$form->province_id)->pluck('name_en','id'));
            }
        } else {
            $form->select('city_id', __('City'));
        }

        $form->email('email', __('Email'));
        $form->mobile('phone', __('Phone'));
        $form->text('address', __('Address'));
        $form->text('name_it', __('Name it'));
        $form->text('name_en', __('Name en'));
        $form->ckeditor('description_it', __('Description it'));
        $form->ckeditor('description_en', __('Description en'));
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->text('program_title_it', __('Education program title it'));
        $form->text('program_title_en', __('Education program title en'));
        $form->ckeditor('program_text_it', __('Education program text it'));
        $form->ckeditor('program_text_en', __('Education program text en'));
        $form->image('image', __('Image'))->crop(400,400)->
        thumbnail([
            'icon' => [50, 50],
            'main' => [400, 400]
        ])->removable();
        $form->multipleImage('gallereies', __('Gallereies'))->removable();
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->ckeditor('seo_description_it', __('Seo description it'));
        $form->ckeditor('seo_description_en', __('Seo description en'));
        $form->textarea('seo_keywords_it', __('Seo keywords it'));
        $form->textarea('seo_keywords_en', __('Seo keywords en'));
        $form->select('status', __('Status'))->options([EducationEntities::DISABLED => __('Disabled'),EducationEntities::ENABLED => __('Enabled')]);



        // callback before save
        $form->saving(function (Form $form) {
            if (!$form->seo_title_en) {
                $form->seo_title_en = $form->name_en;
            }
            if (!$form->seo_title_it) {
                $form->seo_title_it = $form->name_it;
            }
            if (!$form->seo_description_en) {
                $form->seo_description_en = $form->description_en;
            }
            if (!$form->seo_description_it) {
                $form->seo_description_it = $form->description_it;
            }

        });








        return $form;
    }

    public function entities(Request $request)
    {
        $q = $request->get('q');
        return EducationEntities::where('name_en', 'like', "%$q%")->paginate(null, ['id', 'name_en as text']);
    }

}
