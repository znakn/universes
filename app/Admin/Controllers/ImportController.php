<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\MultipleSteps;
use App\Admin\Forms\Import;
use App\Admin\Forms\Import2;

class ImportController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Import data')
            ->body(MultipleSteps::make([
                'import'   => Import::class,
                'import2'  => Import2::class,
                // 'password' => Steps\Password::class,
            ]));
    }
}