<?php

namespace App\Admin\Controllers;

use App\Countries;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class CountriesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Countries';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Countries());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('short_code', __('Short code'));
        $grid->column('name_it', __('Name It'));
        $grid->column('name_en', __('Name En'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Countries::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('short_code', __('Short code'));
        $show->field('name_it', __('Name It'));
        $show->field('name_en', __('Name En'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Countries());

        $form->text('url', __('Url'));
        $form->text('short_code', __('Short code'));
        $form->text('name_it', __('Name It'));
        $form->text('name_en', __('Name En'));

        return $form;
    }

    public function all(Request $request)
    {
        $q = $request->get('q');
        return Countries::where('name_en', 'like', "%$q%")->paginate(null, ['id', 'name_en as text']);
    }


}
