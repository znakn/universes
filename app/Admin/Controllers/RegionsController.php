<?php

namespace App\Admin\Controllers;

use App\Regions;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Countries;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegionsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Regions';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Regions());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('country.name_en', __('Country'));
        $grid->column('name_it', __('Name It'));
        $grid->column('name_en', __('Name En'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Regions::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('country_id', __('Country'))->as(function ($country_id){
            $country = Countries::find($country_id);
            if ($country) {
                return $country->name_en;
            } else {
                return 'Not select';
            }

        });
        $show->field('name_it', __('Name It'));
        $show->field('name_en', __('Name En'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Regions());

        $form->text('url', __('Url'));
        $form->select('country_id', __('Country'))->options(function($id){
            $country = Countries::find($id);
            if ($country) {
                return [$country->id => $country->name_en];
            }

        })->ajax('/admin/countries/all');
        $form->text('name_it', __('Name It'));
        $form->text('name_en', __('Name En'));

        return $form;
    }

    public function all(Request $request)
    {
        $countryId = $request->get('q');
        return Regions::where('country_id', $countryId)->get(['id', DB::raw('name_en as text')]);
    }
}
