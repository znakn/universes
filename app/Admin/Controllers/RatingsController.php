<?php

namespace App\Admin\Controllers;

use App\Rating;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RatingsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Rating';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Rating());

        $grid->column('id', __('Id'));
        $grid->column('entity.name_it', __('Entity'));
        $grid->column('user.name', __('Firt Name'));
        $grid->column('user.last_name', __('Last Name'));
        $grid->column('structura', __('Structura'));
        $grid->column('social_activities', __('Social activities'));
        $grid->column('courses', __('Courses'));
        $grid->column('accommodation', __('Accommodation'));
        $grid->column('teachers', __('Teachers'));
        $grid->column('avg', __('Avg'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableEdit();
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Rating::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('entity_id', __('Entity id'));
        $show->field('user_id', __('User id'));
        $show->field('structura', __('Structura'));
        $show->field('social_activities', __('Social activities'));
        $show->field('courses', __('Courses'));
        $show->field('accommodation', __('Accommodation'));
        $show->field('teachers', __('Teachers'));
        $show->field('avg', __('Avg'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Rating());

        $form->number('entity_id', __('Entity id'));
        $form->number('user_id', __('User id'));
        $form->decimal('structura', __('Structura'));
        $form->decimal('social_activities', __('Social activities'));
        $form->decimal('courses', __('Courses'));
        $form->decimal('accommodation', __('Accommodation'));
        $form->decimal('teachers', __('Teachers'));
        $form->decimal('avg', __('Avg'));

        return $form;
    }
}
