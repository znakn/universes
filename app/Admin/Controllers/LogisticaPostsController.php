<?php

namespace App\Admin\Controllers;

use App\LogisticaPosts;
use App\LogisticaTypes;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Programs;
use App\Province;
use App\Regions;
use App\Cities;


class LogisticaPostsController extends CustomAdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Logistica Posts';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LogisticaPosts());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('author.name', __('Author'));
        $grid->column('title_it', __('Title it'));
        //$grid->column('title_en', __('Title en'));
        $grid->column('region.name_en', __('Region'));
        $grid->column('province.name_en', __('Province'));
        $grid->column('city.name_en', __('City'));

        $grid->column('image', __('Image'))->display(function ($image) {

            return '/upload/'.LogisticaPosts::FORM_TYPE.'/thumb_'.$image;

        })->image(env('APP_URL'));
        $grid->column('program', __('Program'));
        //$grid->column('vote_count', __('Vote count'));
        $grid->column('status', __('Status'))->using([LogisticaPosts::DISABLED => __('Disabled'),LogisticaPosts::ON_MODERATE=>__('Moderated'),LogisticaPosts::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->equal('status',__('Status'))->select([LogisticaPosts::DISABLED => __('Disabled'),LogisticaPosts::ON_MODERATE=>__('Moderated'),LogisticaPosts::ENABLED => __('Enabled')]);
        });
        $grid->model()->orderBy('status','DESC')->orderBy('created_at','DESC');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LogisticaPosts::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('user_id', __('User'))->as(function ($user_id){
            return User::where(['id'=>$user_id])->first()->name;
        });
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('image', __('Image'))->as(function ($image) {

            return '/upload/'.LogisticaPosts::FORM_TYPE.'/thumb_'.$image;

        })->image(env('APP_URL'));

        $show->field('type_id', __('Logistica Type'))->as(function ($type_id){
            return LogisticaTypes::where(['id'=>$type_id])->first()->name_en;
        });
        $show->field('program', __('Program'))->as(function ($program){
            return Programs::where(['url'=>$program])->first()->title_en;
        });

        $show->field('region_id', __('Region'))->as(function ($regionId){
            $region = Regions::find($regionId);
            if ($region) {
                return $region->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('province_id', __('Province'))->as(function ($provinceId){
            $province = Province::find($provinceId);
            if ($province) {
                return $province->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('city_id', __('City'))->as(function ($cityId){
            $city = Cities::find($cityId);
            if ($city) {
                return $city->name_en;
            } else {
                return 'Not select';
            }
        });
        $show->field('text_it', __('Text it'));
        $show->field('text_en', __('Text en'));
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_description_it', __('Seo description it'));
        $show->field('seo_description_en', __('Seo description en'));
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('email', __('Contact Email'));
        $show->field('phone', __('Contact Phone'));
        $show->field('vote_count', __('Vote count'));
        $show->field('status', __('Status'))->using([LogisticaPosts::DISABLED => __('Disabled'),LogisticaPosts::ON_MODERATE=>__('Moderated'),LogisticaPosts::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('program', __('Program'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = '')
    {

        $post = $id ? LogisticaPosts::find($id) : null;


        $form = new Form(new LogisticaPosts());

        $form->text('url', __('Url'));
        $form->select('user_id', __('Author'))->options(User::all()->pluck('name','id'))->default(Auth::user()->id);
        $form->select('program',__('Program'))->options(Programs::all()->pluck('title_en','url'));
        $form->select('type_id',__('Type'))->options(LogisticaTypes::all()->pluck('name_en','id'));
        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));

        if ($form->isEditing()) {
            if ($post) {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',1)->pluck('name_en','id'))
                    ->load('province_id','/admin/provinces/all');
            } else {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',1)->pluck('name_en','id'))
                    ->load('province_id','/admin/provinces/all');
            }

        } else {
            $form->select('region_id', __('Region'))->loads(['city_id'],['/admin/cities/all']);
        }

        if ($form->isEditing()) {
            if ($post) {
                $form->select('province_id',__('Province'))
                    ->options(Province::where('country_id',1)->where('region_id',$post->region_id)
                        ->pluck('name_en','id'))->load('city_id','/admin/cities/all');
            } else {
                $form->select('province_id',__('Province'))
                    ->options(Province::where('country_id',1)->where('region_id',$form->region_id)
                        ->pluck('name_en','id'))->load('city_id','/admin/cities/all');
            }

        } else {
            $form->select('province_id',__('Province'));
        }

        if ($form->isEditing()) {
            if ($post) {
                $form->select('city_id',__('City'))
                    ->options(Cities::where('country_id',1)->where('region_id',$post->region_id)->where('province_id',$post->province_id)->pluck('name_en','id'));
            } else {
                $form->select('city_id',__('City'))
                    ->options(Cities::where('country_id',1)->where('region_id',$form->region_id)->where('province_id',$form->province_id)->pluck('name_en','id'));
            }
        } else {
            $form->select('city_id', __('City'));
        }






        $form->multipleImage('gallereies', __('Gallereies'))->removable()->disk(LogisticaPosts::FORM_TYPE)->uniqueName();
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->text('email', __('Contact email'));
        $form->text('phone', __('Contact phone'));
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->ckeditor('seo_description_it', __('Seo description it'));
        $form->ckeditor('seo_description_en', __('Seo description en'));
        $form->ckeditor('seo_keywords_it', __('Seo keywords it'));
        $form->ckeditor('seo_keywords_en', __('Seo keywords en'));
        $form->number('vote_count', __('Vote count'));
        $form->select('status', __('Status'))->options([LogisticaPosts::DISABLED => __('Disabled'),LogisticaPosts::ON_MODERATE=>__('Moderated') ,LogisticaPosts::ENABLED => __('Enabled')]);

        return $form;
    }
}
