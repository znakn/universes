<?php

namespace App\Admin\Controllers;

use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Hash;

class UsersController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Users';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('email', __('Email'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->column('last_name', __('Last name'));
        $grid->column('country', __('Country'));
        $grid->column('city', __('City'));
        $grid->column('date_birth', __('Date birth'));
        $grid->column('is_agree_terms', __('Is agree terms'))->using(['1'=>__('Yes'),'0'=>__('No')]);
        $grid->column('prefer_language', __('Prefer language'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('password', __('Password'));
        $show->field('remember_token', __('Remember token'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('last_name', __('Last name'));
        $show->field('country', __('Country'));
        $show->field('city', __('City'));
        $show->field('date_birth', __('Date birth'));
        $show->field('is_agree_terms', __('Is agree terms'))->using(['1'=>__('Yes'),'0'=>__('No')]);
        $show->field('prefer_language', __('Prefer language'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User());

        $form->text('name', __('Name'));
        $form->text('last_name', __('Last name'));
        $form->password('password', __('Password'));
        $form->text('country', __('Country'));
        $form->text('city', __('City'));
        $form->date('date_birth', __('Date birth'))->format('YYYY-MM-DD');
        $form->select('is_agree_terms', __('Is agree terms'))->options(['1'=>__('Yes'),'0'=>__('No')]);
        $form->select('prefer_language', __('Prefer language'))->options(['it'=>'IT','en'=>'EN']);


        $form->saving(function (Form $form) {
            if (!$form->password) {
                    $form->password = $form->model()->password;
            } else {
                $form->password = Hash::make($form->password);
            }
            return $form;
        });


        return $form;
    }
}
