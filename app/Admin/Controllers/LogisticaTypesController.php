<?php

namespace App\Admin\Controllers;

use App\LogisticaTypes;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class LogisticaTypesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Logistica Types';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LogisticaTypes());

        $grid->column('id', __('Id'));
        $grid->column('name_it', __('Name it'));
        $grid->column('name_en', __('Name en'));
        $grid->column('status', __('Status'))->using([LogisticaTypes::DISABLED => __('Disabled'),LogisticaTypes::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LogisticaTypes::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name_it', __('Name it'));
        $show->field('name_en', __('Name en'));
        $show->field('status', __('Status'))->using([LogisticaTypes::DISABLED => __('Disabled'),LogisticaTypes::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LogisticaTypes());

        $form->text('name_it', __('Name it'));
        $form->text('name_en', __('Name en'));
        $form->select('status', __('Status'))->options([LogisticaTypes::DISABLED => __('Disabled'),LogisticaTypes::ENABLED => __('Enabled')]);
        return $form;
    }
}
