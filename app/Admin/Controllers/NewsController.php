<?php

namespace App\Admin\Controllers;

use App\News;
use App\Programs;
use App\User;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Auth;

class NewsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'News';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new News());

        $grid->column('id', __('Id'));
        $grid->column('url');
        $grid->column('image', __('Image'))->display(function ($image) {

            return '/upload/'.News::FORM_TYPE.'/thumb_'.$image;

        })->image(env('APP_URL'));
        $grid->column('title_it', __('Title it'));
        $grid->column('title_en', __('Title en'));
        $grid->column('author.name', __('Author'));


        $grid->column('vote_count', __('Vote count'));
        $grid->column('status', __('Status'))->using([News::DISABLED => __('Disabled'),News::ON_MODERATE=>__('Moderated'),News::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->equal('status',__('Status'))->select([News::DISABLED => __('Disabled'),News::ON_MODERATE=>__('Moderated'),News::ENABLED => __('Enabled')]);
        });

        $grid->model()->orderBy('status','DESC');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(News::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('image', __('Image'))->as(function ($image) {

            return '/upload/'.News::FORM_TYPE.'/thumb_'.$image;

        })->image(env('APP_URL'));
        //$show->field('video',__('Video'));
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('author', __('Author'));
        $show->field('text_it', __('Text it'));
        $show->field('text_en', __('Text en'));
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_decription_it', __('Seo decription it'));
        $show->field('seo_description_en', __('Seo description en'));
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('vote_count', __('Vote count'));
        $show->field('status', __('Status'))->using([News::DISABLED => __('Disabled'),News::ON_MODERATE=>__('Moderated'),News::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new News());
        $form->text('url', __('Url'));
        $form->select('user_id', __('Author'))->options(User::all()->pluck('name','id'))->default(Auth::user()->id);
        $form->select('program',__('Program'))->options(Programs::all()->pluck('title_en','url'));
        $form->image('image', __('Image'))->disk(News::FORM_TYPE)->removable()->uniqueName();

        //$form->text('video', __('Video'));
        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->ckeditor('seo_description_it', __('Seo description it'));
        $form->ckeditor('seo_description_en', __('Seo description en'));
        $form->ckeditor('seo_keywords_it', __('Seo keywords it'));
        $form->ckeditor('seo_keywords_en', __('Seo keywords en'));
        $form->number('vote_count', __('Vote count'))->disable();
        $form->select('status', __('Status'))->options([News::DISABLED => __('Disabled'),News::ON_MODERATE=>__('Moderated') ,News::ENABLED => __('Enabled')]);


        return $form;
    }
}
