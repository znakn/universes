<?php

namespace App\Admin\Controllers;

use App\EducationEntities;
use App\Gallereies;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class GallereisController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Gallereies';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Gallereies());
        $grid->column('entity.name_en', __('Entity'));
        $grid->column('image')->image();


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Gallereies::findOrFail($id));
        $show->field('entity_id',__('Entity'))->as(function ($entity_id){
            return EducationEntities::find($entity_id)->name_en;
        });
        $show->field('image',__('Image'))->image();
        $show->field('text_it',__('Text It'));
        $show->field('text_en',__('Text En'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Gallereies());
        $form->select('entity_id',__('Entity'))->options(function ($id) {
            $entity = EducationEntities::find($id);

            if ($entity) {
                return [$entity->id => $entity->name_en];
            }
        })->ajax('/admin/education-entities/entities');
        $form->image('image',__('Image'));
        $form->text('text_it',__('Text It'));
        $form->text('text_en',__('Text En'));

        return $form;
    }
}
