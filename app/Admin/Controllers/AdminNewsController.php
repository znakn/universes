<?php

namespace App\Admin\Controllers;

use App\AdminNews;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\User;
use App\Programs;
use Illuminate\Support\Facades\Auth;


class AdminNewsController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Admin News';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AdminNews());

        $grid->column('id', __('Id'));
        $grid->column('url');
        $grid->column('image', __('Image'))->display(function () {
            if ($this->image) {
                return '/storage/'.$this->image;
            } else {
               if ($this->video){
                   $youtubeId = AdminNews::getYoutubeIdFromUrl($this->video);
                   if ($youtubeId) {
                       return 'https://img.youtube.com/vi/'.$youtubeId.'/0.jpg';
                   }
               }
            }
        })->image(env('APP_URL'));

        $grid->column('title_it', __('Title it'));
        $grid->column('title_en', __('Title en'));
        $grid->column('author.name', __('Author'));


        $grid->column('vote_count', __('Vote count'));
        $grid->column('status', __('Status'))->using([AdminNews::DISABLED => __('Disabled'),AdminNews::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AdminNews::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('image', __('Image'));
        $show->field('video',__('Video'));
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('author', __('Author'));
        $show->field('text_it', __('Text it'));
        $show->field('text_en', __('Text en'));
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_decription_it', __('Seo decription it'));
        $show->field('seo_description_en', __('Seo description en'));
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('vote_count', __('Vote count'));
        $show->field('status', __('Status'))->using([AdminNews::DISABLED => __('Disabled'),AdminNews::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));


        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AdminNews());

        $form->text('url', __('Url'));
        $form->select('user_id', __('Author'))->options(User::all()->pluck('name','id'))->default(Auth::user()->id);
        $form->select('program',__('Program'))->options(Programs::all()->pluck('title_en','url'));
        $form->image('image', __('Image'))->crop(540,360)->
        thumbnail([
            'icon' => [540, 360],
            'main' => [540, 360]
        ]);
        $form->text('video', __('Video'));
        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->ckeditor('seo_description_it', __('Seo description it'));
        $form->ckeditor('seo_description_en', __('Seo description en'));
        $form->ckeditor('seo_keywords_it', __('Seo keywords it'));
        $form->ckeditor('seo_keywords_en', __('Seo keywords en'));
        $form->number('vote_count', __('Vote count'))->disable();
        $form->select('status', __('Status'))->options([AdminNews::DISABLED => __('Disabled'),AdminNews::ENABLED => __('Enabled')]);

        return $form;
    }


}
