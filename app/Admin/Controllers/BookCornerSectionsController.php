<?php

namespace App\Admin\Controllers;

use App\BookCornerSections;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Programs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookCornerSectionsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Book Corner Sections';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new BookCornerSections());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('program', __('Program'));
        $grid->column('title_it', __('Title it'));
        $grid->column('title_en', __('Title en'));
        //$grid->column('text_it', __('Text it'));
        //$grid->column('text_en', __('Text en'));
        //$grid->column('seo_title_it', __('Seo title it'));
        //$grid->column('seo_title_en', __('Seo title en'));
        //$grid->column('seo_description_it', __('Seo description it'));
        //$grid->column('seo_description_en', __('Seo description en'));
        //$grid->column('seo_keywords_it', __('Seo keywords it'));
        //$grid->column('seo_keywords_en', __('Seo keywords en'));
        $grid->column('status', __('Status'))->using([BookCornerSections::DISABLED => __('Disabled'),BookCornerSections::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(BookCornerSections::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('program', __('Program'));
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('text_it', __('Text it'));
        $show->field('text_en', __('Text en'));
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_description_it', __('Seo description it'));
        $show->field('seo_description_en', __('Seo description en'));
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('status', __('Status'))->using([BookCornerSections::DISABLED => __('Disabled'),BookCornerSections::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new BookCornerSections());

        $form->text('url', __('Url'));
        $form->select('program',__('Program'))->options(Programs::all()->pluck('title_en','url'));
        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));
        $form->textarea('text_it', __('Text it'));
        $form->textarea('text_en', __('Text en'));
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->textarea('seo_description_it', __('Seo description it'));
        $form->textarea('seo_description_en', __('Seo description en'));
        $form->textarea('seo_keywords_it', __('Seo keywords it'));
        $form->textarea('seo_keywords_en', __('Seo keywords en'));
        $form->select('status', __('Status'))->options([BookCornerSections::DISABLED => __('Disabled'),BookCornerSections::ENABLED => __('Enabled')]);

        return $form;
    }

    public function all(Request $request)
    {
        $program = $request->get('q');
        return BookCornerSections::where('program', $program)->get(['id', DB::raw('title_en as text')]);
    }
}
