<?php

namespace App\Admin\Controllers;

use App\Programs;
use Encore\Admin\Controllers\AdminController;
use App\Admin\Controllers\CustomAdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProgramsController extends CustomAdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Programs';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Programs());

        $grid->column('id', __('Id'));
        $grid->column('title_it', __('Title it'));
        $grid->column('title_en', __('Title en'));
        $grid->column('image', __('Image'))->image();
        $grid->column('status', __('Status'))->using([Programs::DISABLED => __('Disabled'),Programs::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        // disable create button
        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Programs::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('description_it', __('Description it'));
        $show->field('description_en', __('Description en'));
        $show->field('text_it', __('Text it'));
        $show->field('text_en', __('Text en'));
        $show->field('image', __('Image'))->image();
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_description_it', __('Seo description it'));
        $show->field('seo_description_en', __('Seo description en'));
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('status', __('Status'))->using([Programs::DISABLED => __('Disabled'),Programs::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $form = new Form(new Programs());

        $form->select('url', __('Url'))->options([Programs::SECONDARY_SCHOOL => Programs::SECONDARY_SCHOOL,Programs::MASTER_UNIVERSITY => Programs::MASTER_UNIVERSITY]);
        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));
        $form->ckeditor('description_it', __('Description it'));
        $form->ckeditor('description_en', __('Description en'));
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->image('image', __('Image'))->removable();
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->ckeditor('seo_description_it', __('Seo description it'));
        $form->ckeditor('seo_description_en', __('Seo description en'));
        $form->textarea('seo_keywords_it', __('Seo keywords it'));
        $form->textarea('seo_keywords_en', __('Seo keywords en'));
        $form->select('status', __('Status'))->options([Programs::DISABLED => __('Disabled'),Programs::ENABLED => __('Enabled')]);

        return $form;
    }

    public function all(Request $request)
    {
        $q = $request->get('q');
        return Programs::where('title_en', 'like', "%$q%")->paginate(null, ['url', 'title_en as text']);
    }
}
