<?php

namespace App\Admin\Controllers;

use App\Block;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BlockController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Block';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Block());

        $grid->column('id', __('Id'));
        $grid->column('page', __('Page'));
        $grid->column('title_it', __('Title it'));
        $grid->column('title_en', __('Title en'));
        $grid->column('status', __('Status'))->using([Block::DISABLED => __('Disabled'),Block::ENABLED => __('Enabled')]);

        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->disableCreateButton(true);
        $grid->disableBatchActions(true);


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Block::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('page', __('Page'));
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('text_it', __('Text it'));
        $show->field('text_en', __('Text en'));
        $show->field('status', __('Status'))->using([Block::DISABLED => __('Disabled'),  Block::ENABLED => __('Enabled')]);

        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Block());

        $form->text('page', __('Page'));
        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->select('status', __('Status'))->options([Block::DISABLED => __('Disabled'),  Block::ENABLED => __('Enabled')]);
        $form->tools(function (Form\Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();

        });

        return $form;
    }
}
