<?php

namespace App\Admin\Controllers;

use App\StaticPages;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class StaticPagesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Static Pages';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new StaticPages());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('image', __('Image'))->image();
        $grid->column('title_it', __('Title it'));
        $grid->column('title_en', __('Title en'));
        $grid->column('status', __('Status'))->using([StaticPages::DISABLED => __('Disabled'),StaticPages::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(StaticPages::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('image', __('Image'))->image();
        $show->field('title_it', __('Title it'));
        $show->field('title_en', __('Title en'));
        $show->field('text_it', __('Text it'))->unescape();
        $show->field('text_en', __('Text en'))->unescape();
        $show->field('seo_title_it', __('Seo title it'));
        $show->field('seo_title_en', __('Seo title en'));
        $show->field('seo_decription_it', __('Seo decription it'));
        $show->field('seo_description_en', __('Seo description en'));
        $show->field('seo_keywords_it', __('Seo keywords it'));
        $show->field('seo_keywords_en', __('Seo keywords en'));
        $show->field('status', __('Status'))->using([StaticPages::DISABLED => __('Disabled'),StaticPages::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new StaticPages());

        $form->text('url', __('Url'));
        $form->image('image', __('Image'));
        $form->text('title_it', __('Title it'));
        $form->text('title_en', __('Title en'));
        $form->ckeditor('text_it', __('Text it'));
        $form->ckeditor('text_en', __('Text en'));
        $form->text('seo_title_it', __('Seo title it'));
        $form->text('seo_title_en', __('Seo title en'));
        $form->ckeditor('seo_decription_it', __('Seo decription it'));
        $form->ckeditor('seo_description_en', __('Seo description en'));
        $form->textarea('seo_keywords_it', __('Seo keywords it'));
        $form->textarea('seo_keywords_en', __('Seo keywords en'));
        $form->select('status', __('Status'))->options([StaticPages::DISABLED => __('Disabled'),StaticPages::ENABLED => __('Enabled')]);
        return $form;
    }
}
