<?php

namespace App\Admin\Controllers;

use App\Institutions;
use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class InstitutionsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Institutions';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Institutions());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('name_it', __('Name it'));
        $grid->column('name_en', __('Name en'));
        $grid->column('status', __('Status'))->using([Institutions::DISABLED => __('Disabled'),Institutions::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Institutions::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('name_it', __('Name it'));
        $show->field('name_en', __('Name en'));
        $show->field('status', __('Status'))->using([Institutions::DISABLED => __('Disabled'),Institutions::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Institutions());

        $form->text('url', __('Url'));
        $form->text('name_it', __('Name it'));
        $form->text('name_en', __('Name en'));
        $form->icon('icon',__('Icon'));
        $form->select('status', __('Status'))->options([Institutions::DISABLED => __('Disabled'),Institutions::ENABLED => __('Enabled')]);

        $form->saved(function (Form $form) {
            $userId = Admin::user()->id;
            $educationEntitiesMenuId = Menu::where('title','Education Entities')->first();
            $institutionId = $form->model()->id;
            if ($educationEntitiesMenuId) {
                // Try find element in menu
                $elMenu = Menu::where('parent_id',$educationEntitiesMenuId)->where('uri','like','%education-entities='.$institutionId.'%')->first();
                if (!$elMenu) {
                    // add new Menu element
                    $newMenu = new Menu();
                    $newMenu->parent_id = $educationEntitiesMenuId->id;
                    $newMenu->title = $form->model()->name_en;
                    $newMenu->uri = '/education-entities?institution_id='.$institutionId;
                    $newMenu->order = 0;
                    $newMenu->permission = '*';
                    $newMenu->withPermission();
                    $newMenu->icon = $form->model()->icon;
                    $newMenu->save();
                } else {
                    $elMenu->parent_id = $educationEntitiesMenuId->id;
                    $elMenu->title = $form->model()->name_en;
                    $elMenu->uri = '/education-entities?institution_id='.$institutionId;
                    $elMenu->order = 0;
                    $elMenu->permission = '*';
                    $elMenu->withPermission();
                    $elMenu->icon = $form->model()->icon;
                    $elMenu->save();
                }
            }

        });

        return $form;
    }
}
