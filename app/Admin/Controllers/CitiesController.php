<?php

namespace App\Admin\Controllers;

use App\Cities;
use App\Countries;
use App\Province;
use App\Regions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CitiesController extends CustomAdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Cities';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Cities());

        $grid->column('id', __('Id'));
        $grid->column('url', __('Url'));
        $grid->column('country.name_en', __('Country'));
        $grid->column('region.name_en', __('Region'));
        $grid->column('province.name_en', __('Province'));
        $grid->column('name_it', __('Name It'));
        $grid->column('name_en', __('Name En'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Cities::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('url', __('Url'));
        $show->field('country_id', __('Country'))->as(function ($country_id){
            $country = Countries::find($country_id);
            if ($country) {
                return $country->name_en;
            } else {
                return 'Not select';
            }

        });
        $show->field('region_id', __('Region'))->as(function ($region_id){
            $region = Regions::find($region_id);
            if ($region) {
                return $region->name_en;
            } else {
                return 'Not select';
            }

        });
        $show->field('province_id', __('Province'))->as(function ($province_id){
            $province = Province::find($province_id);
            if ($province) {
                return $province->name_en;
            } else {
                return 'Not select';
            }

        });
        $show->field('name_it', __('Name It'));
        $show->field('name_en', __('Name En'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }
    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $form = new Form(new Cities());

        $city = $id ? Cities::find($id): null;

        $form->text('url', __('Url'));
        $form->select('country_id', __('Country'))->options(function($id){
            $country = Countries::find($id);
            if ($country) {
                return [$country->id => $country->name_en];
            }

        })->ajax('/admin/countries/all')->load('region_id','/admin/regions/all');
        if ($form->isEditing()) {
            if ($city) {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',$city->country_id)->pluck('name_en','id'));
            } else {
                $form->select('region_id',__('Region'))
                    ->options(Regions::where('country_id',$form->country_id)->pluck('name_en','id'));
            }

        } else {
            $form->select('region_id',__('Region'));
        }

        if ($form->isEditing()) {
            if ($city) {
                $form->select('province_id',__('Province'))
                    ->options(Province::where('country_id',$city->country_id)->where('region_id',$city->region_id)->pluck('name_en','id'));
            } else {
                $form->select('province_id',__('Province'))
                    ->options(Province::where('country_id',$form->country_id)->where('region_id',$form->region_id)->pluck('name_en','id'));
            }

        } else {
            $form->select('province_id',__('Province'));
        }




        $form->text('name_it', __('Name It'));
        $form->text('name_en', __('Name En'));

        return $form;
    }

    public function all(Request $request)
    {
        $provinceId = $request->get('q');

        $province = Province::where(['id'=>$provinceId])->first();

        if ($province) {
            return Cities::where('province_id', $province->id)->where(['country_id'=>$province->country_id])->
            where(['region_id'=>$province->region_id])->get(['id', DB::raw('name_en as text')]);

        } else {
            return '';
        }
    }
}
