<?php

namespace App\Admin\Controllers;

use App\Disciplines;
use App\EducationEntities;
use App\Terms;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class DisciplinesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Disciplines';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Disciplines());

        $grid->column('id', __('Id'));
        $grid->column('entity.name_en', __('Education Entity'));
        $grid->column('name_it', __('Name it'));
        $grid->column('name_en', __('Name en'));
        $grid->column('status', __('Status'))->using([Disciplines::DISABLED => __('Disabled'),Disciplines::ENABLED => __('Enabled')]);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Disciplines::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('entity_id', __('Education Entity'));

        $show->field('entity_id', __('Education Entity'))->as(function ($entityId){
            $entity = EducationEntities::find($entityId);
            if ($entity) {
                return $entity->name_en;
            } else {
                return 'Not select';
            }
        });

        $show->field('name_it', __('Name it'));
        $show->field('name_en', __('Name en'));

        $show->field('terms', __('Terms'))->as(function ($terms){
            $mas = [];
            foreach ($terms as $term){
                $mas[] = $term->name_en;
            }
            return implode(', ',$mas);
        });
        $show->field('image')->image();
        $show->field('status', __('Status'))->using([Disciplines::DISABLED => __('Disabled'),Disciplines::ENABLED => __('Enabled')]);
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Disciplines());

        $form->select('entity_id',__('Entity'))->options(function ($id) {
            $entity = EducationEntities::find($id);

            if ($entity) {
                return [$entity->id => $entity->name_en];
            }
        })->ajax('/admin/education-entities/entities');
        $form->text('name_it', __('Name it'));
        $form->text('name_en', __('Name en'));
        $form->ckeditor('description_it',__('Description It'));
        $form->ckeditor('description_en',__('Description En'));
        $form->multipleSelect('terms', __('Terms'))->options(Terms::all()->pluck('name_en','id'));
        $form->multipleImage('image', __('Images'));
        $form->select('status', __('Status'))->options([Disciplines::DISABLED => __('Disabled'),Disciplines::ENABLED => __('Enabled')]);

        return $form;
    }
}
