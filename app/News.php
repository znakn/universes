<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\Resizable;
use App\Helpers\Thumbs;
use App\Helpers\Slug;

class News extends Model
{
    use Resizable;
    const ENABLED = 1;
    const DISABLED = 0;
    const ON_MODERATE = 2;

    const FORM_TYPE = 'news';
    const GALLERY_PATH_EXT = '/upload/news/';

    protected $table = 'news';

    public function getStatusAsText()
    {
        $textStatus = '';
        switch ($this->status)
        {
            case self::ENABLED:
                $textStatus = __('profile.enable');
                break;
            case self::DISABLED:
                $textStatus = __('profile.disable');
                break;
            case self::ON_MODERATE:
                $textStatus = __('profile.on_moderate');
                break;
        }

        return $textStatus;
    }


    public function author()
    {
        return $this->hasOne('App\User','id','user_id');
    }



    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = Slug::url($model->title_it);
            }
        });


        self::updating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = Slug::url($model->title_it);
            }
        });

        self::saved(function ($model) {
            if ($model->image) {
                Thumbs::makeResizePhoto(self::FORM_TYPE,$model->image);
            }
        });

    }

    public function saveNewNews($data)
    {
        $this->status = self::ON_MODERATE;
        $this->title_it = $data['title'];
        $this->title_en = $data['title'];
        $this->text_en = $data['text'];
        $this->text_it = $data['text'];
        if (isset($data['upload_image'])) {
            $this->image = isset($data['upload_image'])?$data['upload_image']:'';
            Thumbs::makeResizePhoto(self::FORM_TYPE,$data['upload_image']);
        }
        $this->user_id = $data['user_id'];
        $this->program = $data['program'];
        $this->url = Slug::url($data['title']);
        $this->seo_title_en = $data['title'];
        $this->seo_title_it = $data['title'];
        $this->seo_description_en = mb_substr($data['text'],0,250);
        $this->seo_description_it = mb_substr($data['text'],0,250);
        $this->seo_keywords_en = mb_substr($data['title'],0,100);
        $this->seo_keywords_it = mb_substr($data['title'],0,100);
        $this->save();
    }

    private function create_slug($string){
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return $slug;
    }

}
