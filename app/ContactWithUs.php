<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactWithUs extends Model
{
    protected $table = 'contact_with_us';

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

}
