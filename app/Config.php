<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    //
    protected $table = 'config';

    public static function getValue($key,$default = '')
    {
        $res = '';
        $record = self::where(['key'=>$key])->first();
        if ($record) {
            $res = $record->value;
        } else {
            $res = $default;
        }
        return $res;
    }

    public static function setValue($key,$value)
    {
        $record = self::where(['key'=>$key])->first();
        if ($record) {
            $record->value = $value;
        } else {
            $record = new self();
            $record->key = $key;
            $record->value = $value;
        }
        $record->save();
    }
}
