<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    //
    protected $table = 'countries';

    public function regions()
    {
        return $this->hasMany('App\Regions','id','country_id');
    }
}
