<?php

namespace App\Helpers;


class Slug
{
    public static function url($string)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return mb_strtolower($slug);
    }
}