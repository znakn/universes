<?php

namespace App;

use Encore\Admin\Traits\Resizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Slug;

class EducationEntities extends Model
{
    use Resizable;
    const ENABLED = 1;
    const DISABLED = 0;

    protected $table = 'education_entities';

    private $gallereies = [];

    protected $appends = [
        'gallereies'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['gallereis'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['url', 'web_address', 'facebook_address', 'twitter_address', 'instagram_address',
        'linkedin_address', 'institution_id', 'country_id', 'region_id', 'city_id', 'email',
        'phone', 'address', 'name_it', 'name_en', 'decription_it', 'description_en',
        'text_it', 'text_en', 'program_title_it', 'program_title_en', 'program_text_it',
        'program_text_en', 'seo_title_it', 'seo_title_en', 'seo_description_it',
        'seo_description_en', 'seo_keywords_it', 'seo_keywords_en', 'status',
        'logo', 'image', 'updated_at', 'created_at'];


    public function country()
    {
        return $this->hasOne('App\Countries', 'id', 'country_id');
    }

    public function region()
    {
        return $this->hasOne('App\Regions', 'id', 'region_id');
    }

    public function city()
    {
        return $this->hasOne('App\Cities', 'id', 'city_id');
    }

    public function province()
    {
        return $this->hasOne('App\Province','id','province_id');
    }

    public function institution()
    {
        return $this->hasOne('App\Institutions', 'id', 'institution_id');
    }

    public function education_program()
    {
        return $this->hasOne('App\Programs', 'url', 'program');
    }

    public function images()
    {
        return $this->hasMany('App\Gallereies', 'id', 'entity_id');
    }

    public function disciplines()
    {
        return $this->hasMany('App\Disciplines', 'id', 'entity_id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Reviews', 'id', 'entity_id');
    }

    public function setGallereiesAttribute($images)
    {
        $this->gallereies = $images;
    }

    public static function boot()
    {
        parent::boot();



        self::saved(function ($model) {

            $images = $model->gallereies;
            if (is_array($images)) {
                foreach ($images as $image) {
                    $img = Gallereies::where('image', $image)->where('entity_id', $model->id)->first();
                    if (!$img) {
                        $imgn = new Gallereies();
                        $imgn->entity_id = $model->id;
                        $imgn->image = $image;
                        $imgn->save();
                        unset($imgn);
                    }
                }
            }
        });

        self::deleting(function($model){
            // Delete all related entities with this entity
            // Delete related images
            $entityId = $model->id;

            $images = Gallereies::where(['entity_id' => $entityId])->get();

            foreach ($images as $image) {
                $image->delete();
            }
            // Delete related disciplines
            $disciplines = Disciplines::where(['entity_id' => $entityId])->get();
            foreach ($disciplines as $discipline) {
                $disciplineTerms = DisciplinesTerms::where(['discipline_id' => $discipline->id])->get();
                foreach ($disciplineTerms as $dt) {
                    $dt->delete();
                }
                $discipline->delete();
            }
            // Delete related reviews
            $reviews = Reviews::where(['entity_id' => $entityId])->get();
            foreach ($reviews as $review) {
                $review->delete();
            }

        });
    }


    public function getGallereiesAttribute()
    {
        $images = Gallereies::where('entity_id', $this->id)->get();
        foreach ($images as $image) {
            $this->gallereies[] = $image->image;
        }
        return $this->gallereies;
    }


    public function getList($params)
    {
        $query = self::newQuery();
        $query->select('education_entities.*');
        if (isset($params['status'])) {
            $query = $query->where('education_entities.status', '=', $params['status']);
        }
        if (isset($params['program'])) {
            $query = $query->where('education_entities.program', '=',$params['program']);
        }
        if (isset($params['region'])) {
            $query = $query->join('regions', 'education_entities.region_id', '=', 'regions.id')->where('regions.url', '=', $params['region']);
        }
        if (isset($params['province'])) {
            $query = $query->join('provinces', 'education_entities.province_id', '=', 'provinces.id')->where('provinces.url', '=', $params['province']);
        }
        if (isset($params['city'])) {
            $query = $query->join('cities', 'education_entities.city_id', '=', 'cities.id')->where('cities.url', '=', $params['city']);
        }
        if (isset($params['string'])) {
            $query = $query->where('education_entities.name_en', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.name_it', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.description_it', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.description_it', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.text_it', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.text_en', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.email', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.program_title_it', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.program_title_en', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.address', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.phone', 'like', '%' . $params['string'] . '%')
                ->orWhere('education_entities.web_address', 'like', '%' . $params['string'] . '%');
        }
        $limit = isset($params['limit']) ? $params['limit'] : 0;
        $offset = isset($params['offset']) ? $params['offset'] : 0;
        if ($limit) {
            $query = $query->limit($limit);
        }
        if ($offset) {
            $query = $query->offset($offset);
        }

        return $query->paginate(9);
    }

    public function getRating()
    {
        $allRatings = Rating::where(['entity_id'=>$this->id])->get();
        $count = 0;
        $avgSum = 0;
        $structura = 0;
        $socialActivities = 0;
        $courses = 0;
        $accommodation = 0;
        $teachers = 0;
        foreach ($allRatings as $rating)
        {
            $avgSum = $avgSum+$rating->avg;
            $structura = $structura+$rating->structura;
            $socialActivities = $socialActivities+$rating->social_activities;
            $courses = $courses+$rating->courses;
            $accommodation = $accommodation+$rating->accommodation;
            $teachers = $teachers+$rating->teachers;

            $count++;
        }
        $totalRating = ($count > 0) ? $avgSum/$count:0;
        $totalStructura = ($count>0)? round($structura/$count,1):0;
        $totalSocialActivities = ($count>0)? round($socialActivities/$count,1):0;
        $totalCourses = ($count>0)? round($courses/$count,1):0;
        $totalAccommodation = ($count>0)? round($accommodation/$count,1):0;
        $totalTeachers = ($count>0)? round($teachers/$count,1):0;
        return ['rating'=>round($totalRating,1),'count'=>$count,'structura'=>$totalStructura,'social_activities'=>$totalSocialActivities
        ,'courses'=>$totalCourses,'accommodation'=>$totalAccommodation,'teachers'=>$totalTeachers];
    }


}
