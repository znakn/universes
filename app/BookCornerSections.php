<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookCornerSections extends Model
{
    //

    protected $table = 'book_corner_sections';

    const ENABLED = 1;
    const DISABLED = 0;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            // ... code here
            if (!$model->url) {
                $model->url = $model->create_slug($model->title_it);
            }
        });


        self::updating(function ($model) {
            // ... code here
            if (!$model->url) {
                $model->url = $model->create_slug($model->title_it);
            }
        });

    }

    private function create_slug($string)
    {
        $slug = mb_strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $string));
        return $slug;
    }

    public static function getListByProgram($program)
    {
        return self::where(['program' => $program])->where(['status' => self::ENABLED])->get();
    }

}
