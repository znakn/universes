<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\Resizable;
use App\Helpers\Slug;

class AdminNews extends Model
{
    use Resizable;
    //
    const ENABLED = 1;
    const DISABLED = 0;

    protected $table = 'admin_news';

    public function author()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = Slug::url($model->title_it);
            }
        });


        self::updating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = Slug::url($model->title_it);
            }
        });

    }

    public static function getYoutubeIdFromUrl($url) {
        $parts = parse_url($url);
        if(isset($parts['query'])){
            parse_str($parts['query'], $qs);
            if(isset($qs['v'])){
                return $qs['v'];
            }else if(isset($qs['vi'])){
                return $qs['vi'];
            }
        }
        if(isset($parts['path'])){
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path)-1];
        }
        return false;
    }

}
