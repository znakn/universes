<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institutions extends Model
{
    const ENABLED = 1;
    const DISABLED = 0;

    protected $table = 'institutions';

    public static function boot()
    {
        parent::boot();

        self::deleting(function($model){
            // Delete all related entities with this entity
            // Delete related images
            $institutionId = $model->id;

            $entities = EducationEntities::where(['institution_id'=>$institutionId])->get();
            foreach ($entities as $entity) {
                $entity->delete();
            }


        });
    }

}
