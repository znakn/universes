<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticPages extends Model
{
    const ENABLED = 1;
    const DISABLED = 0;

}
