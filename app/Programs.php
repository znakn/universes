<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programs extends Model
{
    const ENABLED = 1;
    const DISABLED = 0;
    const SECONDARY_SCHOOL = 'secondary_school';
    const MASTER_UNIVERSITY = 'master_university';


    protected $table = 'programs';


    public static function getCaption($programUrl)
    {
        $lang = app()->getLocale();
        $program = self::where(['url'=>$programUrl])->first();
        $caption = isset($program->{'title_'.$lang})?$program->{'title_'.$lang}:'';
        return $caption;
    }


}
