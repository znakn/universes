<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    //
    protected $table = 'blocks';

    const ENABLED = 1;
    const DISABLED = 0;
}
