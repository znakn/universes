<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disciplines extends Model
{
    const ENABLED = 1;
    const DISABLED = 0;

    protected $table = 'disciplines';

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = $model->create_slug($model->name_it);
            }
        });


        self::updating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = $model->create_slug($model->name_it);
            }
        });

    }


    public function entity()
    {
        return $this->hasOne('App\EducationEntities','id','entity_id');
    }

    public function terms()
    {
        return $this->belongsToMany('App\Terms','disciplines_terms','discipline_id','term_id');
    }

    public function setImageAttribute($image)
    {
        if (is_array($image)) {
            $this->attributes['image'] = json_encode($image);
        }
    }

    public function getImageAttribute($image)
    {
        return json_decode($image, true);
    }

    private function create_slug($string){
        $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return $slug;
    }

}
