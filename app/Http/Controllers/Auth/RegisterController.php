<?php

namespace App\Http\Controllers\Auth;

use App\Programs;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';


    private $program = '';

    private $locale = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        // Hac session
        $this->middleware(function ($request, $next) {
            $this->program = session()->get('program','');
            return $next($request);
        });
        $this->locale = app()->getLocale();
        $this->redirectTo = LaravelLocalization::localizeUrl('/');

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'last_name'=>'required|string|max:255',
            'country'=>'required|string|max:255',
            'city'=>'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/',
            'date_birth'=>'required',
            'prefer_language'=>'string',
            'is_agree_terms'=>'required'
        ],[
            'password.min'=>__('register.wrong_password'),
            'password.required'=>__('register.wrong_password'),
            'password.regex'=>__('register.wrong_password')
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'last_name'=>$data['last_name'],
            'country'=>$data['country'],
            'city'=>$data['city'],
            'email' => $data['email'],
            'prefer_language' => $data['prefer_language'],
            'password' => bcrypt($data['password']),
            'date_birth'=>date('Y-m-d',strtotime($data['date_birth'])),
            'is_agree_terms' => ($data['is_agree_terms'] == 'on') ? '1':'0'
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $data = [];
        $data['menu'] = '';

        if (!$this->program) {
            $this->program = Programs::MASTER_UNIVERSITY;
        }
        $data['color'] = ($this->program == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/'.$this->program);
        $data['seo']['title'] =  'Registration on portal';
        $data['seo']['description'] =  'Registration on portal tips';
        $data['seo']['keywords'] =  'Registration , tips ,portal , university';
        $data['program_url'] = $this->program;


        return view('auth.register',$data);
    }
}
