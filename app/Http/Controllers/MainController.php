<?php

namespace App\Http\Controllers;


use function abort;
use App\AdminNews;
use App\Block;
use App\BookCornerSections;
use App\BookCornerTypes;
use App\Contact;
use App\Gallereies;
use App\LogisticaPosts;
use App\Blog;
use App\Disciplines;
use App\EducationEntities;
use App\LogisticaTypes;
use App\News;
use App\Programs;
use App\BooksCornerPosts;
use App\Rating;
use App\Regions;
use App\Reviews;
use App\StaticPages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use function redirect;


class MainController extends Controller
{
    private $locale;

    private $program;

    public function __construct()
    {
        $this->locale = app()->getLocale();
        $this->middleware(function ($request, $next) {
            $this->program = session()->get('program',Programs::MASTER_UNIVERSITY);
            return $next($request);
        });
    }


    //
    public function index()
    {
        $programs = Programs::all();

        $titles = [];

        foreach ($programs as $program) {
            $el = [];
            $el['title'] = $program->{'title_' . $this->locale}; // str_replace(' ', '<br>', $program->{'title_' . $this->locale});
            $el['url'] = LaravelLocalization::localizeUrl('/program/' . $program->url);
            if ($program->url == Programs::SECONDARY_SCHOOL) {
                $el['class'] = 'school-link';
            } else {
                $el['class'] = 'university-link';
            }
            $titles[] = $el;
        }


        return view('home', ['titles' => $titles]);
    }

    public function custom($url)
    {
        $data = [];
        $page = StaticPages::where(['url'=>$url])->where(['status'=>StaticPages::ENABLED])->first();

        if ($page) {
            $program = Programs::where(['url' => $this->program])->first();
            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $this->program);
            $data['program_url'] = $program->url;
            $data['program'] = $program;
            $data['color'] = ($this->program == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['page'] = $page;
            $data['locale'] = $this->locale;

            $data['seo']['title'] = $page->{'seo_title_' . $this->locale} ?? '';
            $data['seo']['description'] = $page->{'seo_description_' . $this->locale} ?? '';
            $data['seo']['keywords'] = $page->{'seo_keywords_' . $this->locale} ?? '';
            $data['menu'] = '';


            return view('custom_page', $data);

        } else {
            abort(404);
        }

    }

    public function program($url)
    {
        $view = '';
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/');
        $data['regions'] = Regions::all('id', 'name_' . $this->locale . ' as name', 'svg_vertical as svg', 'url')->toArray();
        $data['locale'] = $this->locale;
        $data['menu'] = '';

        $program = Programs::where(['url' => $url])->first();
        if ($program) {
            Session::put('program', $url);
            $data['program'] = $program;
            $data['program_url'] = $program->url;
            $data['seo']['title'] = $program->{'seo_title_' . $this->locale} ?? '';
            $data['seo']['description'] = $program->{'seo_description_' . $this->locale} ?? '';
            $data['seo']['keywords'] = $program->{'seo_keywords_' . $this->locale} ?? '';
            $data['title'] = $program->{'title_' . $this->locale} ?? '';
            $data['description'] = $program->{'description_' . $this->locale} ?? '';
            $data['text'] = $program->{'text_' . $this->locale} ?? '';
            $data['image'] = $program->image;
            $data['color'] = ($url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['menu'] = '';

            return view('main_program', $data);
        } else {
            abort(404);
        }


    }

    public function entity_list_program($program, Request $request)
    {
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);
        $data['seo']['title'] = '';
        $data['seo']['description'] = '';
        $data['seo']['keywords'] = '';
        $data['regions'] = Regions::all('id', 'name_' . $this->locale . ' as name', 'svg_horizontal as svg', 'url', 'svg_view_box', 'svg_order', 'svg_style')->sortBy('svg_order')->toArray();


        $programObj = Programs::where(['url' => $program])->first();
        $data['program_url'] = $programObj->url;
        $data['program'] = $programObj;
        $data['region_url'] = '';

        $params = ['program' => $program, 'status' => EducationEntities::ENABLED];
        $entity = new EducationEntities();
        $entities = $entity->getList($params);
        $data['entities'] = $entities;
        $data['menu'] = 'tips';

        if ($program) {
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
        }
        return view('entity_list', $data);
    }

    public function entity_list_region($program, $region, Request $request)
    {
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);
        $data['seo']['title'] = '';
        $data['seo']['description'] = '';
        $data['seo']['keywords'] = '';
        $data['regions'] = Regions::all('id', 'name_' . $this->locale . ' as name', 'svg_horizontal as svg', 'url', 'svg_view_box', 'svg_order', 'svg_style')->sortBy('svg_order')->toArray();
        $data['menu'] = 'tips';


        /* @var Programs $programObj */
        $programObj = Programs::where(['url' => $program])->first();
        /* @var Regions $regionObj */
        $regionObj = Regions::where(['url' => $region])->first();

        if ($programObj && $regionObj) {
            $data['program'] = $programObj;
            $data['region_url'] = $region;
            $data['program_url'] = $program;
            $data['provinces'] = $regionObj->getProvincesListArray();

            $entity = new EducationEntities();
            $params = ['program' => $program,'region'=>$region, 'status' => EducationEntities::ENABLED];
            $entities = $entity->getList($params);
            $data['entities'] = $entities;


            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
        }
        return view('entity_list', $data);
    }

    public function entity($program, $slug)
    {
        $data = [];

        $entity = EducationEntities::where(['url' => $slug])->first();


        if ($entity) {
            $disciplines = Disciplines::where(['entity_id' => $entity->id,'status' => Disciplines::ENABLED])->get();
            $region = $entity->region->url;
            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);
            $data['seo']['title'] = '';
            $data['seo']['description'] = '';
            $data['seo']['keywords'] = '';
            $data['menu'] = 'tips';
            $data['regions'] = Regions::all('id', 'name_' . $this->locale . ' as name', 'svg_horizontal as svg', 'url', 'svg_view_box', 'svg_order', 'svg_style')->sortBy('svg_order')->toArray();
            $data['locale'] = $this->locale;

            $galleries = Gallereies::where(['entity_id' => $entity->id])->get();
            $data['galleries'] = $galleries;
            $reviews = Reviews::where(['entity_id' => $entity->id])->paginate(3);;
            $data['reviews'] = $reviews;
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $program])->first();
            /* @var Regions $regionObj */
            $regionObj = Regions::where(['url' => $region])->first();

            $data['existRatingByUser'] = Rating::existRating($entity->id);
            $totalRatingArray = $entity->getRating();
            $data['rating'] = $totalRatingArray['rating'];
            $data['ratingCount'] = $totalRatingArray['count'];
            $data['ratingArray'] = $totalRatingArray;

            if ($programObj && $regionObj) {
                $data['program'] = $programObj;
                $data['region_url'] = $region;
                $data['program_url'] = $program;
                $data['cities'] = $regionObj->getCitiesListArray();

                $data['entity'] = $entity;
                $data['disciplines'] = $disciplines;
                $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            }
            return view('entity', $data);


        } else {
            abort(404);
        }


    }

    public function news(Request $request, $program)
    {
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);
        /* @var Programs $programObj */
        $programObj = Programs::where(['url' => $program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
        $data['program_url'] = $programObj->url;
        $news = News::where(['status' => News::ENABLED])->where(['program' => $program])->orderBy('created_at','DESC')->paginate(4);
        $data['news'] = $news;
        $adminNews = AdminNews::where(['status'=>AdminNews::ENABLED])->where('image','<>','')->where(['program'=>$program])->orderBy('created_at','DESC')->paginate(2);
        $data['adminNews'] = $adminNews;
        $videoNews = AdminNews::where(['status'=>AdminNews::ENABLED])->where('video','<>','')->where(['program'=>$program])->orderBy('created_at','DESC')->paginate(2);
        $data['videoNews'] = $videoNews;
        $data['locale'] = $this->locale;
        $data['menu'] = 'news';
        $newsBlock  = Block::where(['page'=>'news_page'])->first();
        if ($newsBlock) {
            $data['newsBlock'] = $newsBlock;
        }

        return view('news_list', $data);
    }

    public function blog(Request $request, $program)
    {
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);
        /* @var Programs $programObj */
        $programObj = Programs::where(['url' => $program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
        $data['program_url'] = $programObj->url;
        $data['locale'] = $this->locale;
        $blogs = Blog::where(['status' => Blog::ENABLED])->where(['program' => $program])->orderBy('created_at','DESC')->get();
        $data['blogs'] = $blogs;
        $data['menu'] = Blog::FORM_TYPE;
        $blogBlock  = Block::where(['page'=>'blog_page'])->first();
        if ($blogBlock) {
            $data['blogBlock'] = $blogBlock;
        }

        return view('blog_list', $data);
    }


    public function books_corner(Request $request, $program, $section = '')
    {
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);

        $booksCornerTypes = BookCornerTypes::where(['status' => BookCornerTypes::ENABLED])->get();
        $data['booksCornerTypes'] = $booksCornerTypes;

        /* @var Programs $programObj */
        $programObj = Programs::where(['url' => $program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
        $data['program_url'] = $programObj->url;

        $bookCornerPost = new BooksCornerPosts();
        $param = ['program' => $program, 'status' => BooksCornerPosts::ENABLED,'order'=>['created_at'=>'DESC']];
        if ($section) {
            $param['section'] = $section;
        }
        $publications = $bookCornerPost->getList($param);
        $data['publications'] = $publications;

        $data['sections'] = BookCornerSections::where(['program'=>$program])->where(['status'=>BookCornerSections::ENABLED])->get();

        $data['locale'] = $this->locale;
        $data['menu'] = BooksCornerPosts::FORM_TYPE;
        $data['form_type'] = BooksCornerPosts::FORM_TYPE;
        $data['program'] = $programObj;
        $data['section'] = $section;
        $data['type_id'] = '';
        $block  =  Block::where(['page'=>'book_corner'])->first();
        if ($block) {
            $data['block'] = $block;
        }

        return view('books_corner_list', $data);
    }

    public function logistica(Request $request, $program)
    {
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);
        /* @var Programs $programObj */
        $programObj = Programs::where(['url' => $program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
        $data['program_url'] = $programObj->url;
        $data['program'] = $programObj;
        $data['locale'] = $this->locale;
        $logisticaTypes = LogisticaTypes::where(['status' => LogisticaTypes::ENABLED])->get();
        $data['logisticaTypes'] = $logisticaTypes;

        $params = ['status' => LogisticaPosts::ENABLED, 'program' => $program,'order'=>['created_at'=>'DESC']];
        $logisticaPost = new LogisticaPosts();
        $apartmentsReview = $logisticaPost->getList($params);
        $data['apartmentsReview'] = $apartmentsReview;
        $data['menu'] = LogisticaPosts::FORM_TYPE;
        $data['form_type'] = LogisticaPosts::FORM_TYPE;
        $data['regions'] = Regions::all('id', 'name_' . $this->locale . ' as name', 'svg_horizontal as svg', 'url', 'svg_view_box', 'svg_order', 'svg_style')->sortBy('svg_order')->toArray();


        return view('logistica_list', $data);
    }


    public function location($lang, Request $request)
    {
        app()->setLocale($lang);
        LaravelLocalization::setLocale($lang);
        Session::put('locale',$lang);
        $previeUrl = url()->previous();
        return redirect(LaravelLocalization::localizeUrl($previeUrl, $lang));
    }

    public function news_view($slug)
    {
        $data = [];
        $data['menu'] = 'news';

        $news = News::where(['url' => $slug])->where(['status' => News::ENABLED])->first();
        if ($news) {
            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $news->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $news->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['news'] = $news;
            $data['seo']['title'] = $news->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $news->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $news->{'seo_keywords_' . $this->locale};


            return view('news', $data);
        } else {
            abort(404);
        }
    }

    public function news_preview($slug)
    {
        if (!Auth::check()) return redirect('/');
        $data = [];
        $data['menu'] = 'news';
        $user = Auth::user();

        $news = News::where(['url' => $slug])->first();
        if ($news&&$news->user_id = $user->id) {
            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $news->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $news->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['news'] = $news;
            $data['seo']['title'] = $news->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $news->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $news->{'seo_keywords_' . $this->locale};


            return view('news', $data);
        } else {
            abort(404);
        }
    }

    public function admin_news_view($slug)
    {
        $data = [];
        $data['menu'] = 'news';

        $news = AdminNews::where(['url' => $slug])->where(['status' => AdminNews::ENABLED])->first();

        if ($news) {
            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $news->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $news->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['news'] = $news;
            $data['seo']['title'] = $news->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $news->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $news->{'seo_keywords_' . $this->locale};


            return view('admin_news', $data);
        } else {
            abort(404);
        }
    }


    public function review_view($program, $slug)
    {
        $data = [];
        $data['menu'] = 'tips';

        $review = Reviews::where(['url' => $slug])->where(['status' => Reviews::ENABLED])->first();
        if ($review) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['review'] = $review;
            $data['seo']['title'] = $review->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $review->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $review->{'seo_keywords_' . $this->locale};


            return view('review', $data);
        } else {
            abort(404);
        }
    }

    public function advert_view($slug)
    {
        $data = [];
        $data['menu'] = BooksCornerPosts::FORM_TYPE;

        $advert = BooksCornerPosts::where(['url' => $slug])->where(['status' => BooksCornerPosts::ENABLED])->first();
        if ($advert) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $advert->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $advert->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['advert'] = $advert;
            $data['seo']['title'] = $advert->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $advert->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $advert->{'seo_keywords_' . $this->locale};


            return view('book_advert', $data);
        } else {
            abort(404);
        }
    }

    public function advert_preview($slug)
    {
        if (!Auth::check()) return redirect('/');
        $data = [];
        $data['menu'] = BooksCornerPosts::FORM_TYPE;
        $user = Auth::user();
        $advert = BooksCornerPosts::where(['url' => $slug])->first();
        if ($advert&&($advert->user_id == $user->id)) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $advert->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $advert->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['advert'] = $advert;
            $data['seo']['title'] = $advert->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $advert->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $advert->{'seo_keywords_' . $this->locale};


            return view('book_advert', $data);
        } else {
            abort(404);
        }
    }

    public function logistic_advert_view($slug)
    {
        $data = [];
        $data['menu'] = LogisticaPosts::FORM_TYPE;

        $advert = LogisticaPosts::where(['url' => $slug])->where(['status' => LogisticaPosts::ENABLED])->first();
        if ($advert) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $advert->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $advert->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['advert'] = $advert;
            $data['seo']['title'] = $advert->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $advert->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $advert->{'seo_keywords_' . $this->locale};


            return view('logistica_advert', $data);
        } else {
            abort(404);
        }
    }

    public function logistic_advert_preview($slug)
    {
        if (!Auth::check()) return redirect('/');
        $data = [];
        $data['menu'] = LogisticaPosts::FORM_TYPE;
        $user = Auth::user();

        $advert = LogisticaPosts::where(['url' => $slug])->first();
        if ($advert&&($advert->user_id == $user->id)) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $advert->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $advert->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['advert'] = $advert;
            $data['seo']['title'] = $advert->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $advert->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $advert->{'seo_keywords_' . $this->locale};


            return view('logistica_advert', $data);
        } else {
            abort(404);
        }
    }

    public function blog_view($slug)
    {
        $data = [];
        $data['menu'] = Blog::FORM_TYPE;

        $blog = Blog::where(['url' => $slug])->where(['status' => Blog::ENABLED])->first();
        if ($blog) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $blog->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $blog->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['blog'] = $blog;
            $data['seo']['title'] = $blog->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $blog->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $blog->{'seo_keywords_' . $this->locale};

            return view('blog', $data);
        } else {
            abort(404);
        }
    }

    public function blog_preview($slug)
    {
        if (!Auth::check()) return redirect('/');
        $data = [];
        $data['menu'] = Blog::FORM_TYPE;
        $user = Auth::user();

        $blog = Blog::where(['url' => $slug])->first();
        if ($blog&&($blog->user_id == $user->id)) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $blog->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $blog->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['blog'] = $blog;
            $data['seo']['title'] = $blog->{'seo_title_' . $this->locale};
            $data['seo']['description'] = $blog->{'seo_description_' . $this->locale};
            $data['seo']['keywords'] = $blog->{'seo_keywords_' . $this->locale};

            return view('blog', $data);
        } else {
            abort(404);
        }
    }

    public function info()
    {
        if (!$this->program) {
            $this->program = Programs::MASTER_UNIVERSITY;
        }
        $data = [];
        $data['menu'] = 'info';
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $this->program);

        $programObj = Programs::where(['url' => $this->program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
        $data['program_url'] = $programObj->url;
        $data['program'] = $programObj;
        $data['locale'] = $this->locale;

        $contacts = Contact::where(['status' => Contact::ENABLED])->get();
        $data['contacts'] = $contacts;
        $block  =  Block::where(['page'=>'info_page'])->first();
        if ($block) {
            $data['block'] = $block;
        }
        return view('info', $data);
    }

    public function discipline_view($slug)
    {
        $data = [];
        $data['menu'] = 'tips';

        $discipline = Disciplines::where(['url' => $slug])->where(['status' => Disciplines::ENABLED])->first();
        if ($discipline) {

            $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/' . $this->program);
            /* @var Programs $programObj */
            $programObj = Programs::where(['url' => $this->program])->first();
            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY) ? 'blue' : 'green';
            $data['program_url'] = $programObj->url;
            $data['program'] = $programObj;
            $data['locale'] = $this->locale;
            $data['discipline'] = $discipline;
            $data['seo']['title'] = $discipline->{'name_' . $this->locale};
            $data['seo']['description'] = $discipline->{'description_' . $this->locale};
            $data['seo']['keywords'] = $discipline->{'name_' . $this->locale};

            return view('discipline', $data);
        } else {
            abort(404);
        }
    }





    public function rating(Request $request)
    {
        if (!Auth::check()) return redirect('/');

        $data = [];
        $structura = $request->post('structura');
        $course = $request->post('course');
        $teachers = $request->post('teachers');
        $socialActivities = $request->post('social_activities');
        $accommodation = $request->post('accommodation');
        $entityId = $request->post('entity_id');
        $user = Auth::user();

        $enity = EducationEntities::where(['id'=>$entityId])->first();

        if(!$enity) {
            abort(404);
        }

        // check exist record with this entity and rating in our DB
        $ratingRecord = Rating::where(['entity_id'=>$entityId,'user_id'=>$user->id])->first();

        if (!$ratingRecord) {
            $rating = new Rating();
            $rating->entity_id = $entityId;
            $rating->user_id = $user->id;
            $rating->structura = $structura;
            $rating->courses = $course;
            $rating->teachers = $teachers;
            $rating->social_activities = $socialActivities;
            $rating->accommodation = $accommodation;
            $rating->avg = ($structura + $course + $teachers + $socialActivities + $accommodation)/5;
            $rating->save();
            $title = __('Thank you for your post');
        } else {
            $title = __('Oh , your rating already exist');
        }

        $program = Session::get('program');

        /* @var Programs $programObj */
        $programObj = Programs::where(['url'=>$program])->first();

        $data['homeUrl'] = LaravelLocalization::localizeUrl('/');
        $data['locale'] = $this->locale;
        $data['post_type'] = __('Rating');
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['program_url'] = $programObj->url;
        $data['menu'] = '';
        $data['page_title'] = $title;
        $data['return_url'] = LaravelLocalization::localizeUrl('/'.$program.'/entity/'.$enity->url);
        $data['return_text'] = __('Return to enity');
        return view('thanks_for_post',$data);
    }




    public function test(Request $request)
    {
        $s = $request->get('domen', '');
        if ($s) {
            $agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36';

            $curl = curl_init('https://google.com/search?q=site:' . $s);
            curl_setopt($curl, CURLOPT_USERAGENT, $agent);
            curl_setopt($curl, CURLOPT_FAILONERROR, true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            echo $result;
        }
    }

}
