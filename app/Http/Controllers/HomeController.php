<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Programs;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function custom_page() 
    {
        $program = Programs::MASTER_UNIVERSITY;
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/'.$program);
        /* @var Programs $programObj */
        $programObj = Programs::where(['url'=>$program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['program_url'] = $programObj->url;
        $data['locale'] = 'en';
        $data['menu'] = '';

        return view('custom',$data);
    }
}
