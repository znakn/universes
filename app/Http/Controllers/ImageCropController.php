<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;



class ImageCropController extends Controller
{

    private $locale = '';
    private $config = [];


    public function __construct()
    {
        $this->locale = app()->getLocale();
        $this->config = config('app.img');
    }


    //
    public function upload(Request $request)
    {
        $imgType = $request->post('image_type');
        $folderPath = public_path('upload/');
        $imagePath = public_path('upload/'.$imgType);
//        if (!file_exists($folderPath)) {
//            mkdir($folderPath,777);
//        }
        if (!File::isDirectory($imagePath)) {
            File::makeDirectory($imagePath,0775,true,true);
        }


        $imageParts = explode(";base64,", $request->image);
        $imageTypeAux = explode("image/", $imageParts[0]);
        $imageType = $imageTypeAux[1];
        $imageBase64 = base64_decode($imageParts[1]);

        $baseImageFileName = uniqid() . '.png';


        $fileConfig = $this->config[$imgType];


        $file = $imagePath .'/origin_'.$baseImageFileName;
        file_put_contents($file, $imageBase64);

        // make resize image
        foreach ($this->config[$imgType] as $configName => $configValue) {
            $this->createResizeImage($file,$configName,$configValue['width'],$configValue['height']);
        }

        return response()->json(['success'=>'success','file' => '/upload/'.$imgType.'/thumb_'.$baseImageFileName,'file_name' => $baseImageFileName]);
    }

    private function createResizeImage($file,$name,$width,$height)
    {
        $baseFileName = '';
        $baseFilePath = '';
        $pos = mb_strpos($file,'origin_');
        if ($pos) {
            $baseFileName = mb_substr($file,$pos+7);
            $baseFilePath = mb_substr($file,0,$pos);
        }

        // create instance
        $img = Image::make($file);
        $img->resize($width,$height);
        $img->save($baseFilePath.'/'.$name.'_'.$baseFileName);


    }





}
