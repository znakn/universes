<?php

namespace App\Http\Controllers;

use function abort;
use App\Blog;
use App\BooksCornerPosts;
use App\Cities;
use App\LogisticaPosts;
use App\News;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Programs;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\BookCornerSections;
use App\BookCornerTypes;
use App\Regions;
use App\Province;
use App\LogisticaTypes;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    private $program;
    private $config = [];
    private $locale = '';

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->program = session()->get('program',Programs::MASTER_UNIVERSITY);
            return $next($request);
        });
        $this->config = config('app.img');
        $this->locale = app()->getLocale();
    }


    public function index($slug = 'profile')
    {
        if (!Auth::check()) abort(404);

        $data = [];
        $data['menu'] = '';
        $data['color'] = ($this->program == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/'.$this->program);
        $data['seo']['title'] =  __('profile.seo_profile_title');
        $data['seo']['description'] =  __('profile.seo_profile_description');
        $data['seo']['keywords'] =  __('profile.seo_profile_keywords');
        $data['locale'] = app()->getLocale();
        $user = Auth::user();


        if (!$this->program) {
            $this->program = Programs::MASTER_UNIVERSITY;
        }
        $data['program_url'] = $this->program;
        $data['user'] = $user;
        $data['profileMessage'] = '';
        $data['count_books_posts'] = ($user->books_corner_posts) ? count($user->books_corner_posts):'0';
        $data['count_accommodations_posts'] = ($user->accommodation_posts) ? count($user->accommodation_posts):'0';
        $data['count_blog_posts'] = ($user->blogs_posts) ? count($user->blogs_posts):'0';
        $data['count_news_posts'] = ($user->news_posts)?  count($user->news_posts):'0';


        switch ($slug) {
            case 'profile':


                if (Session::has('profile_message')) {
                    $data['profileMessage'] = Session::get('profile_message','');
                    Session::forget('profile_message');
                }
                $data['active'] = 'profile';
                $view = 'profile/profile';

                break;
            case 'book_posts':
                $data['active'] = 'book_posts';
                $data['posts'] = $user->books_corner_posts()->orderBy('id','desc')->paginate(10);
                $view = 'profile/book_posts';
                break;
            case 'accommodation_posts':
                $data['active'] = 'accommodation_posts';
                $data['posts'] = $user->accommodation_posts()->orderBy('id','desc')->paginate(10);
                $view = 'profile/accommodation_posts';
                break;
            case 'blogs':
                $data['active'] = 'blogs';
                $data['posts'] = $user->blogs_posts()->orderBy('id','desc')->paginate(10);
                $view = 'profile/blogs';
                break;
            case 'news':
                $data['active'] = 'news';
                $data['posts'] = $user->news_posts()->orderBy('id','desc')->paginate(10);
                $view = 'profile/news';
                break;
        }


        return view($view,$data);

    }

    public function profile_update(Request $request)
    {

        $user = \auth()->user();

        if ($request->post('old_password','')) {
            $validatedData = $request->validate([
                    'name' => 'required|string|max:255',
                    'last_name'=>'required|string|max:255',
                    'country'=>'required|string|max:255',
                    'city'=>'required|string|max:255',
                    'password' => 'required|string|min:8|confirmed|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/',
                    'date_birth'=>'required',
                    'prefer_language'=>'string',
                    'old_password' => function ($attribute, $value, $fail) {
                        if (!Hash::check($value, auth()->user()->password)) {
                            $fail('The old password is invalid.');
                        }
                    },
                ],[
                    'password.min'=>__('register.wrong_password'),
                    'password.required'=>__('register.wrong_password'),
                    'password.regex'=>__('register.wrong_password')
                ]
            );

            $user->password = Hash::make($validatedData['password']);


        } else {
            $validatedData = $request->validate([
                    'name' => 'required|string|max:255',
                    'last_name'=>'required|string|max:255',
                    'country'=>'required|string|max:255',
                    'city'=>'required|string|max:255',
                    'date_birth'=>'required',
                    'prefer_language'=>'string',
                ]
            );
        }

        $user->name = $validatedData['name'];
        $user->last_name = $validatedData['last_name'];
        $user->country = $validatedData['country'];
        $user->city = $validatedData['city'];
        $user->date_birth = $validatedData['date_birth'];
        $user->prefer_language = $validatedData['prefer_language'];

        $user->save();

        Session::put('profile_message',__('profile.profile_change'));


        return redirect(LaravelLocalization::localizeUrl('/profile'));

    }

    public function edit_posts($type,$id)
    {
        if (!Auth::check()) abort(404);

        $data = [];

        $data['menu'] = '';
        $data['color'] = ($this->program == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/'.$this->program);
        $data['locale'] = app()->getLocale();
        $data['program_url'] = $this->program;
        $user = Auth::user();
        $data['user'] = $user;
        $data['form_type'] = $type;

        $view = '';

        switch ($type)
        {
            case BooksCornerPosts::FORM_TYPE:
                $post = BooksCornerPosts::where(['id'=>$id])->first();

                $view = 'profile/forms/book_corner_form_post';

                $data['seo']['title'] =  __('profile.seo_title_edit_books_corner_post');
                $data['seo']['description'] =  __('profile.seo_description_edit_books_corner_post');
                $data['seo']['keywords'] =  __('profile.seo_keywords_edit_books_corner_posts');
                $data['form_title'] = __('profile.edit_book_corner_post');
                $sections = BookCornerSections::where(['program'=>$this->program])->where(['status'=>BookCornerSections::ENABLED])->get();
                $data['booksCornerSections'] = $sections;
                $booksCornerTypes = BookCornerTypes::where(['status' => BookCornerTypes::ENABLED])->get();
                $data['booksCornerTypes'] = $booksCornerTypes;
                $data['img_config'] = $this->config[BooksCornerPosts::FORM_TYPE];



                break;
            case LogisticaPosts::FORM_TYPE:
                $post = LogisticaPosts::where(['id'=>$id])->first();
                $data['type_id'] = old('type_id')? old('type_id') : $post->type_id;
                $data['region_id'] = old('region_id') ? old('region_id') : $post->region_id;
                $data['province_id'] = old('province_id') ? old('province_id') : $post->province_id;
                $data['city_id'] = old('city_id') ? old('city_id') : $post->city_id;

                $view = 'profile/forms/logistica_form_post';

                $data['seo']['title'] =  __('profile.seo_title_edit_accommodation_post');
                $data['seo']['description'] =  __('profile.seo_description_edit_accommodation_post');
                $data['seo']['keywords'] =  __('profile.seo_keywords_edit_accommodation_posts');
                $data['form_title'] = __('profile.edit_accommodations_post');

                $data['regions'] = Regions::all('id','name_'.$this->locale.' as name','svg_horizontal as svg','url','svg_view_box','svg_order','svg_style')->sortBy('svg_order')->toArray();
                $logisticaTypes = LogisticaTypes::where(['status'=>LogisticaTypes::ENABLED])->get();
                $provinces = Province::select('id','name_'.$this->locale.' as name','url')->where(['region_id'=>$data['region_id']])->get();
                $data['provinces'] = $provinces;
                $cities = Cities::select('id','name_'.$this->locale.' as name','url')->where(['province_id'=>$data['province_id']])->get();
                $data['cities'] = $cities;
                $data['logisticaTypes'] = $logisticaTypes;

                $data['img_config'] = $this->config[LogisticaPosts::FORM_TYPE];


                break;
            case Blog::FORM_TYPE:
                $post = Blog::where(['id'=>$id])->first();
                $view = 'profile/forms/form_post';

                $data['seo']['title'] =  __('profile.seo_title_edit_blog_post');
                $data['seo']['description'] =  __('profile.seo_description_edit_blog_post');
                $data['seo']['keywords'] =  __('profile.seo_keywords_edit_blog_posts');
                $data['form_title'] = __('profile.edit_blog_post');
                $data['img_config'] = $this->config[Blog::FORM_TYPE];


                break;
            case News::FORM_TYPE:
                $post = News::where(['id'=>$id])->first();
                $view = 'profile/forms/form_post';

                $data['seo']['title'] =  __('profile.seo_title_edit_news_post');
                $data['seo']['description'] =  __('profile.seo_description_edit_news_post');
                $data['seo']['keywords'] =  __('profile.seo_keywords_edit_news_posts');
                $data['form_title'] = __('profile.edit_news_post');
                $data['img_config'] = $this->config[News::FORM_TYPE];


                break;
        }



        if ($post) {
            $data['post'] = $post;
            return view($view,$data);
        } else {
            abort(404);
        }
    }

    public function update_post(Request $request)
    {
        if (!Auth::check()) return redirect('/');
        $type = $request->post('form_type');
        $postId = $request->post('id');
        $program = $this->program;
        $user = Auth::user();
        switch ($type){
            case BooksCornerPosts::FORM_TYPE:

                $validatedData = $request->validate([
                        'title' => 'required|max:255',
                        'text' => 'required',
                        'type_id'=>'required',
                        'section_id'=>'required',
                        'author'=>'max:255|required',
                        'curse'=>'max:255|required',
                        'upload_image'=> 'max:255',
                        'email'=>'required',
                        'phone'=>'required',

                ],[
                        'title.required' => __('Title can\'t be empty '),
                        'text.required' => __('Description can\'t be empty '),
                        'type_id.required' => __('Type can\'t be empty '),
                        'section_id.required' => __('Section can\'t be empty '),
                        'author.required' => __('Author can\'t be empty '),
                        'curse.required'=> __('Curse\Matherie can\'t be empty '),
                        'phone.required'=>__('Contact phone can\'t be empty'),
                        'email.required'=>__('Contact email can\'t be empty'),
                ]);
                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;
                $data['page_title'] = __('Thank you for your post');
                $model = BooksCornerPosts::where(['id'=>$postId])->first();
                if (!$model) abort(404);
                $model->saveNewBooksCornerPost($validatedData);
                $redirectUrl = "/profile/book_posts";
                break;
            case News::FORM_TYPE:
                $model = News::where(['id'=>$postId])->first();
                if (!$model) abort(404);

                $validatedData = $request->validate([
                        'title' => 'required|max:255',
                        'text' => 'required',
                        'upload_image'=> 'max:255'

                ],[
                        'title.required' => __('Title can\'t be empty '),
                        'text.required' => __('Description can\'t be empty '),
                ]);
                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;
                $data['page_title'] = __('Thank you for your news');

                $model->saveNewNews($validatedData);
                $redirectUrl = '/profile/news';
                break;
            case Blog::FORM_TYPE:
                $model = Blog::where(['id'=>$postId])->first();
                if (!$model) abort(404);
                $validatedData = $request->validate([
                        'title' => 'required|max:255',
                        'text' => 'required',
                        'upload_image'=> 'max:255'

                ],[
                        'title.required' => __('Title can\'t be empty '),
                        'text.required' => __('Description can\'t be empty '),
                ]);
                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;
                $data['page_title'] = __('Thank you for your blog post');

                $model->saveNewBlog($validatedData);
                $redirectUrl = '/profile/blogs';
                break;
            case LogisticaPosts::FORM_TYPE:


                $validatedData = $request->validate([
                        'title' => 'required|max:255',
                        'text' => 'required',
                        'logistica_type_id'=>'required',
                        'region_id'=>'required',
                        'province_id'=>'required',
                        'city_id'=>'required',
                        'email'=>'required',
                        'phone'=>'required',
                        'upload_image'=> 'max:255',
                        'gallereies'=>'array'
                ],
                        [
                                'title.required' => __('Title can\'t be empty '),
                                'text.required' => __('Description can\'t be empty '),
                                'type_id.required' => __('Type can\'t be empty '),
                                'region_id.required' => __('Region can\'t be empty '),
                                'province_id.required' => __('Province can\'t be empty '),
                                'city_id.required'=> __('City can\'t be empty '),
                                'phone.required'=>__('Contact phone can\'t be empty'),
                                'email.required'=>__('Contact email can\'t be empty'),
                        ]
                );



                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;

                $model = LogisticaPosts::where(['id'=>$postId])->first();

                if (!$model) abort(404);
                $data['page_title'] =  __('Thak you for your logistica post ');
                $model->saveNewPost($validatedData);
                $redirectUrl = '/profile/accommodation_posts';

                break;

        }

        return redirect($redirectUrl);
    }

    public function delete_posts($type,$id)
    {

        if (!Auth::check()) abort(404);

        $res = '';

        switch ($type)
        {
            case 'book_posts':
                $post = BooksCornerPosts::where(['id'=>$id])->first();
              break;
            case 'accomodations_posts':
                $post = LogisticaPosts::where(['id'=>$id])->first();
                break;
            case 'blog_posts':
                $post = Blog::where(['id'=>$id])->first();
                break;
            case 'news_posts':
                $post = News::where(['id'=>$id])->first();
                break;
        }
        if ($post) {
            $post->delete();
            $res = ['status'=>'ok','message'=>'Your post deleted'];
        } else {
            $res = ['status'=>'error','message'=>__('profile.error_post_not_found')];
        }
        echo json_encode($res);
        exit;
    }


}
