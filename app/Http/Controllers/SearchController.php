<?php

namespace App\Http\Controllers;

use App\BooksCornerPosts;
use App\Cities;
use App\LogisticaPosts;
use App\Province;
use App\Regions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\EducationEntities;
use Illuminate\Support\Facades\Session;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Programs;
use App\BookCornerTypes;
use App\LogisticaTypes;
use App\BookCornerSections;
use Illuminate\Support\Facades\Lang;



class SearchController extends Controller
{
    private $locale;

    public function __construct()
    {
        $this->locale = app()->getLocale();
    }

    //
    public function cities(Request $request)
    {
        $res = [];
        $provinceName = $request->post('province','');
        if ($provinceName) {
            /* @var Province $province */
            $province = Province::where(['url'=>$provinceName])->first();

            if ($province) {
                $res['cities'] = $province->getCitiesListArray();
            }
        }
        $res['res'] = 'ok';
        $response = new Response();
        $response->header('Content-Type','json');
        $response->setContent($res);
        return $response;

    }

    public function provinces(Request $request)
    {
        $res = [];
        $regionName = $request->post('region','');
        if ($regionName) {
            /* @var Regions $region */
            $region = Regions::where(['url'=>$regionName])->first();
            if ($region) {
                $res['provinces'] = $region->getProvincesListArray();
            }
        }
        $res['res'] = 'ok';
        $response = new Response();
        $response->header('Content-Type','json');
        $response->setContent($res);
        return $response;

    }

    public function provincesById(Request $request)
    {
        $res = [];
        $regionId = $request->post('region_id','');
        if ($regionId) {
            /* @var Regions $region */
            $region = Regions::where(['id'=>$regionId])->first();
            if ($region) {
                $res['provinces'] = $region->getProvincesListArray();
            }
        }
        $res['res'] = 'ok';
        $response = new Response();
        $response->header('Content-Type','json');
        $response->setContent($res);
        return $response;

    }


    public function citiesById(Request $request)
    {
        $res = [];
        $provinceId = $request->post('province_id','');
        if ($provinceId) {
            /* @var Province $province */
            $province = Province::where(['id'=>$provinceId])->first();
            if ($province) {
                $res['cities'] = $province->getCitiesListArray();
            }
        }
        $res['res'] = 'ok';
        $response = new Response();
        $response->header('Content-Type','json');
        $response->setContent($res);
        return $response;

    }

    public function form(Request $request)
    {
        $program = $request->get('program');
        $region = $request->get('region','');
        $province = $request->get('province','');
        $city = $request->get('city');
        $searchString = $request->get('search_string');
        $data = [];
        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/'.$program);
        $data['seo']['title'] =  '';
        $data['seo']['description'] =  '';
        $data['seo']['keywords'] =  '';
        $data['regions'] = Regions::all('id','name_'.$this->locale.' as name','svg_horizontal as svg','url','svg_view_box','svg_order','svg_style')->sortBy('svg_order')->toArray();
        $data['menu'] = 'tips';

        /* @var Programs $programObj */
        $programObj = Programs::where(['url'=>$program])->first();

        if ($region) {
            /* @var Regions $regionObj */
            $regionObj = Regions::where(['url'=>$region])->first();
            $data['provinces'] = $regionObj->getProvincesListArray();
            $data['region_url'] = $region;
        }



        if ($province) {
            /* @var Regions $provinceObj */
            $provinceObj = Province::where(['url'=>$province])->first();
            $data['cities'] = $provinceObj->getCitiesListArray();
            $data['province_url'] = $province;
        }


        if ($programObj) {
            $data['program'] = $programObj;
            $data['program_url'] = $program;

            $entity = new EducationEntities();
            $params = ['program'=>$program,'status'=>EducationEntities::ENABLED];
            if ($region) {
                $params['region'] = $region;
            }
            if ($province) {
                $params['province'] = $province;
            }
            if ($city) {
                $data['city_url'] = $city;
                $params['city'] = $city;
            }

            if ($searchString) {
                $params['string'] = $searchString;
            }

            $entities = $entity->getList($params);


            $entities->appends(request()->input())->links();
            $data['entities'] = $entities;

            $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        }

        return view('entity_list',$data);
    }

    public function books_form(Request $request)
    {
        $data = [];
        $program = $request->get('program');
        $type_id = $request->get('corner_books_type_id','');
        $title =  $request->get('author_title','');
        $curse =  $request->get('curse','');
        $section = $request->get('section','');

        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/'.$program);

        $booksCornerTypes = BookCornerTypes::where(['status'=>BookCornerTypes::ENABLED])->get();
        $data['booksCornerTypes'] = $booksCornerTypes;

        /* @var Programs $programObj */
        $programObj = Programs::where(['url'=>$program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['program_url'] = $programObj->url;


        $param = ['status'=>BooksCornerPosts::ENABLED,'program'=>$program,'order'=>['created_at'=>'DESC']];
        if ($type_id) {
            $param['type_id'] = $type_id;
        }
        if ($title){
            $param['title'] = $title;
        }
        if ($curse) {
            $param['curse'] = $curse;
        }
        if ($section) {
            $param['section'] = $section;
        }

        $bookCornerPost = new BooksCornerPosts();
        $publications = $bookCornerPost->getList($param);
        $publications->appends(request()->input())->links();
        $data['publications'] = $publications;

        $data['locale'] = $this->locale;
        $data['menu'] = BooksCornerPosts::FORM_TYPE;
        $data['form_type'] = BooksCornerPosts::FORM_TYPE;
        $data['program'] = $programObj;
        $data['section'] = $section;
        $data['sections'] = BookCornerSections::where(['program'=>$program])->where(['status'=>BookCornerSections::ENABLED])->get();
        $data['type_id'] = $type_id;


        return view('books_corner_list',$data);
    }

    public function logistica_form(Request $request)
    {
        $data = [];
        $program = $request->get('program');
        $type_id = $request->get('logistica_type_id','');
        $title =  $request->get('title','');
        $region_id =  $request->get('region_id','');
        $province_id =  $request->get('province_id','');
        $city_id =  $request->get('city_id','');

        $programObj = Programs::where(['url'=>$program])->first();
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['program_url'] = $programObj->url;
        $data['program'] = $programObj;
        $data['locale'] = $this->locale;


        $data['homeUrl'] = LaravelLocalization::localizeUrl('/program/'.$program);
        $data['seo']['title'] =  '';
        $data['seo']['description'] =  '';
        $data['seo']['keywords'] =  '';
        $data['regions'] = Regions::all('id','name_'.$this->locale.' as name','svg_horizontal as svg','url','svg_view_box','svg_order','svg_style')->sortBy('svg_order')->toArray();
        $data['menu'] = LogisticaPosts::FORM_TYPE;
        $data['form_type'] = LogisticaPosts::FORM_TYPE;

        $params = ['status'=>LogisticaPosts::ENABLED,'program'=>$program,'order'=>['created_at'=>'DESC']];
        if ($type_id) {
            $params['type_id'] = $type_id;
            $data['logisticaTypeId'] = $type_id;
        }
        if ($title){
            $params['title'] = $title;
        }
        if ($region_id) {
            $region = Regions::where(['id'=>$region_id])->first();
            if ($region) {
                $params['region_id'] = $region_id;
                $data['region_url'] = $region->url;

                $provicesArray = [];
                foreach ($region->provinces as $province) {
                    $provicesArray[] = ['id'=>$province->id,'url'=>$province->url,'name'=>$province->{'name_'.$this->locale}];
                }
                $data['provinces'] = $provicesArray;

            }

        }
        if ($province_id){
            $province = Province::where(['id'=>$province_id])->first();
            if ($province) {
                $params['province_id'] = $province_id;
                $data['province_url'] = $province->url;
                $citiesArray = [];
                foreach ($province->cities as $city) {
                    $citiesArray[] = ['id'=>$city->id,'url'=>$city->url,'name'=>$city->{'name_'.$this->locale}];
                }
                $data['cities'] = $citiesArray;
            }

        }
        if ($city_id) {
            $city = Cities::where(['id'=>$city_id])->first();
            if ($city) {
                $params['city_id'] = $city_id;
                $data['city_url'] = $city->url;
            }

        }


        $logisticaTypes = LogisticaTypes::where(['status'=>LogisticaTypes::ENABLED])->get();
        $data['logisticaTypes'] = $logisticaTypes;

        $logisticaPost = new LogisticaPosts();
        $apartmentsReview = $logisticaPost->getList($params);
        $data['apartmentsReview'] = $apartmentsReview;

        return view('logistica_list',$data);


    }


}
