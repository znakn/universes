<?php

namespace App\Http\Controllers;


use App\AccommodationGalleries;
use App\BookCornerSections;
use App\ContactWithUs;
use App\News;
use App\Province;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Programs;
use App\BooksCornerPosts;
use App\LogisticaPosts;
use App\Blog;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Illuminate\Support\Facades\Auth;
use App\BookCornerTypes;
use App\Regions;
use App\LogisticaTypes;


class FormController
{

    use ValidatesRequests;

    private $locale = '';
    private $config = [];

    public function __construct()
    {
        $this->locale = app()->getLocale();
        $this->config = config('app.img');
    }

    public function index(Request $request,$program,$type)
    {
        $data = [];
        $email = Auth::user()->email;
        /* @var Programs $programObj */
        $programObj = Programs::where(['url'=>$program])->first();

        $data['homeUrl'] = LaravelLocalization::localizeUrl('/');
        $data['locale'] = $this->locale;
        $data['post_type'] = $type;
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['program_url'] = $programObj->url;
        $data['form_type'] = $type;
        $data['user_email'] = $email;

        $data['menu'] = '';

        $view = 'form_post';
        switch ($type){
            case BooksCornerPosts::FORM_TYPE:
                $data['form_title'] = __('global.add_publication');
                $data['img_config'] = $this->config[$type];
                $booksCornerTypes = BookCornerTypes::where(['status' => BookCornerTypes::ENABLED])->get();
                $data['booksCornerTypes'] = $booksCornerTypes;
                $sections = BookCornerSections::where(['program'=>$program])->where(['status'=>BookCornerSections::ENABLED])->get();
                $data['booksCornerSections'] = $sections;
                $view = 'book_corner_form_post';
                break;
            case News::FORM_TYPE:
                $data['form_title'] = __('global.add_news');
                $data['img_config'] = $this->config[$type];
                break;
            case Blog::FORM_TYPE:
                $data['form_title'] = __('global.add_blog');
                $data['img_config'] = $this->config[$type];
                break;
            case LogisticaPosts::FORM_TYPE:
                $data['form_title'] = __('global.add_accommodation');
                $data['regions'] = Regions::all('id','name_'.$this->locale.' as name','svg_horizontal as svg','url','svg_view_box','svg_order','svg_style')->sortBy('svg_order')->toArray();
                if (old('region_id')) {
                    $region = Regions::where(['id'=>old('region_id')])->first();
                    if ($region) {
                        $data['provinces'] = $region->getProvincesListArray();
                    }
                }
                if (old('province_id')) {
                    $province = Province::where(['id'=>old('province_id')])->first();
                    if ($province) {
                        $data['cities'] = $province->getCitiesListArray();
                    }
                }

                $logisticaTypes = LogisticaTypes::where(['status'=>LogisticaTypes::ENABLED])->get();
                $data['logisticaTypes'] = $logisticaTypes;

                $data['img_config'] = $this->config[$type];
                $view = 'logistica_form_post';
                break;

        }

//        ppr(old());
//        ppre($data);




        return view($view,$data);
    }

    public function save(Request $request,$program,$type)
    {
        if (!Auth::check()) return redirect('/');

        $data = [];
        /* @var Programs $programObj */
        $programObj = Programs::where(['url'=>$program])->first();

        $data['homeUrl'] = LaravelLocalization::localizeUrl('/');
        $data['locale'] = $this->locale;
        $data['post_type'] = $type;
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['program_url'] = $programObj->url;
        $data['menu'] = '';

        $user = Auth::user();
        switch ($type){
            case BooksCornerPosts::FORM_TYPE:

                $validatedData = $request->validate([
                    'title' => 'required|unique:books_corner_posts,title_en,title_it|max:255',
                    'text' => 'required',
                    'type_id'=>'required',
                    'section_id'=>'required',
                    'author'=>'max:255|required',
                    'curse'=>'max:255|required',
                    'upload_image'=> 'max:255',
                    'email'=>'required',
                    'phone'=>'required',

                ],[
                    'title.required' => __('Title can\'t be empty '),
                    'text.required' => __('Description can\'t be empty '),
                    'type_id.required' => __('Type can\'t be empty '),
                    'section_id.required' => __('Section can\'t be empty '),
                    'author.required' => __('Author can\'t be empty '),
                    'curse.required'=> __('Curse\Matherie can\'t be empty '),
                    'phone.required'=>__('Contact phone can\'t be empty'),
                    'email.required'=>__('Contact email can\'t be empty'),
                ]);
                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;
                $data['page_title'] = __('global.thanks_post');
                $data['return_url'] = LaravelLocalization::localizeUrl('/'.$program.'/list/books-corner');
                $data['return_text'] = __('global.return');
                $model = new BooksCornerPosts();
                $model->saveNewBooksCornerPost($validatedData);
                break;
            case News::FORM_TYPE:
                    $model = new News();

                $validatedData = $request->validate([
                    'title' => 'required|unique:news,title_en,title_it|max:255',
                    'text' => 'required',
                    'upload_image'=> 'max:255'

                ],[
                    'title.required' => __('Title can\'t be empty '),
                    'text.required' => __('Description can\'t be empty '),
                ]);
                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;
                $data['return_url'] = LaravelLocalization::localizeUrl('/'.$program.'/list/news');
                $data['return_text'] = __('global.return');

                $data['page_title'] = __('global.thanks_post');

                $model->saveNewNews($validatedData);
                break;
            case Blog::FORM_TYPE:
                $model = new Blog();

                $validatedData = $request->validate([
                    'title' => 'required|unique:blog,title_en,title_it|max:255',
                    'text' => 'required',
                    'upload_image'=> 'max:255'

                ],[
                    'title.required' => __('Title can\'t be empty '),
                    'text.required' => __('Description can\'t be empty '),
                ]);
                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;
                $data['page_title'] = __('global.thanks_post');
                $data['return_url'] = LaravelLocalization::localizeUrl('/'.$program.'/list/blog');
                $data['return_text'] = __('global.return');


                $model->saveNewBlog($validatedData);
                break;
            case LogisticaPosts::FORM_TYPE:


                $validatedData = $request->validate([
                        'title' => 'required|unique:logistica_posts,title_en,title_it|max:255',
                        'text' => 'required',
                        'logistica_type_id'=>'required',
                        'region_id'=>'required',
                        'province_id'=>'required',
                        'city_id'=>'required',
                        'email'=>'required',
                        'phone'=>'required',
                        'upload_image'=> 'max:255',
                        'gallereies'=>'array'
                    ],
                    [
                        'title.required' => __('Title can\'t be empty '),
                        'text.required' => __('Description can\'t be empty '),
                        'type_id.required' => __('Type can\'t be empty '),
                        'region_id.required' => __('Region can\'t be empty '),
                        'province_id.required' => __('Province can\'t be empty '),
                        'city_id.required'=> __('City can\'t be empty '),
                        'phone.required'=>__('Contact phone can\'t be empty'),
                        'email.required'=>__('Contact email can\'t be empty'),
                    ]
                );



                $validatedData['user_id'] = $user->id;
                $validatedData['program'] = $program;

                $model = new LogisticaPosts();
                $data['page_title'] =  __('global.thanks_post');
                $data['return_url'] = LaravelLocalization::localizeUrl('/'.$program.'/list/logistica');
                $data['return_text'] = __('global.return');


                $model->saveNewPost($validatedData);

                break;

        }

        return view('thanks_for_post',$data);
    }

    public function contact_with_us(Request $request)
    {
        if (!Auth::check()) return redirect('/');

        $data = [];
        $user = Auth::user();
        $program = Session::get('program');

        /* @var Programs $programObj */
        $programObj = Programs::where(['url'=>$program])->first();

        $data['homeUrl'] = LaravelLocalization::localizeUrl('/');
        $data['locale'] = $this->locale;
        $data['post_type'] = 'Request';
        $data['color'] = ($programObj->url == Programs::MASTER_UNIVERSITY)? 'blue':'green';
        $data['program_url'] = $programObj->url;
        $data['menu'] = '';
        $data['page_title'] = __('Thank you for your post');



        $validateData = $request->validate(['message'=>'required']);

        $record = new ContactWithUs();
        $record->user_id = $user->id;
        $record->message = $validateData['message'];
        $record->save();

        return view('thanks_for_post',$data);
    }

    public function fileUpload(Request $request)
    {
        $image = $request->file('file');
        $imageType = $request->post('image_type');
        switch ($imageType)
        {
            case 'logistica_gallery':

                    $path = public_path('/upload/'.LogisticaPosts::FORM_TYPE.'/');
                break;
            case 'book_corner_image':
                    $path = public_path('/upload/'.BooksCornerPosts::FORM_TYPE.'/');
                break;
            case Blog::FORM_TYPE:
                $path = public_path('/upload/'.Blog::FORM_TYPE.'/');
                break;
            case News::FORM_TYPE:
                $path = public_path('/upload/'.News::FORM_TYPE.'/');
            default:
                break;
        }

        // create thumb
        $filename = $this->generateUniqueName($image);

        $file_name= $filename;
        $image->move($path,$file_name);

        return response()->json(['success'=>$file_name,'name'=>$file_name]);

    }

    /**
     * Generate a unique name for uploaded file.
     * @return string
     */
    protected function generateUniqueName($file)
    {
        return md5(uniqid()).'.'.$file->getClientOriginalExtension();
    }





}
