<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    const ENABLED = 1;
    const DISABLED = 0;

    public function entity()
    {
        return $this->hasOne('App\EducationEntities','id','entity_id');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = $model->create_slug($model->title_it);
            }
        });


        self::updating(function($model){
            // ... code here
            if (!$model->url) {
                $model->url = $model->create_slug($model->title_it);
            }
        });


    }

    private function create_slug($string){
        $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return $slug;
    }
}
