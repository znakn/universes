<?php

namespace App;

use Encore\Admin\Form\Field\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Regions extends Model
{
    protected $table = 'regions';

    private $locale = 'it';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->locale = Session::exists('locale') ? Session::get('locale'):'it';
    }

    public function country()
    {
        return $this->hasOne('App\Countries','id','country_id');
    }

    public function provinces()
    {
        return $this->hasMany(Province::class,'region_id','id');
    }


    public function cities()
    {
        return $this->hasMany(Cities::class,'region_id','id');
    }

    public function getCitiesListArray()
    {
        $citiesArray = [];
        foreach ($this->cities as $city) {
            $citiesArray[] = ['id'=>$city->id,'url'=>$city->url,'name'=>$city->{'name_'.$this->locale}];
        }
        return $citiesArray;
    }

    public function getProvincesListArray()
    {
        $provincesArray = [];
        foreach ($this->provinces as $province) {
            $provincesArray[] = ['id'=>$province->id,'url'=>$province->url,'name'=>$province->{'name_'.$this->locale}];
        }
        return $provincesArray;
    }


}
