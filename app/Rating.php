<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $table = 'ratings';

    public static function existRating($entityId)
    {
        $res = false;
        $user = Auth::user();
        if ($user) {
            $r = self::where(['user_id'=>$user->id,'entity_id'=>$entityId])->first();
            if ($r) {
                $res = true;
            }
        }
        return $res;
    }

    public function entity()
    {
        return $this->hasOne('App\EducationEntities','id','entity_id');
    }

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

}
