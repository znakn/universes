<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','last_name','country','city','date_birth', 'email', 'password','is_agree_terms','prefer_language'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function books_corner_posts()
    {
        return $this->hasMany('App\BooksCornerPosts', 'user_id', 'id');
    }

    public function accommodation_posts()
    {
        return $this->hasMany('App\LogisticaPosts', 'user_id', 'id');
    }

    public function blogs_posts()
    {
        return $this->hasMany('App\Blog', 'user_id', 'id');
    }

    public function news_posts()
    {
        return $this->hasMany('App\News', 'user_id', 'id');
    }


}
