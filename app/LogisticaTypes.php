<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogisticaTypes extends Model
{
    protected $table = 'logistica_types';

    const ENABLED = 1;
    const DISABLED = 0;
}
