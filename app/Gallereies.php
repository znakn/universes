<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallereies extends Model
{
    //
    public function entity()
    {
        return $this->hasOne('App\EducationEntities','id','entity_id');
    }
}
