<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{

    Auth::routes();


    Route::get('/', 'MainController@index')->name('home');
    //Route::get('/custom', 'HomeController@custom_page')->name('custom');
    Route::get('/program/{url}', 'MainController@program')->name('program');

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/locale/{lang}', 'MainController@location')->name('home');

    Route::get('/{program}/list', 'MainController@entity_list_program')->name('entity_list_program');
    Route::get('/{program}/{region}/list', 'MainController@entity_list_region')->name('entity_list_region');

    Route::post('/search/cities', 'SearchController@cities')->name('get_cities');
    Route::post('/search/provinces', 'SearchController@provinces')->name('get_provinces');
    Route::post('/search/cities-by-id', 'SearchController@citiesById')->name('get_cities_by_id');
    Route::post('/search/provinces-by-id', 'SearchController@provincesById')->name('get_provinces_by_id');
    Route::get('/search/form', 'SearchController@form')->name('search_form');

    Route::get('{program}/entity/{slug}','MainController@entity')->name('entity');

    Route::get('{program}/list/news', 'MainController@news')->name('news_list_program');
    Route::get('/news/{slug}', 'MainController@news_view')->name('news_view');
    Route::get('/news/preview/{slug}', 'MainController@news_preview')->name('news_preview');
    Route::get('/portal-news/{slug}', 'MainController@admin_news_view')->name('admin_news_view');
    Route::get('/review/{program}/{slug}', 'MainController@review_view')->name('review_view');
    Route::get('/book-corner-post/{slug}', 'MainController@advert_view')->name('advert_view');
    Route::get('/book-corner-post/preview/{slug}', 'MainController@advert_preview')->name('advert_preview');
    Route::get('/logistica-post/{slug}', 'MainController@logistic_advert_view')->name('logistica_view');
    Route::get('/logistica-post/preview/{slug}', 'MainController@logistic_advert_preview')->name('logistica_preview');


    Route::get('{program}/list/blog', 'MainController@blog')->name('blog_list_program');
    Route::get('/blog/{slug}', 'MainController@blog_view')->name('blog_view');
    Route::get('/blog/preview/{slug}', 'MainController@blog_preview')->name('blog_preview');
    Route::get('/discipline/{slug}', 'MainController@discipline_view')->name('discipline_view');

    Route::get('/info', 'MainController@info')->name('info');



    Route::get('{program}/list/tips', 'MainController@tips')->name('tips_list_program');
    Route::get('{program}/list/books-corner/{section?}', 'MainController@books_corner')->name('books_corner_list');
    Route::get('{program}/list/logistica', 'MainController@logistica')->name('logistica_list_program');

    Route::get('{program}/post-form/{type}','FormController@index')->name('post_form');
    Route::post('{program}/post-form/{type}','FormController@save')->name('save_post_form');
    Route::post('/form/file-upload','FormController@fileUpload')->name('form-file-upload');

    Route::get('/search/books/form', 'SearchController@books_form')->name('search_books_form');
    Route::get('/search/logistica/form', 'SearchController@logistica_form')->name('search_logistica_form');

    Route::post('/contact-with-us','FormController@contact_with_us')->name('contact_with_us');
    Route::get('/profile/{slug?}','ProfileController@index')->name('profile');
    Route::post('/profile-update','ProfileController@profile_update')->name('profile_update');
    Route::get('/profile/post-edit/{type}/{id}','ProfileController@edit_posts')->name('edit_posts');
    Route::post('/profile/profile-update','ProfileController@update_post')->name('post_update');
    Route::post('/profile/post-delete/{type}/{id}','ProfileController@delete_posts')->name('delete_posts');

    Route::post('/rating','MainController@rating')->name('rating');


    Route::get('/test', 'FormController@test')->name('test');

    Route::get('/{url}','MainController@custom')->name('custom_page');


});


Route::post('/image-crop/upload','ImageCropController@upload');







